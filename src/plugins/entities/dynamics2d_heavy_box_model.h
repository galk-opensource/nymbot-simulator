/**
 * This acts and works like a normal Box entity in the original src files.
 * Except it has its own linear/angular floor friction values.
 */

#ifndef DYNAMICS2D_HEAVY_BOX_MODEL_H
#define DYNAMICS2D_HEAVY_BOX_MODEL_H

namespace argos {
   class CDynamics2DStretchableObjectModel;
   class CDynamics2DHeavyBoxModel;
   class CPhysicsBoxModel;
}

#include <argos3/plugins/simulator/physics_engines/dynamics2d/dynamics2d_stretchable_object_model.h>
#include <argos3/plugins/simulator/physics_engines/physics_box_model.h>
#include "heavy_box_entity.h"

namespace argos {

   class CDynamics2DHeavyBoxModel : public CDynamics2DStretchableObjectModel,
                               public CPhysicsBoxModel {

   public:

      CDynamics2DHeavyBoxModel(CDynamics2DEngine& c_engine,
                          CHeavyBoxEntity& c_entity);
      virtual ~CDynamics2DHeavyBoxModel() {}

      void Resize(const CVector3& c_size);
   };

}

#endif
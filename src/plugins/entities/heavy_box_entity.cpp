/**
 * This acts and works like a normal Box entity in the original src files.
 * Except it has its own linear/angular floor friction values.
 * You can create the entity through the xml like creating a box instead changing the tag from <box> </box> to <heavy_box> </heavy_box>
 * and when you make a moveable heavy box you will need to add linear_friction and angular_friction attributes in addition to mass.
 */

#include "heavy_box_entity.h"
#include <argos3/core/utility/math/matrix/rotationmatrix3.h>
#include <argos3/core/simulator/space/space.h>
#include <argos3/core/simulator/simulator.h>
#include <argos3/plugins/simulator/media/led_medium.h>
#include <argos3/plugins/simulator/physics_engines/physics_box_model.h>

namespace argos {

   /****************************************/
   /****************************************/

   CHeavyBoxEntity::CHeavyBoxEntity():
      CComposableEntity(nullptr),
      m_pcEmbodiedEntity(nullptr),
      m_pcLEDEquippedEntity(nullptr),
      m_fMass(1.0f),
      m_pcLEDMedium(nullptr),
      m_fLinearFriction(1.0f),
      m_fAngularFriction(1.0f) {}

   /****************************************/
   /****************************************/

   CHeavyBoxEntity::CHeavyBoxEntity(const std::string& str_id,
                          const CVector3& c_position,
                          const CQuaternion& c_orientation,
                          bool b_movable,
                          const CVector3& c_size,
                          Real f_mass,
                          Real f_linearFriction,
                          Real f_angularFriction) :
      CComposableEntity(nullptr, str_id),
      m_pcEmbodiedEntity(
         new CEmbodiedEntity(this,
                             "body_0",
                             c_position,
                             c_orientation,
                             b_movable)),
      m_pcLEDEquippedEntity(
         new CLEDEquippedEntity(this,
                                "leds_0")),
      m_cSize(c_size),
      m_fMass(f_mass),
      m_fLinearFriction(f_linearFriction),
      m_fAngularFriction(f_angularFriction) {
      AddComponent(*m_pcEmbodiedEntity);
      AddComponent(*m_pcLEDEquippedEntity);
   }

   /****************************************/
   /****************************************/

   void CHeavyBoxEntity::Init(TConfigurationNode& t_tree) {
      try {
         /* Init parent */
         CComposableEntity::Init(t_tree);
         /* Parse XML to get the size */
         GetNodeAttribute(t_tree, "size", m_cSize);
         /* Parse XML to get the movable attribute */         
         bool bMovable;
         GetNodeAttribute(t_tree, "movable", bMovable);
         if(bMovable) {
            /* Parse XML to get the mass */
            GetNodeAttribute(t_tree, "mass", m_fMass);
            GetNodeAttribute(t_tree, "linear_friction", m_fLinearFriction);
            GetNodeAttribute(t_tree, "angular_friction", m_fAngularFriction);
         }
         else {
            m_fMass = 0.0f;
            m_fLinearFriction = 0.0f;
            m_fAngularFriction = 0.0f;
         }
         /* Create embodied entity using parsed data */
         m_pcEmbodiedEntity = new CEmbodiedEntity(this);
         AddComponent(*m_pcEmbodiedEntity);
         m_pcEmbodiedEntity->Init(GetNode(t_tree, "body"));
         m_pcEmbodiedEntity->SetMovable(bMovable);
         /* Init LED equipped entity component */
         m_pcLEDEquippedEntity = new CLEDEquippedEntity(this);
         AddComponent(*m_pcLEDEquippedEntity);
         if(NodeExists(t_tree, "leds")) {
            /* Create LED equipped entity
             * NOTE: the LEDs are not added to the medium yet
             */
            m_pcLEDEquippedEntity->Init(GetNode(t_tree, "leds"));
            /* Add the LEDs to the medium */
            std::string strMedium;
            GetNodeAttribute(GetNode(t_tree, "leds"), "medium", strMedium);
            m_pcLEDMedium = &CSimulator::GetInstance().GetMedium<CLEDMedium>(strMedium);
            m_pcLEDEquippedEntity->SetMedium(*m_pcLEDMedium);
            m_pcLEDEquippedEntity->Enable();
         }
         UpdateComponents();
      }
      catch(CARGoSException& ex) {
         THROW_ARGOSEXCEPTION_NESTED("Failed to initialize box entity \"" << GetId() << "\".", ex);
      }
   }

   /****************************************/
   /****************************************/

   void CHeavyBoxEntity::Reset() {
      /* Reset all components */
      CComposableEntity::Reset();
      /* Update components */
      UpdateComponents();
   }

   /****************************************/
   /****************************************/

   void CHeavyBoxEntity::EnableLEDs(CLEDMedium& c_medium) {
      m_pcLEDMedium = &c_medium;
      m_pcLEDEquippedEntity->SetMedium(*m_pcLEDMedium);
      m_pcLEDEquippedEntity->Enable();
   }

   /****************************************/
   /****************************************/

   void CHeavyBoxEntity::DisableLEDs() {
      m_pcLEDEquippedEntity->Disable();
   }
   
   /****************************************/
   /****************************************/

   void CHeavyBoxEntity::AddLED(const CVector3& c_offset,
                           const CColor& c_color) {
      m_pcLEDEquippedEntity->AddLED(c_offset,
                                    GetEmbodiedEntity().GetOriginAnchor(),
                                    c_color);
      UpdateComponents();
   }

   /****************************************/
   /****************************************/

   void CHeavyBoxEntity::Resize(const CVector3& c_size) {
      /* Store size */
      m_cSize = c_size;
      /* Go through the physics box models and call resize on them */
      for(size_t i = 0; i < m_pcEmbodiedEntity->GetPhysicsModelsNum(); ++i) {
         dynamic_cast<CPhysicsBoxModel&>(m_pcEmbodiedEntity->GetPhysicsModel(i)).Resize(c_size);
      }
      /* Update bounding box */
      m_pcEmbodiedEntity->CalculateBoundingBox();
   }

   /****************************************/
   /****************************************/

   REGISTER_ENTITY(CHeavyBoxEntity,
                   "heavy_box",
                   "Carlo Pinciroli [ilpincy@gmail.com]",
                   "1.0",
                   "A stretchable 3D box.",
                   "The box entity can be used to model walls, obstacles or box-shaped grippable\n"
                   "objects. It can be movable or not. A movable object can be pushed and gripped.\n"
                   "An unmovable object is pretty much like a wall.\n\n"
                   "REQUIRED XML CONFIGURATION\n\n"
                   "To declare an unmovable object (i.e., a wall) you need the following:\n\n"
                   "  <arena ...>\n"
                   "    ...\n"
                   "    <heavy_box id=\"box1\" size=\"0.75,0.1,0.5\" movable=\"false\">\n"
                   "      <body position=\"0.4,2.3,0\" orientation=\"45,0,0\" />\n"
                   "    </heavy_box>\n"
                   "    ...\n"
                   "  </arena>\n\n"
                   "To declare a movable object you need the following:\n\n"
                   "  <arena ...>\n"
                   "    ...\n"
                   "    <heavy_box id=\"box1\" size=\"0.75,0.1,0.5\" movable=\"true\" mass=\"2.5\" linear_friction=\"2.0\" angular_friction=\"3.3\">\n"
                   "      <body position=\"0.4,2.3,0\" orientation=\"45,0,0\" />\n"
                   "    </heavy_box>\n"
                   "    ...\n"
                   "  </arena>\n\n"
                   "The 'id' attribute is necessary and must be unique among the entities. If two\n"
                   "entities share the same id, initialization aborts.\n"
                   "The 'size' attribute specifies the size of the box along the three axes, in\n"
                   "the X,Y,Z order. When you add a box, imagine it initially unrotated and\n"
                   "centered in the origin. The size, then, corresponds to the extent along the X,\n"
                   "Y and Z axes.\n"
                   "The 'movable' attribute specifies whether or not the object is movable. When\n"
                   "set to 'false', the object is unmovable: if another object pushes against it,\n"
                   "the box won't move. When the attribute is set to 'true', the box is movable\n"
                   "upon pushing or gripping. When an object is movable, the 'mass' attribute is\n"
                   "required.\n"
                   "The 'mass' attribute quantifies the mass of the box in kg.\n"
                   "The 'linear_friction' attribute is the value of linear friction to the floor.\n"
                   "The 'angular_friction' attribute is the value of angular friction to the floor.\n"
                   "The 'body/position' attribute specifies the position of the base of the box in\n"
                   "the arena. The three values are in the X,Y,Z order.\n"
                   "The 'body/orientation' attribute specifies the orientation of the 3D box. All\n"
                   "rotations are performed with respect to the center of mass. The order of the\n"
                   "angles is Z,Y,X, which means that the first number corresponds to the rotation\n"
                   "around the Z axis, the second around Y and the last around X. This reflects\n"
                   "the internal convention used in ARGoS, in which rotations are performed in\n"
                   "that order. Angles are expressed in degrees.\n\n"
                   "OPTIONAL XML CONFIGURATION\n\n"
                   "It is possible to add any number of colored LEDs to the box. In this way,\n"
                   "the box is visible with a robot camera. The position and color of the\n"
                   "LEDs is specified with the following syntax:\n\n"
                   "  <arena ...>\n"
                   "    ...\n"
                   "    <heavy_box id=\"box1\" size=\"0.75,0.1,0.5\" movable=\"true\" mass=\"2.5\">\n"
                   "      <body position=\"0.4,2.3,0\" orientation=\"45,0,0\" />\n"
                   "      <leds medium=\"id_of_led_medium\">\n"
                   "        <led offset=\" 0.15, 0.15,0.15\" anchor=\"origin\" color=\"white\" />\n"
                   "        <led offset=\"-0.15, 0.15,0\"    anchor=\"origin\" color=\"red\"   />\n"
                   "        <led offset=\" 0.15, 0.15,0\"    anchor=\"origin\" color=\"blue\"  />\n"
                   "        <led offset=\" 0.15,-0.15,0\"    anchor=\"origin\" color=\"green\" />\n"
                   "      </leds>\n"
                   "    </heavy_box>\n"
                   "    ...\n"
                   "  </arena>\n\n"
                   "In the example, four LEDs are added to the box. The LEDs have\n"
                   "different colors and are located one on the top and three\n"
                   "around the box. The LEDs are managed by the LED medium declared in\n"
                   "the <media> section of the configuration file with id \"id_of_led_medium\"",
                   "Usable"
      );

   /****************************************/
   /****************************************/

   REGISTER_STANDARD_SPACE_OPERATIONS_ON_COMPOSABLE(CHeavyBoxEntity);

   /****************************************/
   /****************************************/

}
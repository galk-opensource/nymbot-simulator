/**
 * This acts and works like a normal Box entity in the original src files.
 * Except it has its own linear/angular floor friction values.
 */

#ifndef HEAVY_BOX_ENTITY_H
#define HEAVY_BOX_ENTITY_H

namespace argos {
   class CHeavyBoxEntity;
}

#include <argos3/core/simulator/entity/embodied_entity.h>
#include <argos3/core/simulator/entity/composable_entity.h>
#include <argos3/plugins/simulator/entities/led_equipped_entity.h>

namespace argos {

   class CHeavyBoxEntity : public CComposableEntity {

   public:

      ENABLE_VTABLE();

      CHeavyBoxEntity();

      CHeavyBoxEntity(const std::string& str_id,
                 const CVector3& c_position,
                 const CQuaternion& c_orientation,
                 bool b_movable,
                 const CVector3& c_size,
                 Real f_mass = 1.0f,
                 Real f_linearFriction = 1.0f,
                 Real f_angularFriction = 1.0f);

      virtual void Init(TConfigurationNode& t_tree);
      virtual void Reset();

      /*
       * Enables the LEDs for this entity.
       * Adds the LED equipped entity to the given medium.
       * If you don't call this method, the LEDs added with
       * CHeavyBoxEntity::AddLED() won't be updated correctly.
       * @param c_medium The medium to which the LEDs must be associated.
       * @see CHeavyBoxEntity::AddLED()
       */
      void EnableLEDs(CLEDMedium& c_medium);

      /*
       * Disables the LEDs for this entity.
       */
      void DisableLEDs();

      /**
       * Adds an LED to this entity.
       * For the LEDs to be updated correctly, you must first call
       * CHeavyBoxEntity::EnableLEDs().
       * @param c_offset The position of the LED wrt the origin anchor.
       * @param c_color The color of the LED.
       * @see CHeavyBoxEntity::EnableLEDs()
       */
      void AddLED(const CVector3& c_offset,
                  const CColor& c_color = CColor::BLACK);

      inline CEmbodiedEntity& GetEmbodiedEntity() {
         return *m_pcEmbodiedEntity;
      }

      inline const CEmbodiedEntity& GetEmbodiedEntity() const {
         return *m_pcEmbodiedEntity;
      }

      inline CLEDEquippedEntity& GetLEDEquippedEntity() {
         return *m_pcLEDEquippedEntity;
      }

      inline const CLEDEquippedEntity& GetLEDEquippedEntity() const {
         return *m_pcLEDEquippedEntity;
      }

      inline const CVector3& GetSize() const {
         return m_cSize;
      }

      void Resize(const CVector3& c_size);

      inline Real GetMass() const {
         return m_fMass;
      }

      inline void SetMass(Real f_mass) {
         m_fMass = f_mass;
      }

      inline Real GetLinearFriction() const {
         return m_fLinearFriction;
      }

      inline void SetLinearFriction(Real f_linearFriction) {
         m_fLinearFriction = f_linearFriction;
      }

      inline Real GetAngularFriction() const {
         return m_fAngularFriction;
      }

      inline void SetAngularFriction(Real f_angularFriction) {
         m_fAngularFriction = f_angularFriction;
      }

      virtual std::string GetTypeDescription() const {
         return "heavy box";
      }

   private:

      CEmbodiedEntity*    m_pcEmbodiedEntity;
      CLEDEquippedEntity* m_pcLEDEquippedEntity;
      CVector3            m_cSize;
      Real                m_fMass;
      CLEDMedium*         m_pcLEDMedium;
      Real                m_fLinearFriction;
      Real                m_fAngularFriction;

   };

}

#endif
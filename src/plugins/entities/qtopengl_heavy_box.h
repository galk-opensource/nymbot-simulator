/**
 * This acts and works like a normal Box entity in the original src files.
 * Except it has its own linear/angular floor friction values which isn't relevant for this file.
 */

#ifndef QTOPENGL_HEAVY_BOX_H
#define QTOPENGL_HEAVY_BOX_H

namespace argos {
   class CQTOpenGLHeavyBox;
   class CHeavyBoxEntity;
}

#ifdef __APPLE__
#include <gl.h>
#else
#include <GL/gl.h>
#endif

namespace argos {

   class CQTOpenGLHeavyBox {

   public:

      CQTOpenGLHeavyBox();

      virtual ~CQTOpenGLHeavyBox();

      virtual void DrawLEDs(CHeavyBoxEntity& c_entity);
      virtual void Draw(const CHeavyBoxEntity& c_entity);

   private:

      void MakeBody();
      void MakeLED();

   private:

      GLuint m_unBaseList;
      GLuint m_unBodyList;
      GLuint m_unLEDList;
      GLuint m_unVertices;

   };

}

#endif

#ifndef INSECTBOT_MEASURES_H
#define INSECTBOT_MEASURES_H

#include <argos3/core/utility/datatypes/datatypes.h>
#include <argos3/core/utility/math/angles.h>
#include <argos3/core/utility/math/vector3.h>

namespace argos {

   // base dimensions define the rigid body in the simulation
   static constexpr Real INSECTBOT_BASE_WIDTH             = 0.01;  // in meters
   static constexpr Real INSECTBOT_BASE_HEIGHT            = 0.05;  // in meters
   // static constexpr Real INSECTBOT_MASS                   = 0.0005; // units are unknown
   // 80 grams we need to check what are the units.
   // 96 gram a heavier version.
   static constexpr Real INSECTBOT_MASS                   = 1; // units are unknown


   // other dimensions define the shape used for collision detection
   static constexpr Real INSECTBOT_LENGTH                   = 0.056; // in meters
   static constexpr Real INSECTBOT_WIDTH                  = 0.021; // in meters
   static constexpr Real INSECTBOT_HEIGHT                 = 0.0273; // in meters
   static constexpr Real INSECTBOT_FRICTION   = 0.0f; // TODO: not clear how used and what are the units. Was 1.5f in master prior to merge

   static constexpr Real INSECTBOT_WHEEL_RADIUS       = 0.00875; // in meters

   // argos builtin differential steering parameters
   static constexpr Real INSECTBOT_INTERWHEEL_DISTANCE      = 0.0175;  // distance between wheels in meters
   // static constexpr Real INSECTBOT_MAX_FORCE  = 0.001f;
   // TODO: needs to be measured for its real values
   static constexpr Real INSECTBOT_MAX_TORQUE = 0.001f;
   static constexpr Real INSECTBOT_MAX_FORCE  = 2;

   
   // nymbots v1 do not have proximity sensors
   static constexpr Real PROXIMITY_SENSOR_RING_ELEVATION = 0.0001f;
   static constexpr Real PROXIMITY_SENSOR_RING_RADIUS = 0.009f;
   static const CRadians PROXIMITY_SENSOR_RING_START_ANGLE = CRadians((ARGOS_PI / 12.0f) * 0.5f);
   static constexpr Real PROXIMITY_SENSOR_RING_RANGE = 0.08f;
   static constexpr Real PROXIMITY_SENSOR_NUM_SENSORS = 24;
 
}

#endif

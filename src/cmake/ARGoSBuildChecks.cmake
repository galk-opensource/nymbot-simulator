#
# Find the ARGoS package
#
set(ARGoS_DIR "/usr/share/argos3/cmake")
message(STATUS "ARGOS_DIR: ${ARGOS_DIR}")
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${ARGOS_PREFIX}/share/argos3/cmake)
message(STATUS "CMAKE_MODULE_PATH: ${CMAKE_MODULE_PATH}")

# Select one of the two lines below
set(OpenGL_GL_PREFERENCE GLVND) 
#set(OpenGL_GL_PREFERENCE LEGACY)

find_package(ARGoS REQUIRED)
if(NOT ARGOS_FOUND)
# this works when argos installed from source or using pkgconfig
    message(WARNING "ARGOS not found using cmake, trying to find it using pkgconfig")
    find_package(PkgConfig REQUIRED)
    pkg_check_modules(ARGOS REQUIRED argos3_simulator)
    set(ARGOS_PREFIX ${ARGOS_PREFIX} CACHE INTERNAL "")
    message(STATUS "ARGOS_PREFIX: ${ARGOS_PREFIX}")
  else(NOT ARGOS_FOUND)
# this works when argos installed from binary
   message(STATUS "ARGOS found using cmake")
   set(ARGOS_PREFIX /usr CACHE INTERNAL "")
   message(STATUS "ARGOS_PREFIX: ${ARGOS_PREFIX}")
endif(NOT ARGOS_FOUND)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${ARGOS_PREFIX}/share/argos3/cmake)
message(STATUS "CMAKE_MODULE_PATH: ${CMAKE_MODULE_PATH}")
set(CMAKE_INSTALL_PREFIX ${ARGOS_PREFIX} CACHE STRING "Install path prefix, prepended onto install directories." FORCE)

#
# Check whether all the necessary libs have been installed to compile the
# code that depends on Qt and OpenGL
#
# include(ARGoSCheckQTOpenGL)   # GalK, definitely wrong

find_package(ARGoSQTOpenGL REQUIRED)
message(STATUS "ARGOS_QTOPENGL_FOUND: ${ARGOS_QTOPENGL_FOUND}")

#
# Find Lua
#
find_package(Lua REQUIRED)

#
# Look for librt, necessary on some platforms
#
if(NOT APPLE)
    find_package(RT REQUIRED)
endif(NOT APPLE)

#
# Set ARGoS include dir
#
include_directories(${CMAKE_SOURCE_DIR} ${ARGOS_INCLUDE_DIRS} ${LUA_INCLUDE_DIR})

#
# Set ARGoS link dir
#
# GalK when using pkgconfig:   link_directories(${ARGOS_LIBRARY_DIRS}) 
link_directories(${ARGOS_LIBRARY_DIR})
message(STATUS "ARGOS_LIBRARY_DIR: ${ARGOS_LIBRARY_DIR}")
message(STATUS "ARGOS_LIBRARIES: ${ARGOS_LIBRARIES}")
message(STATUS "ARGOS_LIBRARY_DIRS: ${ARGOS_LIBRARY_DIRS}")


#ifndef NYMBOT_LOCATION_DATA_H
#define NYMBOT_LOCATION_DATA_H


using namespace argos;
using namespace std;

#define DEFAULT_ANGULAR_VELOCITY 5
#define DEFAULT_SPEED 148

enum TStateNames {INSECTBOT_STATE_STOP, INSECTBOT_STATE_TURNING, INSECTBOT_STATE_MOVING};


struct nymbot_location_data {
    float x;
    float y;
    CDegrees orientation;
    Real wantedOrientation;
    int16_t angular_velocity = 0;
    vector<float> speed {0.0,0.0};
    bool is_managed = false;
    int16_t default_angular_velocity = DEFAULT_ANGULAR_VELOCITY;
    uint8_t default_speed = DEFAULT_SPEED;
    TStateNames state;
};

#endif

#ifndef COLLISION_H
#define COLLISION_H


using namespace argos;
using namespace std;



struct Collision {
	int id1;
	int id2;
	CDegrees angle; // calculated by the first nymbot
};

#endif


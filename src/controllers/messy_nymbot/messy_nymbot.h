
#ifndef MESSY_NYMBOT_H
#define MESSY_NYMBOT_H

/*
 * Include some necessary headers.
 */

/* Definition of the CCI_Controller class. */
#include <argos3/core/control_interface/ci_controller.h>
/* Definition of the differential steering actuator */
#include <argos3/plugins/robots/generic/control_interface/ci_differential_steering_actuator.h>
#include <argos3/plugins/robots/generic/control_interface/ci_proximity_sensor.h>
#include <argos3/plugins/robots/generic/control_interface/ci_positioning_sensor.h>

/* Random number generator */
#include <argos3/core/utility/math/rng.h>
/* Logging functions */
#include <argos3/core/utility/logging/argos_log.h>
#include <vector>
#include <controllers/dummy_nymbot/dummy_nymbot.h>


/*
 * All the ARGoS stuff in the 'argos' namespace.
 * With this statement, you save typing argos:: every time.
 */
using namespace argos;
using namespace std;

enum Behavior {NORMAL,TENDENCY_RIGHT, TENDENCY_LEFT, STOP_SOMETIMES, NUM_OF_BEHAVIORS, STOP};


/*
 * A controller is simply an implementation of the CCI_Controller class.
 */
class CMessyNymbot : public CDummyNymbot {

public:
   CMessyNymbot();
   void Reset() override;
   void setControllerVelocity(Real l, Real r) override;
   CVector2 calcSpeed(double percent, Real l,Real r);

private:
   Behavior m_tCurrentBehavior;
   bool noise;
   clock_t start;
};

#endif

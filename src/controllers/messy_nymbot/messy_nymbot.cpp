/* Include the controller definition */
#include "messy_nymbot.h"
/* Function definitions for XML parsing */
#include <argos3/core/utility/configuration/argos_configuration.h>
/* 2D vector definition */
#include <argos3/core/utility/math/vector2.h>
#include <argos3/core/utility/math/vector3.h>
#include <vector>
#include <algorithm>
#include <fstream>
#include <time.h>
#include <iostream>
using namespace std;
#include <random>


/****************************************/
/****************************************/

#define PIN_STOP 0.0f
#define  WHEELS_BASE 1.3
#define  WHEELS_RADIUS 0.5
#define NOISE_CONST 0.3

#define MAX_SPEED 8
#define MIN_SPEED -8
#define MAX_ANGULAR_VELOCITY 3
#define MIN_ANGULAR_VELOCITY -3



CMessyNymbot::CMessyNymbot() :m_tCurrentBehavior(NORMAL){};
/****************************************/
/****************************************/

void CMessyNymbot::Reset()
{
    random_device rd{};
    mt19937 gen{rd()};
    uniform_int_distribution<int>d1{0,NUM_OF_BEHAVIORS-1};
    int behave = d1(gen);
    m_tCurrentBehavior = (Behavior) (behave + NORMAL);
    noise = true;
}


double randomNoise() {
    float noise;
    random_device rd{};
    mt19937 gen{rd()};

    // values near the mean are the most likely
    // standard deviation affects the dispersion of generated values from the mean
    normal_distribution<double> d{0,1};
    noise = d(gen);
    return noise*0.1;
}


CVector2 CMessyNymbot::calcSpeed(double percent, Real l,Real r) {
	CVector2 res;
	if (percent >= 0.98){ 
		start = clock();
		m_tCurrentBehavior = STOP;
		res={0,0};
	}
	
	else if (percent >= 0.8){
		res = {l*percent,r*percent};
	}
	else {
		res = {l,r};
	}   
	return res;     
} 



/****************************************/
/****************************************/

void CMessyNymbot::setControllerVelocity(Real l, Real r){

    double_t noiseR = 0;
    double_t noiseL = 0;
    random_device rd{};
    mt19937 gen{rd()};
    uniform_real_distribution<Real>d{0,1};
    double percent = d(gen);
    clock_t end;
    double diff = 1;

    if (noise) {
        noiseR = randomNoise();
        noiseL = randomNoise();
    }
    Real motorL = l;
    Real motorR = r;
    Behavior b = m_tCurrentBehavior;


    switch (b) {
        case STOP: {
            end = clock();
            double currdiff =(double)(end - start)/CLOCKS_PER_SEC;
            if (currdiff>= diff) {
                m_tCurrentBehavior = STOP_SOMETIMES;
            }
            break;
        }
        case STOP_SOMETIMES:{
        	CVector2 speed = calcSpeed(percent,l,r);
        	motorL=speed.GetX();
            motorR=speed.GetY();
            break;
        }
        case TENDENCY_RIGHT:
            motorL=(l-percent*NOISE_CONST) + noiseL;
            motorR=r+noiseR;
            break;
        case TENDENCY_LEFT:
            motorL=l + noiseL;
            motorR=(r-percent*NOISE_CONST) + noiseR;
            break;
        case NORMAL:
        default:
           motorL=l + noiseL;
            motorR=r + noiseR;
            break;
        }
        m_fMotorL=motorL;
        m_fMotorR=motorR;
        m_pcMotors->SetLinearVelocity(m_fMotorL, m_fMotorR);
}

/*
 * This statement notifies ARGoS of the existence of the controller.
 * It binds the class passed as first argument to the string passed as
 * second argument.  The string is then usable in the configuration
 * file to refer to this controller.  When ARGoS reads that string in
 * the configuration file, it knows which controller class to
 * instantiate.  See also the configuration files for an example of
 * how this is used.
 */
REGISTER_CONTROLLER(CMessyNymbot, "messy_nymbot_controller")

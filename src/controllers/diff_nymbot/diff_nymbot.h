
#ifndef DIFF_NYMBOT_H
#define DIFF_NYMBOT_H

/*
 * Include some necessary headers.
 */

/* Definition of the CCI_Controller class. */
#include <argos3/core/control_interface/ci_controller.h>
/* Definition of the differential steering actuator */
#include <argos3/plugins/robots/generic/control_interface/ci_differential_steering_actuator.h>
#include <argos3/plugins/robots/generic/control_interface/ci_proximity_sensor.h>
#include <argos3/plugins/robots/generic/control_interface/ci_positioning_sensor.h>

/* Random number generator */
#include <argos3/core/utility/math/rng.h>
/* Logging functions */
#include <argos3/core/utility/logging/argos_log.h>
#include <vector>

#include "plugins/robots/insectbot/simulator/insectbot_measures.h"

/*
 * All the ARGoS stuff in the 'argos' namespace.
 * With this statement, you save typing argos:: every time.
 */
using namespace argos;
using namespace std;


/*
 * A controller is simply an implementation of the CCI_Controller class.
 */
class CDiffNymbot : public CCI_Controller {

public:

   /* Class constructor. */
   CDiffNymbot();

   /* Class destructor. */
   virtual ~CDiffNymbot() {}

   /*
    * This function initializes the controller.
    * The 't_node' variable points to the <parameters> section in the XML
    */
   virtual void Init(TConfigurationNode& t_node);

   /*
    * This function is called once every time step.
    * The length of the time step is set in the XML file.
    */
   virtual void ControlStep();

   /*
    * This function resets the controller to its state right after the
    * Init().
    * It is called when you press the reset button in the GUI.
    * In this example controller there is no need for resetting anything,
    * so the function could have been omitted. It's here just for
    * completeness.
    */
   virtual void Reset();

   /*
    * Called to cleanup what done by Init() when the experiment finishes.
    * In this example controller there is no need for clean anything up,
    * so the function could have been omitted. It's here just for
    * completeness.
    */
   virtual void Destroy() {}

   /*
    * These functions allow to track the current state of the robot
    */
   virtual void setControllerVelocity(Real l, Real r);
   void setVelocity(int speed, int angularVel);
   virtual vector<Real> getVelocity();
   virtual vector<double> getLocation();

protected:
   virtual void log(const std::string& message);


   /* Pointer to the differential steering actuator */
   CCI_DifferentialSteeringActuator* m_pcMotors;
   CCI_ProximitySensor* m_sensor;
   CCI_PositioningSensor* m_positionGetter;

   
   /* actual motor speed */
   Real   m_fMotorL;
   Real   m_fMotorR;

   // converting the sizes taken from "insectbot_measures.h" from m to cm
   static constexpr double INSECTBOT_WIDTH_CM = INSECTBOT_WIDTH * 100;
   static constexpr double INSECTBOT_INTERWHEEL_DISTANCE_CM = INSECTBOT_INTERWHEEL_DISTANCE * 100;

   UInt32 log_robot_interval;
   UInt32 last_logged_robot;

   // std::vector<Real>& proximity_reads;
   std::ofstream log_file;
};

#endif

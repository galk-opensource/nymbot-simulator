/* Include the controller definition */
#include "diff_nymbot.h"
/* Function definitions for XML parsing */
#include <argos3/core/utility/configuration/argos_configuration.h>
/* 2D vector definition */
#include <argos3/core/utility/math/vector2.h>
#include <argos3/core/utility/math/vector3.h>
#include <vector>
#include <algorithm>
#include <fstream>
#include <time.h>
#include <chrono>
using namespace std;


/****************************************/
/****************************************/

//static Real MAX_DRIVE_SPEED;  // TODO:  We keep these around in case we want to re-introduce max speed caps.
//static Real MAX_TURN_SPEED;


CDiffNymbot::CDiffNymbot() : m_pcMotors(NULL),
                                         m_sensor(NULL),
                                         m_positionGetter(NULL),
                                         m_fMotorL(0.0f),
                                         m_fMotorR(0.0f),
                                         log_robot_interval(0),
                                         last_logged_robot(0),
                                         log_file()
{

}

/****************************************/
/****************************************/

void CDiffNymbot::Init(TConfigurationNode &t_node)
{
   /*
    * Get sensor/actuator handles
    *
    * The passed string (ex. "differential_steering") corresponds to the
    * XML tag of the device whose handle we want to have. For a list of
    * allowed values, type at the command prompt:
    *
    * $ argos3 -q actuators
    *
    * to have a list of all the possible actuators, or
    *
    * $ argos3 -q sensors
    *
    * to have a list of all the possible sensors.
    *
    */
   // Get sensor/actuator handles
   m_pcMotors = GetActuator<CCI_DifferentialSteeringActuator>("differential_steering");
   m_sensor = GetSensor<CCI_ProximitySensor>("proximity");
   m_positionGetter = GetSensor<CCI_PositioningSensor>("positioning");
   // Parse the configuration file
   std::string log_file_name;
   GetNodeAttributeOrDefault<std::string>(t_node, "log_file",log_file_name,"experiment.log");
   GetNodeAttributeOrDefault<UInt32>(t_node, "log_robot_interval",log_robot_interval,5);
//   GetNodeAttributeOrDefault<Real>(t_node, "max_drive_speed",MAX_DRIVE_SPEED,10.0f);
//   GetNodeAttributeOrDefault<Real>(t_node, "max_turn_speed",MAX_TURN_SPEED,10.0f);

   if (!log_file.is_open()){
      log_file.open(log_file_name,std::ios_base::app);
      this->log("Robot Created");
   }
   Reset();
}

/****************************************/
/****************************************/

void CDiffNymbot::Reset()
{
   // reset/initialise the robot state
   m_fMotorL = m_fMotorR = 0.0;
    setVelocity(300, 0);
}

/****************************************/
/****************************************/


void CDiffNymbot::log(const std::string& message)
{
   std::time_t tt = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
   std::tm tm = *std::localtime(&tt);

   const CVector3 &pos = m_positionGetter->GetReading().Position;
    CRadians anglex;
    CRadians angley;
    CRadians anglez;

   m_positionGetter->GetReading().Orientation.ToEulerAngles(anglez,angley,anglex);
   std::stringstream sstm;
   sstm<<R"({"Date":")"<<std::put_time( &tm, "%Y-%m-%d %H:%M:%S") <<R"(","ID":")"<< GetId()<<R"(","x":")"<<pos[0]<<R"(","y":")"<<pos[1]<<R"(","Angle":")"<<anglez.GetValue()*anglez.RADIANS_TO_DEGREES<<R"(","Message":")"<<message<<R"("})"<<std::endl;
   //writes to the logfile
   log_file<<sstm.str();
   //write to the screen
   LOG     <<sstm.str();
}

void CDiffNymbot::ControlStep()
{
   time_t seconds;
   seconds = time (NULL);
   if (seconds-last_logged_robot>log_robot_interval){
      this->log("");
      last_logged_robot=seconds;
   }
   this->setVelocity(1000,0); // TODO:  Why is this here?  Seems arbitrary. Shouldn't we send the current speed? or the desired?  GalK thinks it is a default control step, to be re-defined by anyone using the controller. In which case it should be documented as such.

   //m_pcMotors->SetLinearVelocity(MAX_DRIVE_SPEED, MAX_DRIVE_SPEED);
}

/****************************************/
/****************************************/

double minMaxNorm(double max, double min, double x,  double maxRange, double minRange) {
    return  (((x-min) / (max-min))* (maxRange - minRange))+ minRange;
}


inline double normSpeed(double x) {
    return x * 0.01;  // TODO:  multiply by 0.01 instead of dividing by 100, to convert from 1/100s cm/s to cm/s
}

double degToRadians(int x) {
    /*
      From deg/s to rads/sec.
      We did an optimization - multiplying by 0.005 instead of dividing by 180.
    */
      return x * 0.00555555555 * ARGOS_PI;
}

void CDiffNymbot::setControllerVelocity(Real l, Real r){
   m_fMotorL=l;
   m_fMotorR=r;
   m_pcMotors->SetLinearVelocity(m_fMotorL, m_fMotorR);
}


void CDiffNymbot::setVelocity(int speed,int angularVel){
   // speed is in 1/100s cm/s.  355 means 3.55cm/s
   // angular velocity is converted from deg/s to rads/sec (see degToRadians)
   double s = normSpeed(speed); // convert from 1/100s cm/s to cm/s, so s will be 3.55 given speed of 355.
   /* We need to convert from degrees to radians, and we have 2 ways to do that */
   // Option 1: A function that multiplies by pi and divides by 180 (for optimisation we multiplied by 0.0055)
   //double a = degToRadians(angularVel);
   // Option 2: Using ARGOS built-in types and function to convert.
   CDegrees d = CDegrees(angularVel);
   Real r = ToRadians(d).GetValue();

   /* The source for the formulas:
    * https://rossum.sourceforge.net/papers/DiffSteer/
    * for:
    * sl = left wheel linear speed in cm/s
    * sr = right wheel linear speed in cm/s
    * s = middle of the robot linear speed in cm/s
    * a = change in orientation with time
    * d = distance between the wheels
    * we know that:
    * 1. a = (sr - sl) / d
    * 2. s = (sr + sl) / 2 
    * and therefore we get:
    * 1. sl = s - a * d / 2
    * 2. sr = s + a * d /2
    * We take the opposite because the robot mixes them up */

/* In ARgOS, we can set the linear speed of the wheels directly (how quickly they move across the ground)
 * But see below */

    Real wheelLinearVelocityRight = (s - r * INSECTBOT_INTERWHEEL_DISTANCE_CM / 2);
    Real wheelLinearVelocityLeft =  (s + r * INSECTBOT_INTERWHEEL_DISTANCE_CM / 2);

/* In a real robot, we instead to set the angular speed of the wheels (how quickly each of them rotates on its own axis)
 *

    // TODO: We should put a limit to the speed that is the same as in the physical robot.
    //wheelAngularSpeedRight = wheelAngularSpeedRight > MAX_DRIVE_SPEED ? MAX_DRIVE_SPEED : (wheelAngularSpeedRight < (-MAX_DRIVE_SPEED) ? (-MAX_DRIVE_SPEED) : wheelAngularSpeedRight);
    //wheelAngularSpeedLeft = wheelAngularSpeedLeft > MAX_DRIVE_SPEED ? MAX_DRIVE_SPEED : (wheelAngularSpeedLeft < (-MAX_DRIVE_SPEED) ? (-MAX_DRIVE_SPEED) : wheelAngularSpeedLeft);
*/

    setControllerVelocity(wheelLinearVelocityLeft,wheelLinearVelocityRight);



}


vector<Real> CDiffNymbot::getVelocity(){
    vector<Real> v= {m_fMotorL,m_fMotorR};
    return v;
}

vector<double> CDiffNymbot::getLocation(){
    const CVector3 &pos = m_positionGetter->GetReading().Position;
    vector<double> v= {pos[0],pos[1]};
    return v;
}


/*
 * This statement notifies ARGoS of the existence of the controller.
 * It binds the class passed as first argument to the string passed as
 * second argument.  The string is then usable in the configuration
 * file to refer to this controller.  When ARGoS reads that string in
 * the configuration file, it knows which controller class to
 * instantiate.  See also the configuration files for an example of
 * how this is used.
 */
REGISTER_CONTROLLER(CDiffNymbot, "diff_nymbot_controller")

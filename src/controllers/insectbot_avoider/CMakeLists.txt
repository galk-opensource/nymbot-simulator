#add_library(insectbot_avoider SHARED insectbot_avoider.h insectbot_avoider.cpp)
add_library(insectbot_avoider MODULE insectbot_avoider.h insectbot_avoider.cpp)
#  This compiles, but the server reports and error and exits because it cannot find the "insectbot" symbol.
#  The argos server reports this error when the display is initialized. So it makes sense that the same problem
#   occurs in the SHARED version, but the server ignores it. I am therefore leaving this here because it can assit in debugging.
#
#   Update: turns out that in MODULE mode, one has to install (sudo make install), because the avoider is apparently not relying on the plugin as a shared library, so cannot find it
#   after install, still no robots visible.
target_link_libraries(insectbot_avoider
  argos3core_simulator
  argos3plugin_simulator_insectbot
  argos3plugin_simulator_genericrobot)

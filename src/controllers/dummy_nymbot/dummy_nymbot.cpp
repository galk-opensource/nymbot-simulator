/* Include the controller definition */
#include "dummy_nymbot.h"
/* Function definitions for XML parsing */
#include <argos3/core/utility/configuration/argos_configuration.h>
/* 2D vector definition */
#include <argos3/core/utility/math/vector2.h>
#include <argos3/core/utility/math/vector3.h>
#include <vector>
#include <algorithm>
#include <fstream>
#include <time.h>
#include <chrono>
using namespace std;

/****************************************/
/****************************************/

#define PIN_STOP 0.0f
#define WHEELS_BASE 1.3
#define WHEELS_RADIUS 0.5

#define MAX_SPEED 10
#define MIN_SPEED -10
#define MAX_ANGULAR_VELOCITY 3
#define MIN_ANGULAR_VELOCITY -3

CDummyNymbot::CDummyNymbot() : m_pcMotors(NULL),
                               m_sensor(NULL),
                               m_positionGetter(NULL),
                               m_fMotorL(0.0f),
                               m_fMotorR(0.0f),
                               m_maxSensorRange(0.0f),
                               m_driveSpeed(0.0f),
                               m_turnSpeed(0.0f),
                               m_stopProb(0),
                               m_moveProb(0),
                               log_robot_interval(0),
                               last_logged_robot(0),
                               log_file()

{
   m_pcRNG = CRandom::CreateRNG("argos");
}

/****************************************/
/****************************************/

void CDummyNymbot::Init(TConfigurationNode &t_node)
{
   /*
    * Get sensor/actuator handles
    *
    * The passed string (ex. "differential_steering") corresponds to the
    * XML tag of the device whose handle we want to have. For a list of
    * allowed values, type at the command prompt:
    *
    * $ argos3 -q actuators
    *
    * to have a list of all the possible actuators, or
    *
    * $ argos3 -q sensors
    *
    * to have a list of all the possible sensors.
    *
    */
   // Get sensor/actuator handles
   m_pcMotors = GetActuator<CCI_DifferentialSteeringActuator>("differential_steering");
   m_sensor = GetSensor<CCI_ProximitySensor>("proximity");
   m_positionGetter = GetSensor<CCI_PositioningSensor>("positioning");
   // Parse the configuration file
   std::string log_file_name;
   GetNodeAttributeOrDefault<std::string>(t_node, "log_file", log_file_name, "experiment.log");
   GetNodeAttributeOrDefault<UInt32>(t_node, "log_robot_interval", log_robot_interval, 5);
   GetNodeAttributeOrDefault<Real>(t_node, "drive_speed", m_driveSpeed, 10.0f);
   GetNodeAttributeOrDefault<Real>(t_node, "turn_speed", m_turnSpeed, 2.6f);
   GetNodeAttributeOrDefault<UInt32>(t_node, "stop_uniform_range", m_stopProb, 450);
   GetNodeAttributeOrDefault<UInt32>(t_node, "re_move_uniform_range", m_moveProb, 200);
   GetNodeAttributeOrDefault<Real>(t_node, "max_range", m_maxSensorRange, 1.0f);

   // Prompets a red message that the user is using this contoller, which is not the correct one.
    std::cout << RED_COLOR << "Error: YOU ARE USING AN OLD CONTROLLER! PLEASE USE DIFF CONTROLLER." << RESET_COLOR << std::endl;

   if (!log_file.is_open()) {
      log_file.open(log_file_name, std::ios_base::app);
      this->log("Robot Created");
   }
   Reset();
}

/****************************************/
/****************************************/

void CDummyNymbot::Reset()
{
   // reset/intialise the robot state
   m_fMotorL = m_fMotorR = 0.0;
    setVelocity(300, 0);
}

/****************************************/
/****************************************/

void CDummyNymbot::log(const std::string &message)
{
   std::time_t tt = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
   std::tm tm = *std::localtime(&tt);

   const CVector3 &pos = m_positionGetter->GetReading().Position;
   CRadians anglex;
   CRadians angley;
   CRadians anglez;

   m_positionGetter->GetReading().Orientation.ToEulerAngles(anglez, angley, anglex);
   std::stringstream sstm;
   sstm << R"({"Date":")" << std::put_time(&tm, "%Y-%m-%d %H:%M:%S") << R"(","ID":")" << GetId() << R"(","x":")" << pos[0] << R"(","y":")" << pos[1] << R"(","Angle":")" << anglez.GetValue() * anglez.RADIANS_TO_DEGREES << R"(","Message":")" << message << R"("})" << std::endl;
   // writes to the logfile
   log_file << sstm.str();
   // write to the screen
   LOG << sstm.str();
}

void CDummyNymbot::ControlStep()
{
   time_t seconds;
   seconds = time(NULL);
   if (seconds - last_logged_robot > log_robot_interval)
   {
      this->log("");
      last_logged_robot = seconds;
   }
}

/****************************************/
/****************************************/

double minMaxNorm(double max, double min, double x, double maxRange, double minRange)
{
   return (((x - min) / (max - min)) * (maxRange - minRange)) + minRange;
}


double normSpeed(int x) {
    double min = 0;
    double max = 200;
    double simMin = MIN_SPEED;
    double simMax = MAX_SPEED;
    return minMaxNorm(max, min, x, simMax, simMin);
}

double normAngularVel(int x) {
    double min = -15;
    double max = 15;
    double simMin = MIN_ANGULAR_VELOCITY;
    double simMax = MAX_ANGULAR_VELOCITY;
    return minMaxNorm(max, min, x, simMax, simMin);
}

void CDummyNymbot::setControllerVelocity(Real l, Real r)
{
   m_fMotorL = l;
   m_fMotorR = r;
   m_pcMotors->SetLinearVelocity(m_fMotorL, m_fMotorR);
}

void CDummyNymbot::setVelocity(int speed, int angularVel){
    double s = normSpeed(speed);
    double a = normAngularVel(angularVel);
    int speedRight = s + a;
    int speedLeft = s;
    if (angularVel > 0){
        speedRight = s;
        speedLeft = s - a;
    }
    speedRight = speedRight > MAX_SPEED ? MAX_SPEED : (speedRight < (MIN_SPEED) ? (MIN_SPEED) : speedRight);
    speedLeft = speedLeft > MAX_SPEED ? MAX_SPEED : (speedLeft < (MIN_SPEED) ? (MIN_SPEED) : speedLeft);
    setControllerVelocity(speedLeft, speedRight);
}

vector<Real> CDummyNymbot::getVelocity() {
   vector<Real> v = {m_fMotorL, m_fMotorR};
   return v;
}

vector<double> CDummyNymbot::getLocation(){
    const CVector3 &pos = m_positionGetter->GetReading().Position;
    vector<double> v = {pos[0], pos[1]};
    return v;
}


/*
 * This statement notifies ARGoS of the existence of the controller.
 * It binds the class passed as first argument to the string passed as
 * second argument.  The string is then usable in the configuration
 * file to refer to this controller.  When ARGoS reads that string in
 * the configuration file, it knows which controller class to
 * instantiate.  See also the configuration files for an example of
 * how this is used.
 */
REGISTER_CONTROLLER(CDummyNymbot, "dummy_nymbot_controller")

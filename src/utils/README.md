# Sense and draw library

## Background

This library is used to simulate vision for each of the nymbots. It can sense other nymbots and obstacles in the arena such as walls, cylnders and boxes. It can also draw the FoV of each nymbot and mark which objects it sees.
We will first overview the folder structure and then explain how to use the library.

## Folder structure

`insectbot_circle_fov_sense` - This folder contains the code for simulating the sensing of an insectbot. The code contains the following:

1. Helper functions for working with angles and distances.
2. Functions for detecting obstacles and nymbots in the arena.
3. Functions for occlusion handling methods. Four are implmented as of now - X_RAY, OMID, COMPLID and PARTID.

`draw` - This folder contains the code for drawing the FoV of each nymbot and marking which objects it sees.

## Usage

The usage of this library is divided into two parts - sensing and drawing. The following sections describe how to use each of these parts.


### Sensing

#### Linking the library to the loop_function

1. Create a new loop_function folder with .cpp, .h and cmakelists.txt files. Make sure that in your_loop_function.h it inherits from `api_loop_function`. As follows:
   ```cpp
   class YOUR_LOOP_FUNCTION : public CApi_loop_function {
   ```
2. In a api_loop_function.h file under public there is :

   ```cpp
   CInsectbotCircleFovSense sensor;
   ```

   This creates the sensing object for the simulation. All the sensing functions are called using this object.

3. In Init() function of your loop function, is the following line:

   ```cpp
   sensor = CInsectbotCircleFovSense(robotNumber);
   ```

   This initializes the sensing object with the number of robots in the simulation.
4. In Init() function of your loop_function, are the following lines: 
    ```cpp
    initBoxesMap();
    initCylindersMap();
    ```
    These Two functions fill the obstacles structure with the information recieved from argos about the boxes/walls and cylinders in the arena.

#### Sensing obstacles and nymbots

 There are a lot of functions in `insectbot_circle_fov_sense.cpp`, here we will highlight the important ones to understand how to use the library. For understaing whats going on under the hood, please refer to the code (happy reading ;).

   #### Detecting obstacles

   ```cpp
   void findObstacles(nymbot_location_data nymbots_arr[], Obstacles obstacles);
   ```

   This function is used to find the obstacles each robot senes. It takes in an array of nymbot_location_data and an object of type Obstacles. The function will update the nymbots_data array with the obstacles each robot sees.

   #### Detecting nymbots

   ```cpp
   void findNymbots(nymbot_location_data nymbots_arr[], VISION_MODEL model);
   ```

   This function is used to find the nymbots each robot senes. It takes in an array of nymbot_location_data and an object of type VISION_MODEL. The function will update the nymbot_location_data array with the nymbots it sees.

   Both functions are called in the loop_function under
   `void CApi_loop_function::LastSeenPosition()` :

   ```cpp
   sensor.findObstacles(nymbot_location_data, Obstacles[]);
   sensor.findNymbots(nymbot_location_data, VISION_MODEL);
   ```

   Obstacles is the array of all obscacles in the arena. The currenct supported obstacles are:
   1. Boxes.
   2. Cylinders.
   3. Walls.
   4. Circles (Not fully working).

    Walls is currently supporting - North (0), East (90), South(0) and West(90). Also they need to be around the center of the arena. Other wall directions are not supported there is an issue with the distance measurement.
    Boxes can not have to much of a length as the distance to them is measured from the center.
     
    <br>VISION_MODEL is the occlusion handling method. The occlusion handling methods are:

   1. _X_RAY_ - The robot can see through other robots.
   2. _OMID_ - Partially occluded neighbor is omitted from the field of view.
   3. _COMPLID_ - Neighbor is completed from the seen segment.
   4. _PARTID_ - Partially seen segment is regarded as a neighbor.
      <br><br>

   - See Krongauz et al. paper for more information about the occlusion handling methods.

   #### Iterating over the sensed nymbots:

   ```cpp
   vector<DetectionData> detectionData = sensor.getNymbotsDetectionVector(nymbots_arr, id);
   vector<DetectionData> detectionData = sensor.
   getObstaclesDetectionVector(nymbots_arr, id);
   ```

   This function returns a vector of type DetectionData. It contains all the nymbots/obstacles the robot sees. The information about each object includes the object's id, and 2 vectors stretches from the nymbot's location to the edges of the object.

   ```cpp
   for (auto it = detectionData.begin(); it != detectionData.end(); it++)
   ```

   This is an example of how to iterate over the vector.

### Drawing

1. In the your_experiments.argos file, add the user_functions tag:
    ```xml
    <visualization>
        <qt-opengl>
            <user_functions library="build/utils/draw/insectbot_draw/libinsectbot_draw.so" label="insectbot_draw" />
            <camera>
                <placements>
                    <placement index="0" position="0,0,13"  look_at="0,0,0" up="1,0,0" lens_focal_length="280" />
                </placements>
            </camera>
        </qt-opengl>
    </visualization>
    ```
    This will load the draw library into the simulation, and it will be called each step.
2. In the loop_function the `sensor` must be public.
3. The drawing is done by the argos function `DrawInWorld()`, it iterates of the `drawRayList` and `drawCircleList`. These are two vectors in the `sensor`(sense object) object of the loop function.
4. The drawing itself is done using ARGoS functions:
    ```cpp
    void DrawRay( CRay3& c_ray, CColor& c_color,Real f_width);
    void DrawCircle(  CVector3& c_position,
                    CQuaternion& c_orientation,
                    Real f_radius,
                    CColor& c_color,
                    bool b_fill,
                    GLuint un_vertices);
    ```
    Color of rays and circles are defined in `insectbot_circle_fov_sense.h`:
    ```cpp
    #define VECTOR_COLOR CColor::ORANGE    
    #define OBSTACLE_VECTOR_COLOR CColor::PURPLE
    ```
    There are many more functions in the ARGoS library, you can find them in `qtopengl_user_functions.h`.
## History
1. Version 1.0 - The first version of the library. It was used in the experiments of the paper. [sense documentation](./src/utils/insectbot_circle_fov_sense/README.md) [draw documentation](./src/utils/draw/README.md)
    - Created the library.
    - Updated from square to circle FOV.
2. Version 1.1 - The second version of the library.
    - Sense can work with any number of robots.
    - Draw works with loop_function api and not a specific loop_function.
    - Moved sense and draw to be an external library.
    
## Additional information

In `insectbot_circle_fov` there is an array of struct `nymbot_data`, which keeps the visual information each nymbot has.
```cpp
struct nymbot_data {
Rectangle nymbot;

        vector<DetectionData> robotsDetection;
        vector<AngleDetectionData> robotsAngles[ANGLES_ARRAY_SIZE];

        vector<DetectionData> obstaclesDetection;
        vector<AngleDetectionData> obstaclesAngles[ANGLES_ARRAY_SIZE];
    };
```
Here `robotsDetection` and `obstaclesDetection` are vectors of type `DetectionData`, they hold all the elements the robot 'sees', each element is a robot/obstacle. `robotsAngles` and `obstaclesAngles` are vectors of type `AngleDetectionData`, they hold all the elements the robot 'sees' in each angle. The size of the vector is `ANGLES_ARRAY_SIZE`, which is the number of angles the robot can see. The angles are equally spaced between 0 and 360 degrees. The `nymbot` variable is the rectangle that represents the nymbot in the simulation.

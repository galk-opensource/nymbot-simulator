#ifndef NYMBOT_DATA_H
#define NYMBOT_DATA_H


using namespace argos;
using namespace std;

#include <utils/includes/range.h>
#include <utils/includes/detection_data.h>
#include <utils/includes/angle_detection_data.h>
#include <utils/includes/rectangle.h>
#include <utils/includes/line.h>

#define ANGLES_ARRAY_SIZE 3600
#define ANGLE_ADJUSTMENT (ANGLES_ARRAY_SIZE / 360)

struct nymbot_data {
    Rectangle nymbot;

    vector<DetectionData> robotsDetection;
    vector<AngleDetectionData> robotsAngles[ANGLES_ARRAY_SIZE];

    vector<DetectionData> obstaclesDetection;
    vector<AngleDetectionData> obstaclesAngles[ANGLES_ARRAY_SIZE];
};

#endif


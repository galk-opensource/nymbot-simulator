#ifndef DRAW_CIRCLE_H
#define DRAW_CIRCLE_H

#include <argos3/core/utility/math/vector3.h>

struct DrawCircle
{
    argos::CVector3 center;
    argos::Real radius;
    DrawCircle(argos::CVector3 center, argos::Real radius) : center(center), radius(radius) {}
};

#endif
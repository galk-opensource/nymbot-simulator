#ifndef DETECTION_DATA_H
#define DETECTION_DATA_H

using namespace std;

#include "nymbot_vector.h"

struct DetectionData
{
    int type;
	int id;
	CDegrees orientation;
	NymbotVector vector1, vector2;
};

#endif
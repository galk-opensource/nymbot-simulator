#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <utils/includes/defines.h>
#include <utils/includes/line.h>

#define linesInRectangle 4
#define pointsInRectangle 4

using namespace argos;
using namespace std;



struct Rectangle {
    Real points[linesInRectangle][XY];
    Line lines[linesInRectangle];
};

#endif
#ifndef DRAW_RAY_H
#define DRAW_RAY_H

#include <argos3/core/utility/math/vector3.h>
#include <argos3/core/utility/math/ray3.h>
#include <argos3/core/utility/datatypes/color.h>


struct DrawRay {
  argos::CRay3 ray;
  argos::CColor color;
  DrawRay(argos::CRay3 ray, argos::CColor color) : ray(ray), color(color) {} 
};

#endif
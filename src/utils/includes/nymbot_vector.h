#ifndef NYMBOT_VECTOR_H
#define NYMBOT_VECTOR_H

#include <utils/includes/defines.h>
using namespace std;



struct NymbotVector {
	double direction;
    double magnitude;

    double point1[XY];
    double point2[XY];
};

#endif
#ifndef BOX_H
#define BOX_H

#include "rectangle.h"

using namespace std;



struct Box {
    int id;
    double position[XY];
    argos::CDegrees orientation;
    double size[XYZ];
    Rectangle box;
};

#endif
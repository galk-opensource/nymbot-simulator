#ifndef RANGE_H
#define RANGE_H

#include <utils/includes/defines.h>

using namespace argos;
using namespace std;



struct Range {
	Real xRange[XY];
	Real yRange[XY];
	int index;
};

#endif


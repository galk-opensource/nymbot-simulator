#ifndef ANGLE_DETECTION_DATA_H
#define ANGLE_DETECTION_DATA_H


using namespace std;



struct AngleDetectionData {
    int id;
	double distance;
    double point[2];
    CDegrees orientation;
    int type;
};

#endif
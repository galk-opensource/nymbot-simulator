#ifndef OBSTACLES_H
#define OBSTACLES_H


using namespace argos;
using namespace std;

#include <utils/includes/box.h>
#include <utils/includes/circle.h>
#include <utils/includes/cylinder.h>


struct Obstacles {
    vector<Box> boxes;
    vector<Circle> circles;
    vector<Cylinder> cylinders;
    vector<Box> walls;
};

#endif
#ifndef CIRCLE_H
#define CIRCLE_H

#include <utils/includes/defines.h>
using namespace argos;
using namespace std;



struct Circle {
    int id;
    double position[XY];
    double radius;
};

#endif
#ifndef CYLINDER_H
#define CYLINDER_H

#include <utils/includes/defines.h>
using namespace argos;
using namespace std;



struct Cylinder {
    int id;
    double position[XY];
    double radius;
};

#endif
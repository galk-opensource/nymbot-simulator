### Detection methods:
1. findObstacles - find all obstacles in each nymbot's FOV
2. findNymbots - find all nymbots in each nymbot's FOV


### Getters:
1. Get detection by angle: (getNymbotsDetectionByAngle, getObstaclesDetectionByAngle, getWallsDetectionByAngle)
Returns a vector contains information about the objects the nymbot sees in the given angle.
The information about each object includes the object's id, its distance from the nymbot and the point on the object that the nymbot sees. 
The returned vector is sorted by distances.

2. Get detection vector: (getNymbotsDetectionVector, getObstaclesDetectionVector, getWallsDetectionVector)
Returns a vector contains information about the objects the nymbot sees in its FOV.
The information about each object includes the object's id, and 2 vectors stretches from the nymbot's location to the edges of the object.


### Angle converters:
The angle recieved from argos after normalization (using normOrientation function) is in the range of 0-360. In order to convert the angle to be relative to the nymbot's orientation, use the function convertAngleByOrientation.
The nymbot's front becomes the zero angle, the angles from its front to its right back become 0-180, and the angles from its left to its left back become -180-0.
In order to convert the angle back to the global angle (in range 0-360), use the function convertAngleToGlobalAngle.
#include "insectbot_circle_fov_sense.h"

/**
 * @brief Default constructor for CInsectbotCircleFovSense.
 *        Initializes data structures for nymbot_data, drawRayList, and drawCircleList.
 *        Allocates memory with default  ROBOT_MAX_NUMBER robots.
 */
CInsectbotCircleFovSense::CInsectbotCircleFovSense()
{
	nymbots_data = new nymbot_data[ROBOT_MAX_NUMBER];
	drawRayList = new vector<DrawRay>[ROBOT_MAX_NUMBER];
	drawCircleList = new vector<DrawCircle>[ROBOT_MAX_NUMBER];
};

/**
 * @brief Constructor for CInsectbotCircleFovSense with a specified number of robots.
 *        Initializes data structures for nymbot_data, drawRayList, and drawCircleList.
 * @param robotNumberFromExp The number of robots for which memory is allocated.
 */
CInsectbotCircleFovSense::CInsectbotCircleFovSense(unsigned int robotNumberFromExp) : robotNumber(robotNumberFromExp)
{
	nymbots_data = new nymbot_data[robotNumberFromExp];
	drawRayList = new vector<DrawRay>[robotNumberFromExp];
	drawCircleList = new vector<DrawCircle>[robotNumberFromExp];
};

//**********************Helper Functions*************************//
// TODO: move to helper file
/**
 * @brief Normalize CDegrees orientation to the range 0-360.
 * @param orientation The angle in degrees to be normalized.
 * @return The normalized angle in the range 0-360.
 */
Real normOrientation(CDegrees orientation)
{
	Real orientVal = orientation.UnsignedNormalize().GetValue();
	return orientVal;
}

/**
 * @brief Convert a (global) angle from the range 0-360 to the range -180-180,
 *        relative to the orientation 0.
 * @param angle The angle in the range 0-360 to be converted.
 * @return The converted angle in the range -180-180.
 */
Real convertToAngleRangeMinus180To180(Real angle)
{ // TODO: check if this function is needed. Built in ARGOS.
	if (angle >= 0 && angle < 180)
	{
		return angle * -1;
	}
	return 360 - angle;
}

/**
 * @brief Convert an angle from the range -180 to 180 (relative to orientation 0)
 *        to the range 0 to 360.
 * @param angle The angle in the range -180 to 180 to be converted.
 * @return The converted angle in the range 0 to 360.
 */
Real convertToAngleRange0To360(Real angle)
{ // TODO: check if this function is needed. Built in ARGOS.
	if (angle >= -180 && angle < 0)
	{
		return angle * -1;
	}
	return 360 - angle;
}

// calc distance between two points
double CInsectbotCircleFovSense::calcDistance(double x1, double y1, double x2, double y2)
{
	return sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2));
}

/**
 * @brief Given normalized orientation (0-360) and global angle between 0-360,
 *        convert the angle to a range between -180 to 180 based on the orientation.
 * @param orientation The normalized orientation in the range 0 to 360.
 * @param angle The global angle in the range 0 to 360 to be converted.
 * @return The converted angle in the range -180 to 180 based on the orientation.
 */
Real convertAngleByOrientation(Real orientation, Real angle)
{
	// convert orientation and angle to -180 to 180 by orientation 0
	Real newOrientation = convertToAngleRangeMinus180To180(orientation);
	angle = convertToAngleRangeMinus180To180(angle);
	// find start and end range of edge cases (edge cases: from -180/180 angles of newOrientation to -180/180 angles of orientation 0)
	Real startRange = 180 - orientation;
	Real endRange = startRange >= 0 ? 180 : -180;
	if (startRange > endRange)
	{
		Real temp = startRange;
		startRange = endRange;
		endRange = temp;
	}
	// convert angle to -180 to 180 range according to the orientation
	if (angle >= (startRange - EPSILON1) && angle <= (endRange + EPSILON1))
	{
		if (newOrientation >= 0)
		{
			return angle - newOrientation + 360;
		}
		return angle - newOrientation - 360;
	}
	return angle - newOrientation;
}
/**
 * @brief Given normalized orientation (0-360) and angle between -180 to 180 (according to orientation),
 *        convert to the global angle in the range 0-360.
 * @param orientation The normalized orientation in the range 0 to 360.
 * @param angle The angle in the range -180 to 180, relative to the orientation, to be converted.
 * @return The global angle in the range 0 to 360.
 */
Real convertAngleToGlobalAngle(Real orientation, Real angle)
{
	// convert orientation and angle to -180 to 180 by orientation 0
	Real newOrientation = convertToAngleRangeMinus180To180(orientation);
	// find start and end range of edge cases (edge cases: from -180/180 angles of newOrientation to -180/180 angles of orientation 0)
	// and convert atart and end ranges from range -180-180 to range 0-360
	Real startRange = convertToAngleRange0To360(180 - orientation);
	Real endRange = startRange >= 0 ? convertToAngleRange0To360(180) : convertToAngleRange0To360(-180);
	// convert to start and end angles to range -180-180 by the orientation
	startRange = convertAngleByOrientation(orientation, startRange);
	endRange = convertAngleByOrientation(orientation, endRange);
	if (startRange > endRange)
	{
		Real temp = startRange;
		startRange = endRange;
		endRange = temp;
	}
	// convert angle to global 0-360 orientation
	if (angle >= (startRange - EPSILON1) && angle <= (endRange + EPSILON1))
	{
		if (newOrientation >= 0)
		{
			return convertToAngleRange0To360(angle + newOrientation - 360);
		}
		return convertToAngleRange0To360(angle + newOrientation + 360);
	}
	return convertToAngleRange0To360(angle + newOrientation);
}

//**********************End Helper Functions*************************//

Real CInsectbotCircleFovSense::calcM(Real point1[2], Real point2[2])
{
	Real x = (point1[X] - point2[X]);
	if (x == 0)
	{
		return INT_MAX;
	}
	Real res = (point1[Y] - point2[Y]) / x;
	return res;
}

Real CInsectbotCircleFovSense::calcB(Real point[2], Real m)
{
	return point[Y] - m * point[X];
}

// calculate 4 lines defining a rectangle
void CInsectbotCircleFovSense::calcLines(Real rightFront[2], Real leftFront[2], Real rightBack[2], Real leftBack[2], Line lines[4])
{
	Line front;
	front.m = calcM(rightFront, leftFront);
	front.b = calcB(rightFront, front.m);
	Line back;
	back.m = calcM(rightBack, leftBack);
	back.b = calcB(rightBack, back.m);
	Line right;
	right.m = calcM(rightFront, rightBack);
	right.b = calcB(rightFront, right.m);
	Line left;
	left.m = calcM(leftFront, leftBack);
	left.b = calcB(leftFront, left.m);

	lines[FRONT] = front;
	lines[BACK] = back;
	lines[RIGHT] = right;
	lines[LEFT] = left;
}

// calculate 4 points defining the nymbot's rectangle
void CInsectbotCircleFovSense::updateNymbot(Real x, Real y, CDegrees orientation, unsigned int id)
{
	CRadians radAngle = ToRadians(orientation);
	Real sin = argos::Sin(radAngle);
	Real cos = argos::Cos(radAngle);

	nymbots_data[id].nymbot.points[0][X] = x + cos * (FRONT_DIST) + sin * (SIDE_DIST);
	;
	nymbots_data[id].nymbot.points[0][Y] = y + sin * (FRONT_DIST)-cos * (SIDE_DIST);

	nymbots_data[id].nymbot.points[1][X] = x + cos * (FRONT_DIST)-sin * (SIDE_DIST);
	nymbots_data[id].nymbot.points[1][Y] = y + sin * (FRONT_DIST) + cos * (SIDE_DIST);

	nymbots_data[id].nymbot.points[2][X] = x - cos * (BACK_DIST) + sin * (SIDE_DIST);
	nymbots_data[id].nymbot.points[2][Y] = y - sin * (BACK_DIST)-cos * (SIDE_DIST);

	nymbots_data[id].nymbot.points[3][X] = x - cos * (BACK_DIST)-sin * (SIDE_DIST);
	nymbots_data[id].nymbot.points[3][Y] = y - sin * (BACK_DIST) + cos * (SIDE_DIST);

	calcLines(nymbots_data[id].nymbot.points[0], nymbots_data[id].nymbot.points[1], nymbots_data[id].nymbot.points[2], nymbots_data[id].nymbot.points[3], nymbots_data[id].nymbot.lines);
}

/**
 * @brief Updates the data of the Nymbots.
 *
 * This function updates the data of the Nymbots based on the provided array of Nymbot location data.
 *
 * @param nymbots_arr The array of Nymbot location data.
 */
void CInsectbotCircleFovSense::updateNymbotsData(nymbot_location_data nymbots_arr[])
{
	for (unsigned int i = 0; i < robotNumber; i++)
	{
		updateNymbot(nymbots_arr[i].x, nymbots_arr[i].y, nymbots_arr[i].orientation, i);
	}
}

/**
 * @brief Creates a list of rays to be drawn for the circle field of view (FOV) sensing in the insectbot.
 *
 * This function generates a list of rays that represent the field of view (FOV) of the insectbot's circle sensor.
 * The rays are used for visualization purposes, allowing the user to see the coverage of the sensor.
 *
 * @return void
 */
void CInsectbotCircleFovSense::createDrawRayList()
{
	// clear from last frame data
	for (unsigned int id = 0; id < robotNumber; id++)
	{
		drawRayList[id].clear();
	}
	for (unsigned int id = 0; id < robotNumber; id++)
	{
		for (auto it = nymbots_data[id].robotsDetection.begin(); it != nymbots_data[id].robotsDetection.end(); it++)
		{
			drawRayList[id].push_back(DrawRay(CRay3(CVector3(it->vector1.point1[0], it->vector1.point1[1], Z_VAL), CVector3(it->vector1.point2[0], it->vector1.point2[1], Z_VAL)), VECTOR_COLOR));
			drawRayList[id].push_back(DrawRay(CRay3(CVector3(it->vector2.point1[0], it->vector2.point1[1], Z_VAL), CVector3(it->vector2.point2[0], it->vector2.point2[1], Z_VAL)), VECTOR_COLOR));
		}
		for (auto it = nymbots_data[id].obstaclesDetection.begin(); it != nymbots_data[id].obstaclesDetection.end(); it++)
		{
			drawRayList[id].push_back(DrawRay(CRay3(CVector3(it->vector1.point1[0], it->vector1.point1[1], Z_VAL), CVector3(it->vector1.point2[0], it->vector1.point2[1], Z_VAL)), OBSTACLE_VECTOR_COLOR));
			drawRayList[id].push_back(DrawRay(CRay3(CVector3(it->vector2.point1[0], it->vector2.point1[1], Z_VAL), CVector3(it->vector2.point2[0], it->vector2.point2[1], Z_VAL)), OBSTACLE_VECTOR_COLOR));
		}
	}
}

/**
 * Create the circles defining the nymbots field of view to draw on the arena.
 *
 * @param nymbots_arr An array of nymbot location data.
 */
void CInsectbotCircleFovSense::createDrawCircleList(nymbot_location_data nymbots_arr[])
{
	// clear from last frame data
	for (unsigned int id = 0; id < robotNumber; id++)
	{
		drawCircleList[id].clear();
	}
	for (unsigned int id = 0; id < robotNumber; id++)
	{
		drawCircleList[id].push_back(DrawCircle(CVector3(nymbots_arr[id].x, nymbots_arr[id].y, Z_VAL), RADIUS));
	}
}

/**
 * Prints the detected robots in the field of view. For debugging.
 */
void CInsectbotCircleFovSense::printRobotsDetection()
{
	printf("********\n");
	for (unsigned int id = 0; id < robotNumber; id++)
	{
		printf("robots detection data for robot %d: \n", id);
		// loop through all robots detected by robot "id"
		int i = 0;
		for (auto it = nymbots_data[id].robotsDetection.begin(); it != nymbots_data[id].robotsDetection.end(); it++)
		{
			printf("%d. robot %d: vector1: d= %f, m= %f -- vector2: d= %f, m= %f \n", i, it->id, it->vector1.direction, it->vector1.magnitude, it->vector2.direction, it->vector2.magnitude);
			i++;
		}
	}
}

/**
 * Creates a line with the given angle, x-coordinate, and y-coordinate.
 *
 * @param angle The angle of the line.
 * @param x The x-coordinate of the line.
 * @param y The y-coordinate of the line.
 * @return The created line.
 */
Line CInsectbotCircleFovSense::createLine(double angle, float x, float y)
{
	Line line;
	line.m = tan(angle / 180 * M_PI);
	Real point[2] = {x, y};
	line.b = calcB(point, line.m);
	return line;
}

/**
 * Finds the intersection point between two lines.
 *
 * @param line1 The first line.
 * @param line2 The second line.
 * @return A pair of doubles representing the coordinates of the intersection point.
 */
pair<double, double> CInsectbotCircleFovSense::findIntersectionPoint(Line line1, Line line2)
{
	if ((line1.m <= line2.m + EPSILON1) && (line1.m >= line2.m - EPSILON1))
	{
		return make_pair(INT_MAX, INT_MAX);
	}
	Real m1 = line1.m, b1 = line1.b;
	Real m2 = line2.m, b2 = line2.b;
	double x = (b2 - b1) / (m1 - m2);
	double y = m1 * x + b1;
	return make_pair(x, y);
}

/**
 * Finds the closest intersection distance between the given line and the four lines in the array.
 *
 * @param nymbots_arr The array of nymbot location data.
 * @param lines The array of lines.
 * @param points The array of points.
 * @param line The line to find the closest intersection distance with.
 * @param id The ID of the insectbot.
 * @return A pair containing the closest intersection distance and the coordinates of the closest intersection point.
 */
pair<double, pair<double, double>> CInsectbotCircleFovSense::findClosestIntersectionDistance(nymbot_location_data nymbots_arr[], Line lines[4], Real points[4][2], Line line, unsigned int id)
{
	pair<double, double> intersectionPoints[4];
	for (int i = 0; i < 4; i++)
	{
		intersectionPoints[i] = findIntersectionPoint(line, lines[i]);
	}
	double min = INT_MAX, temp;
	pair<double, double> closestPoint;
	for (int i = 0; i < 4; i++)
	{
		if (!isInsideRectangle(nymbots_arr, points, intersectionPoints[i].first, intersectionPoints[i].second))
		{
			continue;
		}
		temp = calcDistance(nymbots_arr[id].x, nymbots_arr[id].y, intersectionPoints[i].first, intersectionPoints[i].second);
		if (temp < min)
		{
			min = temp;
			closestPoint = intersectionPoints[i];
		}
	}
	return make_pair(min, closestPoint);
}

/**
 * Checks if a given point (x, y) is inside a rectangle defined by four points. By calculate the area's of the four triangles
 * defined by the given point and the rectangle's edges if the point is inside the rectangle - the rectangles area is equal
 * to the sum of the four triangles areas.
 *
 * @param nymbots_arr An array of nymbot location data.
 * @param points      An array of four points defining the rectangle.
 * @param x           The x-coordinate of the point to check.
 * @param y           The y-coordinate of the point to check.
 * @return            True if the point is inside the rectangle, false otherwise.
 */
bool CInsectbotCircleFovSense::isInsideRectangle(nymbot_location_data nymbots_arr[], Real points[4][2], double x, double y)
{
	double x1 = points[1][X], y1 = points[1][Y];
	double x2 = points[0][X], y2 = points[0][Y];
	double x3 = points[2][X], y3 = points[2][Y];
	double x4 = points[3][X], y4 = points[3][Y];

	if ((x <= x1 + EPSILON1 && x >= x1 - EPSILON1) && (y <= y1 + EPSILON1 && y >= y1 - EPSILON1))
	{
		return true;
	}
	if ((x <= x2 + EPSILON1 && x >= x2 - EPSILON1) && (y <= y2 + EPSILON1 && y >= y2 - EPSILON1))
	{
		return true;
	}
	if ((x <= x3 + EPSILON1 && x >= x3 - EPSILON1) && (y <= y3 + EPSILON1 && y >= y3 - EPSILON1))
	{
		return true;
	}
	if ((x <= x4 + EPSILON1 && x >= x4 - EPSILON1) && (y <= y4 + EPSILON1 && y >= y4 - EPSILON1))
	{
		return true;
	}

	// calculate the edges lengths
	double a1 = calcDistance(x1, y1, x2, y2);
	double a2 = calcDistance(x2, y2, x3, y3);
	double a3 = calcDistance(x3, y3, x4, y4);
	double a4 = calcDistance(x4, y4, x1, y1);

	// calculate the distances between the given point and each of the rectangle's vertices
	double b1 = calcDistance(x1, y1, x, y);
	double b2 = calcDistance(x2, y2, x, y);
	double b3 = calcDistance(x3, y3, x, y);
	double b4 = calcDistance(x4, y4, x, y);

	double rectangleArea = a1 * a2;

	// calculate the triangles areas using Heron's Formula
	double u1 = (a1 + b1 + b2) / 2;
	double u2 = (a2 + b2 + b3) / 2;
	double u3 = (a3 + b3 + b4) / 2;
	double u4 = (a4 + b4 + b1) / 2;
	double traingle1Area = sqrt(u1 * (u1 - a1) * (u1 - b1) * (u1 - b2));
	double traingle2Area = sqrt(u2 * (u2 - a2) * (u2 - b2) * (u2 - b3));
	double traingle3Area = sqrt(u3 * (u3 - a3) * (u3 - b3) * (u3 - b4));
	double traingle4Area = sqrt(u4 * (u4 - a4) * (u4 - b4) * (u4 - b1));

	traingle1Area = isnan(traingle1Area) ? 0 : traingle1Area;
	traingle2Area = isnan(traingle2Area) ? 0 : traingle2Area;
	traingle3Area = isnan(traingle3Area) ? 0 : traingle3Area;
	traingle4Area = isnan(traingle4Area) ? 0 : traingle4Area;

	// check if the point is inside the rectangle
	if ((rectangleArea <= (traingle1Area + traingle2Area + traingle3Area + traingle4Area) + EPSILON1) && (rectangleArea >= (traingle1Area + traingle2Area + traingle3Area + traingle4Area) - EPSILON1))
	{
		return true;
	}
	return false;
}

/**
 * @brief Creates rectangle detection data for the Insectbot Circle FOV Sense module.
 *
 * This function takes an array of nymbot location data, an ID, an array of lines, an array of points,
 * and a line as input parameters. It generates and returns an AngleDetectionData object that represents
 * the rectangle detection data.
 *
 * @param nymbots_arr An array of nymbot location data.
 * @param id The ID of the rectangle.
 * @param lines An array of lines representing the rectangle.
 * @param points An array of points representing the corners of the rectangle.
 * @param line A line representing the rectangle.
 * @return AngleDetectionData The generated rectangle detection data.
 */
AngleDetectionData CInsectbotCircleFovSense::createRectangleDetectionData(nymbot_location_data nymbots_arr[], unsigned int id, Line lines[4], Real points[4][2], Line line)
{
	AngleDetectionData obstacleData;
	pair<double, pair<double, double>> intersection = findClosestIntersectionDistance(nymbots_arr, lines, points, line, id);
	obstacleData.distance = intersection.first;
	obstacleData.point[X] = intersection.second.first;
	obstacleData.point[Y] = intersection.second.second;
	return obstacleData;
}

/**
 * Finds the intersection points (0,1 or 2) between a line segment and a circle.
 *
 * @param xCenter The x-coordinate of the center of the circle.
 * @param yCenter The y-coordinate of the center of the circle.
 * @param radius The radius of the circle.
 * @param x1 The x-coordinate of the first point of the line segment.
 * @param y1 The y-coordinate of the first point of the line segment.
 * @param x2 The x-coordinate of the second point of the line segment.
 * @param y2 The y-coordinate of the second point of the line segment.
 * @return A vector of pairs representing the intersection points between the line segment and the circle.
 *         If there are no intersections, an empty vector is returned.
 */
vector<pair<double, double>> CInsectbotCircleFovSense::findIntersectionBetweenLineAndCircle(double xCenter, double yCenter, double radius, double x1, double y1, double x2, double y2)
{
	vector<pair<double, double>> points;
	double tangentEpsilon = 1e-9;
	double dx1 = x1 - xCenter;
	double dy1 = y1 - yCenter;
	double dx2 = x2 - xCenter;
	double dy2 = y2 - yCenter;
	double dx = dx2 - dx1;
	double dy = dy2 - dy1;
	double dr = sqrt(pow(dx, 2) + pow(dy, 2));
	double bigD = dx1 * dy2 - dx2 * dy1;
	double discriminant = pow(radius, 2) * pow(dr, 2) - pow(bigD, 2);
	if (discriminant < 0)
	{ // no intersection
		return points;
	}
	else
	{
		int dy_sign = dy < 0 ? -1 : 1;
		points.push_back(make_pair(xCenter + (bigD * dy + dy_sign * dx * sqrt(discriminant)) / pow(dr, 2),
								   yCenter + (-bigD * dx + abs(dy) * sqrt(discriminant)) / pow(dr, 2)));
		points.push_back(make_pair(xCenter + (bigD * dy - dy_sign * dx * sqrt(discriminant)) / pow(dr, 2),
								   yCenter + (-bigD * dx - abs(dy) * sqrt(discriminant)) / pow(dr, 2)));

		if (points.size() == 2 && abs(discriminant) <= tangentEpsilon)
		{ // if the line is tangent to the circle, the two intersection points found are identical
			points.pop_back();
		}
		return points;
	}
}

AngleDetectionData CInsectbotCircleFovSense::createCylinderObstacleData(nymbot_location_data nymbots_arr[], unsigned int id, double position[2], double radius, Line line)
{
	AngleDetectionData obstacleData;
	// find another point on the line
	double x = nymbots_arr[id].x + 1;
	double y = line.m * x + line.b;
	// find distance between robots location to obstacle center
	vector<pair<double, double>> intersections = findIntersectionBetweenLineAndCircle(position[X], position[Y], radius, nymbots_arr[id].x, nymbots_arr[id].y, x, y);
	if (intersections.size() == 0)
	{
		obstacleData.distance = INT_MAX;
		return obstacleData;
	}
	double distance = calcDistance(nymbots_arr[id].x, nymbots_arr[id].y, intersections.at(0).first, intersections.at(0).second);
	int pointIdx = 0;
	if (intersections.size() == 2)
	{ // find the closest intersection point
		double secondDistance = calcDistance(nymbots_arr[id].x, nymbots_arr[id].y, intersections.at(1).first, intersections.at(1).second);
		if (secondDistance < distance)
		{
			distance = secondDistance;
			pointIdx = 1;
		}
	}
	obstacleData.distance = distance;
	obstacleData.point[X] = intersections.at(pointIdx).first;
	obstacleData.point[Y] = intersections.at(pointIdx).second;
	return obstacleData;
}

/**
 * @brief Creates circle obstacle data for the CInsectbotCircleFovSense class.
 *
 * This function calculates the obstacle data for a circle obstacle based on the given parameters.
 * It finds the intersection points between the line and the circle, and calculates the distances
 * from the robot's location to the intersection points. The obstacle data includes the distances
 * and the coordinates of the intersection points.
 *
 * @param nymbots_arr An array of nymbot_location_data representing the robot locations.
 * @param id The ID of the current robot.
 * @param position The position of the obstacle center.
 * @param radius The radius of the obstacle.
 * @param line The line representing the direction of the robot's FOV.
 * @return A pair of AngleDetectionData representing the obstacle data.
 */
pair<AngleDetectionData, AngleDetectionData> CInsectbotCircleFovSense::createCircleObstacleData(nymbot_location_data nymbots_arr[], unsigned int id, double position[2], double radius, Line line)
{ // pair/vector?
	AngleDetectionData obstacleData1;
	AngleDetectionData obstacleData2;
	// find another point on the line
	double x = nymbots_arr[id].x + 1;
	double y = line.m * x + line.b;
	// find distance between robots location to obstacle center
	vector<pair<double, double>> intersections = findIntersectionBetweenLineAndCircle(position[X], position[Y], radius, nymbots_arr[id].x, nymbots_arr[id].y, x, y);
	if (intersections.size() == 0)
	{
		obstacleData1.distance = INT_MAX;
		obstacleData2.distance = INT_MAX;
		return make_pair(obstacleData1, obstacleData2);
	}
	double distance1 = calcDistance(nymbots_arr[id].x, nymbots_arr[id].y, intersections.at(0).first, intersections.at(0).second);
	double distance2 = calcDistance(nymbots_arr[id].x, nymbots_arr[id].y, intersections.at(1).first, intersections.at(1).second);

	obstacleData1.distance = distance1;
	obstacleData1.point[X] = intersections.at(0).first;
	obstacleData1.point[Y] = intersections.at(0).second;

	obstacleData2.distance = distance2;
	obstacleData2.point[X] = intersections.at(1).first;
	obstacleData2.point[Y] = intersections.at(1).second;

	return obstacleData1.point[Y] > obstacleData2.point[Y] ? make_pair(obstacleData1, obstacleData2) : make_pair(obstacleData2, obstacleData1);
}

// update anglesArray with detection data, starting from angle until the nymbot don't detect the obstacle/nymbot "id2"
pair<NymbotVector, NymbotVector> CInsectbotCircleFovSense::updateAnglesArray(nymbot_location_data nymbots_arr[], unsigned int id, vector<AngleDetectionData> anglesArray[ANGLES_ARRAY_SIZE], Rectangle rectangle, Cylinder cylinder, Circle circle, double maxDist, int angle, bool add, bool isRectangle, bool isCylinder, bool isCircle, unsigned int id2, int type2)
{
	// adjust angle to range 0-ANGLES_ARRAY_SIZE
	if (add && (angle >= ANGLES_ARRAY_SIZE))
	{
		angle = 0;
	}
	else if (!add && (angle <= 0))
	{
		angle = ANGLES_ARRAY_SIZE - 1;
	}
	bool stop = false, found = false;
	// vector1 is the vector to the first point detected, vector2 is the vector to the last point detected
	NymbotVector vector1, vector2;
	while (!stop)
	{
		bool tanFlag = false;
		double tempAngle;
		// adjust angle to range 0-ANGLES_ARRAY_SIZE
		if (add && (angle == ANGLES_ARRAY_SIZE))
		{
			angle = 0;
		}
		else if (!add && (angle == -1))
		{
			angle = ANGLES_ARRAY_SIZE - 1;
		}
		AngleDetectionData angleData;
		// handle tan of degrees 90 and 270
		if (angle == 90 * ANGLE_ADJUSTMENT || angle == 270 * ANGLE_ADJUSTMENT)
		{
			tanFlag = true;
			tempAngle = angle + EPSILON1;
		}
		// create line defined by robot "id"'s location and angle
		Line l;
		if (tanFlag)
		{ // create the line with tempAngle instead of angle
			l = createLine(tempAngle / ANGLE_ADJUSTMENT, nymbots_arr[id].x, nymbots_arr[id].y);
		}
		else
		{
			l = createLine((double)angle / ANGLE_ADJUSTMENT, nymbots_arr[id].x, nymbots_arr[id].y);
		}
		if (isRectangle)
		{
			angleData = createRectangleDetectionData(nymbots_arr, id, rectangle.lines, rectangle.points, l);
		}
		if (isCylinder)
		{
			angleData = createCylinderObstacleData(nymbots_arr, id, cylinder.position, cylinder.radius, l);
		}
		if (isCircle)
		{
			angleData = createCylinderObstacleData(nymbots_arr, id, circle.position, circle.radius, l);
		}
		if (angleData.distance == INT_MAX)
		{ // no intersection with the obstacle/nymbot in this angle
			stop = true;
		}
		else
		{
			if (angleData.distance <= maxDist)
			{ // check if the intesrection is inside robot "id"s fov
				if (anglesArray[angle].at(0).distance == -1)
				{
					anglesArray[angle].pop_back();
				}
                angleData.type = type2;
				angleData.id = id2;
				angleData.orientation = CDegrees(30);
				anglesArray[angle].push_back(angleData);
				if (found == false)
				{ // first point detected
					vector1.direction = angle / ANGLE_ADJUSTMENT;
					vector1.magnitude = anglesArray[angle].back().distance;
					vector1.point1[X] = nymbots_arr[id].x;
					vector1.point1[Y] = nymbots_arr[id].y;
					vector1.point2[X] = anglesArray[angle].back().point[X];
					vector1.point2[Y] = anglesArray[angle].back().point[Y];
				}
				found = true;
			}
			else
			{ // the intesrection is outside robot "id"s fov
				stop = true;
			}
		}
		angle = add ? (angle + 1) : (angle - 1);
	}
	if (found)
	{ // if the obstacle/nymbot was detected at least at one point
		angle = add ? (angle - 2) : (angle + 2);
		// adjust angle to range 0-ANGLES_ARRAY_SIZE
		if (!add && (angle >= ANGLES_ARRAY_SIZE))
		{
			angle = 0;
		}
		else if (add && (angle < 0))
		{
			angle = ANGLES_ARRAY_SIZE - 1;
		}
		// last point detected
		vector2.direction = angle / ANGLE_ADJUSTMENT;
		vector2.magnitude = anglesArray[angle].back().distance;
		vector2.point1[X] = nymbots_arr[id].x;
		vector2.point1[Y] = nymbots_arr[id].y;
		vector2.point2[X] = anglesArray[angle].back().point[X];
		vector2.point2[Y] = anglesArray[angle].back().point[Y];
	}
	else
	{ // if the obstacle/nymbot was not detected at all
		vector1.magnitude = -1;
	}
	return make_pair(vector1, vector2);
}

// update anglesArray with detection data, starting from angle until the nymbot don't detect the obstacle/nymbot "id2"
pair<NymbotVector, NymbotVector> CInsectbotCircleFovSense::updateAnglesArray(nymbot_location_data nymbots_arr[], unsigned int id, vector<AngleDetectionData> anglesArray[ANGLES_ARRAY_SIZE], Rectangle rectangle, Cylinder cylinder, Circle circle, double maxDist, int angle, bool add, bool isRectangle, bool isCylinder, bool isCircle, unsigned int id2, int type2, CDegrees orientation)
{
	// adjust angle to range 0-ANGLES_ARRAY_SIZE
	if (add && (angle >= ANGLES_ARRAY_SIZE))
	{
		angle = 0;
	}
	else if (!add && (angle <= 0))
	{
		angle = ANGLES_ARRAY_SIZE - 1;
	}
	bool stop = false, found = false;
	// vector1 is the vector to the first point detected, vector2 is the vector to the last point detected
	NymbotVector vector1, vector2;
	while (!stop)
	{
		bool tanFlag = false;
		double tempAngle;
		// adjust angle to range 0-ANGLES_ARRAY_SIZE
		if (add && (angle == ANGLES_ARRAY_SIZE))
		{
			angle = 0;
		}
		else if (!add && (angle == -1))
		{
			angle = ANGLES_ARRAY_SIZE - 1;
		}
		AngleDetectionData angleData;
		// handle tan of degrees 90 and 270
		if (angle == 90 * ANGLE_ADJUSTMENT || angle == 270 * ANGLE_ADJUSTMENT)
		{
			tanFlag = true;
			tempAngle = angle + EPSILON1;
		}
		// create line defined by robot "id"'s location and angle
		Line l;
		if (tanFlag)
		{ // create the line with tempAngle instead of angle
			l = createLine(tempAngle / ANGLE_ADJUSTMENT, nymbots_arr[id].x, nymbots_arr[id].y);
		}
		else
		{
			l = createLine((double)angle / ANGLE_ADJUSTMENT, nymbots_arr[id].x, nymbots_arr[id].y);
		}
		if (isRectangle)
		{
			angleData = createRectangleDetectionData(nymbots_arr, id, rectangle.lines, rectangle.points, l);
		}
		if (isCylinder)
		{
			angleData = createCylinderObstacleData(nymbots_arr, id, cylinder.position, cylinder.radius, l);
		}
		if (isCircle)
		{
			angleData = createCylinderObstacleData(nymbots_arr, id, circle.position, circle.radius, l);
		}
		if (angleData.distance == INT_MAX)
		{ // no intersection with the obstacle/nymbot in this angle
			stop = true;
		}
		else
		{
			if (angleData.distance <= maxDist)
			{ // check if the intesrection is inside robot "id"s fov
				if (anglesArray[angle].at(0).distance == -1)
				{
					anglesArray[angle].pop_back();
				}
                angleData.type = type2;
				angleData.id = id2;
				angleData.orientation = orientation;
				anglesArray[angle].push_back(angleData);
				if (found == false)
				{ // first point detected
					vector1.direction = angle / ANGLE_ADJUSTMENT;
					vector1.magnitude = anglesArray[angle].back().distance;
					vector1.point1[X] = nymbots_arr[id].x;
					vector1.point1[Y] = nymbots_arr[id].y;
					vector1.point2[X] = anglesArray[angle].back().point[X];
					vector1.point2[Y] = anglesArray[angle].back().point[Y];
				}
				found = true;
			}
			else
			{ // the intesrection is outside robot "id"s fov
				stop = true;
			}
		}
		angle = add ? (angle + 1) : (angle - 1);
	}
	if (found)
	{ // if the obstacle/nymbot was detected at least at one point
		angle = add ? (angle - 2) : (angle + 2);
		// adjust angle to range 0-ANGLES_ARRAY_SIZE
		if (!add && (angle >= ANGLES_ARRAY_SIZE))
		{
			angle = 0;
		}
		else if (add && (angle < 0))
		{
			angle = ANGLES_ARRAY_SIZE - 1;
		}
		// last point detected
		vector2.direction = angle / ANGLE_ADJUSTMENT;
		vector2.magnitude = anglesArray[angle].back().distance;
		vector2.point1[X] = nymbots_arr[id].x;
		vector2.point1[Y] = nymbots_arr[id].y;
		vector2.point2[X] = anglesArray[angle].back().point[X];
		vector2.point2[Y] = anglesArray[angle].back().point[Y];
	}
	else
	{ // if the obstacle/nymbot was not detected at all
		vector1.magnitude = -1;
	}
	return make_pair(vector1, vector2);
}

// update anglesArray with detection data, starting from angle until the nymbot don't detect the circle
NymbotVector CInsectbotCircleFovSense::updateCircleAngles(nymbot_location_data nymbots_arr[], unsigned int id, Circle circle, double maxDist)
{
	for (int i = 0; i < ANGLES_ARRAY_SIZE / 2; i++)
	{
		// create line defined by robot "id"'s location and angle i
		Line l = createLine((double)i / ANGLE_ADJUSTMENT, nymbots_arr[id].x, nymbots_arr[id].y);
		pair<AngleDetectionData, AngleDetectionData> obstacleData = createCircleObstacleData(nymbots_arr, id, circle.position, circle.radius, l);
		if (obstacleData.first.distance <= maxDist)
		{ // check if the first intesrection is inside robot "id"s fov
			if (nymbots_data[id].obstaclesAngles[i].at(0).distance == -1)
			{
				nymbots_data[id].obstaclesAngles[i].pop_back();
			}
			nymbots_data[id].obstaclesAngles[i].push_back(obstacleData.first);
		}
		if (obstacleData.second.distance <= maxDist)
		{ // check if the second intesrection is inside robot "id"s fov
			if (nymbots_data[id].obstaclesAngles[i + ANGLES_ARRAY_SIZE / 2].at(0).distance == -1)
			{
				nymbots_data[id].obstaclesAngles[i + ANGLES_ARRAY_SIZE / 2].pop_back();
			}
			nymbots_data[id].obstaclesAngles[i + ANGLES_ARRAY_SIZE / 2].push_back(obstacleData.second);
		}
	}
	NymbotVector vector;
	vector.direction = 0;
	vector.magnitude = nymbots_data[id].obstaclesAngles[0].back().distance;
	vector.point1[X] = nymbots_arr[id].x;
	vector.point1[Y] = nymbots_arr[id].y;
	vector.point2[X] = nymbots_data[id].obstaclesAngles[0].back().point[X];
	vector.point2[Y] = nymbots_data[id].obstaclesAngles[0].back().point[Y];
	return vector;
}

// get the angle of the line defined by the (xRobot, yRobot) and (xObstacle, yObstacle) points
int CInsectbotCircleFovSense::getCenterAngle(double xRobot, double yRobot, double xObstacle, double yObstacle)
{
	Real robotPosition[2] = {xRobot, yRobot};
	Real obstaclePosition[2] = {xObstacle, yObstacle};
	double tempAngle = atan2(robotPosition[Y] - obstaclePosition[Y], robotPosition[X] - obstaclePosition[X]) * 180 / M_PI;
	tempAngle = digitRound(tempAngle, 3);
	// adjust result given by atan2 to the range 0-360 (and set 360 angle to be 0)
	tempAngle += 180;
	if (tempAngle == 360)
	{
		tempAngle = 0;
	}
	int centerAngle = floor(tempAngle * ANGLE_ADJUSTMENT);
	return centerAngle;
}

/********************** Find objects **********************/

// find all box obstacles
void CInsectbotCircleFovSense::findBoxes(nymbot_location_data nymbots_arr[], unsigned int id, Obstacles obstacles, double maxDist)
{
	for (auto it = obstacles.boxes.begin(); it != obstacles.boxes.end(); it++)
	{
		// find the angle of the line between the nymbot's center and the box's center
		int centerAngle = getCenterAngle(nymbots_arr[id].x, nymbots_arr[id].y, it->position[X], it->position[Y]);
		DetectionData data;
		// update anglesArray from the two sides of centerAngle
		pair<NymbotVector, NymbotVector> vectors1 = updateAnglesArray(nymbots_arr, id, nymbots_data[id].obstaclesAngles, it->box, Cylinder(), Circle(), maxDist, centerAngle - 1, false, true, false, false, it->id, BOX);
		pair<NymbotVector, NymbotVector> vectors2 = updateAnglesArray(nymbots_arr, id, nymbots_data[id].obstaclesAngles, it->box, Cylinder(), Circle(), maxDist, centerAngle, true, true, false, false, it->id, BOX);
		if (vectors1.first.magnitude == -1 && vectors2.first.magnitude == -1)
		{ // the box was not detected in both sides
			continue;
		}
		if (vectors1.first.magnitude == -1)
		{ // the box was detected only in the angles above centerAngle
			data.vector1 = vectors2.first;
			data.vector2 = vectors2.second;
		}
		else if (vectors2.first.magnitude == -1)
		{ // the box was detected only in the angles below centerAngle
			data.vector1 = vectors1.first;
			data.vector2 = vectors1.second;
		}
		else
		{ // the box was detected in both sides
			data.vector1 = vectors1.second;
			data.vector2 = vectors2.second;
		}
		data.type = BOX;
        // TODO: when we have multiple boxes we need an id for each one!
		data.id = BOX;
		nymbots_data[id].obstaclesDetection.push_back(data);
	}
}

// find all walls obstacles
void CInsectbotCircleFovSense::findWalls(nymbot_location_data nymbots_arr[], unsigned int id, Obstacles obstacles, double maxDist)
{
	for (auto it = obstacles.walls.begin(); it != obstacles.walls.end(); it++)
	{
		int centerAngle = 0;
		Real normOri = normOrientation(it->orientation); // [0, 360)
		DetectionData data;

        if ((normOri <= 0 + EPSILON1 && normOri >= 0 - EPSILON1) || (normOri <= 90 + EPSILON1 && normOri >= 90 - EPSILON1))
            centerAngle = normOri * ANGLE_ADJUSTMENT;

        data.type = WALL;
        data.id = it->id;
		data.orientation = it->orientation;
		// update anglesArray from the two sides of centerAngle
		pair<NymbotVector, NymbotVector> vectors1 = updateAnglesArray(nymbots_arr, id, nymbots_data[id].obstaclesAngles, it->box, Cylinder(), Circle(), maxDist, centerAngle - 1, false, true, false, false, data.id, data.type, it->orientation);
		pair<NymbotVector, NymbotVector> vectors2 = updateAnglesArray(nymbots_arr, id, nymbots_data[id].obstaclesAngles, it->box, Cylinder(), Circle(), maxDist, centerAngle, true, true, false, false, data.id, data.type, it->orientation);
		if (vectors1.first.magnitude == -1 && vectors2.first.magnitude == -1)
		{ // the wall was not detected in both sides
			continue;
		}
		if (vectors1.first.magnitude == -1)
		{ // the wall was detected only in the angles above centerAngle
			data.vector1 = vectors2.first;
			data.vector2 = vectors2.second;
		}
		else if (vectors2.first.magnitude == -1)
		{ // the wall was detected only in the angles below centerAngle
			data.vector1 = vectors1.first;
			data.vector2 = vectors1.second;
		}
		else
		{ // the wall was detected in both sides
			data.vector1 = vectors1.second;
			data.vector2 = vectors2.second;
		}
		nymbots_data[id].obstaclesDetection.push_back(data);
	}
}

// find all cylinders obstacles
void CInsectbotCircleFovSense::findCylinders(nymbot_location_data nymbots_arr[], unsigned int id, Obstacles obstacles, double maxDist)
{
	for (auto it = obstacles.cylinders.begin(); it != obstacles.cylinders.end(); it++)
	{
		// find the angle of the line between the nymbot's center and the box's center
		int centerAngle = getCenterAngle(nymbots_arr[id].x, nymbots_arr[id].y, it->position[X], it->position[Y]);
		DetectionData data;
		// update anglesArray from the two sides of centerAngle
		pair<NymbotVector, NymbotVector> vectors1 = updateAnglesArray(nymbots_arr, id, nymbots_data[id].obstaclesAngles, Rectangle(), *it, Circle(), maxDist, centerAngle - 1, false, false, true, false, it->id, CYLINDER);
		pair<NymbotVector, NymbotVector> vectors2 = updateAnglesArray(nymbots_arr, id, nymbots_data[id].obstaclesAngles, Rectangle(), *it, Circle(), maxDist, centerAngle, true, false, true, false, it->id, CYLINDER);
		if (vectors1.first.magnitude == -1 && vectors2.first.magnitude == -1)
		{ // the cylinder was not detected in both sides
			continue;
		}
		if (vectors1.first.magnitude == -1)
		{ // the cylinder was detected only in the angles above centerAngle
			data.vector1 = vectors2.first;
			data.vector2 = vectors2.second;
		}
		else if (vectors2.first.magnitude == -1)
		{ // the cylinder was detected only in the angles below centerAngle
			data.vector1 = vectors1.first;
			data.vector2 = vectors1.second;
		}
		else
		{ // the cylinder was detected in both sides
			data.vector1 = vectors1.second;
			data.vector2 = vectors2.second;
		}
		data.type = CYLINDER;
		data.id = it->id;
		nymbots_data[id].obstaclesDetection.push_back(data);
	}
}

// find all circles obstacles
void CInsectbotCircleFovSense::findCircles(nymbot_location_data nymbots_arr[], unsigned int id, Obstacles obstacles, double maxDist)
{
	for (auto it = obstacles.circles.begin(); it != obstacles.circles.end(); it++)
	{
		// calculate distance between the robot's center and the circle's center
		double distance = calcDistance(nymbots_arr[id].x, nymbots_arr[id].y, it->position[X], it->position[Y]);
		DetectionData data;
		data.type = CIRCLE;
		data.id = it->id;
		if (distance <= it->radius)
		{ // robot is inside the circle
			data.vector1 = updateCircleAngles(nymbots_arr, id, *it, maxDist);
			data.vector2 = data.vector1;
		}
		else
		{ // robot is outside the circle - in this case the circle is like a cylinder
			// find the angle of the line between the nymbot's center and the box's center
			int centerAngle = getCenterAngle(nymbots_arr[id].x, nymbots_arr[id].y, it->position[X], it->position[Y]);
			// update anglesArray from the two sides of centerAngle
			pair<NymbotVector, NymbotVector> vectors1 = updateAnglesArray(nymbots_arr, id, nymbots_data[id].obstaclesAngles, Rectangle(), Cylinder(), *it, maxDist, centerAngle - 1, false, false, false, true, it->id, CIRCLE);
			pair<NymbotVector, NymbotVector> vectors2 = updateAnglesArray(nymbots_arr, id, nymbots_data[id].obstaclesAngles, Rectangle(), Cylinder(), *it, maxDist, centerAngle, true, false, false, true, it->id, CIRCLE);
			if (vectors1.first.magnitude == -1 && vectors2.first.magnitude == -1)
			{ // the circle was not detected in both sides
				continue;
			}
			if (vectors1.first.magnitude == -1)
			{ // the circle was detected only in the angles above centerAngle
				data.vector1 = vectors2.first;
				data.vector2 = vectors2.second;
			}
			else if (vectors2.first.magnitude == -1)
			{ // the circle was detected only in the angles below centerAngle
				data.vector1 = vectors1.first;
				data.vector2 = vectors1.second;
			}
			else
			{ // the circle was detected in both sides
				data.vector1 = vectors1.second;
				data.vector2 = vectors2.second;
			}
		}
		nymbots_data[id].obstaclesDetection.push_back(data);
	}
}

void CInsectbotCircleFovSense::findObstacles(nymbot_location_data nymbots_arr[], Obstacles obstacles)
{
	for (unsigned int id = 0; id < robotNumber; id++)
	{
		nymbots_data[id].obstaclesDetection.clear();
	}
	for (unsigned int id = 0; id < robotNumber; id++)
	{
		for (int i = 0; i < ANGLES_ARRAY_SIZE; i++)
		{
			AngleDetectionData obstacleData = {-1, -1, {INT_MAX, INT_MAX}};
			nymbots_data[id].obstaclesAngles[i].clear();
			nymbots_data[id].obstaclesAngles[i].push_back(obstacleData);
		}
		findBoxes(nymbots_arr, id, obstacles, RADIUS);
		findCylinders(nymbots_arr, id, obstacles, RADIUS);
		findCircles(nymbots_arr, id, obstacles, RADIUS);
		findWalls(nymbots_arr, id, obstacles, RADIUS);
		// for each angle in obstaclesAngles, sort the obstacles detected by distance
		for (int i = 0; i < ANGLES_ARRAY_SIZE; i++)
		{
			sort(
				nymbots_data[id].obstaclesAngles[i].begin(), nymbots_data[id].obstaclesAngles[i].end(), [](AngleDetectionData &od1, AngleDetectionData &od2) -> bool
				{ return od1.distance < od2.distance; });
		}
	}
	createDrawCircleList(nymbots_arr);
	createDrawRayList();
}

/********************** Find objects end **********************/

// get the vector that stretches from robot "id"'s center to the extreme edge of the rectangle
NymbotVector CInsectbotCircleFovSense::getExtremeVector(nymbot_location_data nymbots_arr[], unsigned int id, Rectangle rectangle, int angle, bool add)
{
	// adjust angle to range 0-ANGLES_ARRAY_SIZE
	if (add && (angle >= ANGLES_ARRAY_SIZE))
	{
		angle = 0;
	}
	else if (!add && (angle <= 0))
	{
		angle = ANGLES_ARRAY_SIZE - 1;
	}
	bool stop = false;
	AngleDetectionData detectionData = {-1, -1, {INT_MAX, INT_MAX}};
	while (!stop)
	{
		// adjust angle to range 0-ANGLES_ARRAY_SIZE
		if (add && (angle == ANGLES_ARRAY_SIZE))
		{
			angle = 0;
		}
		else if (!add && (angle == 0))
		{
			angle = ANGLES_ARRAY_SIZE - 1;
		}
		AngleDetectionData temp;
		// create line defined by robot "id"'s location and angle
		Line l = createLine((double)angle / ANGLE_ADJUSTMENT, nymbots_arr[id].x, nymbots_arr[id].y);
		temp = createRectangleDetectionData(nymbots_arr, id, rectangle.lines, rectangle.points, l);
		if (temp.distance == INT_MAX)
		{ // no intersection with the obstacle in this angle
			stop = true;
		}
		else
		{
			detectionData = temp;
			angle = add ? (angle + 1) : (angle - 1);
		}
	}
	angle = add ? (angle - 1) : (angle + 1);
	// create a vector to the last point detected
	NymbotVector vector;
	vector.direction = angle / ANGLE_ADJUSTMENT;
	vector.magnitude = detectionData.distance;
	vector.point1[X] = nymbots_arr[id].x;
	vector.point1[Y] = nymbots_arr[id].y;
	vector.point2[X] = detectionData.point[X];
	vector.point2[Y] = detectionData.point[Y];
	return vector;
}

// find all nymbots detected
void CInsectbotCircleFovSense::DetectNymbots(nymbot_location_data nymbots_arr[], unsigned int id, double maxDist, int model)
{
	for (unsigned int id2 = 0; id2 < robotNumber; id2++)
	{
		if (id2 == id)
		{
			continue;
		}
		// find the angle of the line between the nymbot's "id" center and nymbot's "id2" center
		int centerAngle = getCenterAngle(nymbots_arr[id].x, nymbots_arr[id].y, nymbots_arr[id2].x, nymbots_arr[id2].y);
		DetectionData data;
		// update anglesArray from the two sides of centerAngle
		pair<NymbotVector, NymbotVector> vectors1 = updateAnglesArray(nymbots_arr, id, nymbots_data[id].robotsAngles, nymbots_data[id2].nymbot, Cylinder(), Circle(), maxDist, centerAngle - 1, false, true, false, false, id2, RECTANGLE);
		pair<NymbotVector, NymbotVector> vectors2 = updateAnglesArray(nymbots_arr, id, nymbots_data[id].robotsAngles, nymbots_data[id2].nymbot, Cylinder(), Circle(), maxDist, centerAngle, true, true, false, false, id2, RECTANGLE);
		if (vectors1.first.magnitude == -1 && vectors2.first.magnitude == -1)
		{ // the nymbot was not detected in both sides
			continue;
		}
		if (vectors1.first.magnitude == -1)
		{ // the nymbot was detected only in the angles above centerAngle
			data.vector1 = vectors2.first;
			data.vector2 = vectors2.second;
		}
		else if (vectors2.first.magnitude == -1)
		{ // the nymbot was detected only in the angles below centerAngle
			data.vector1 = vectors1.first;
			data.vector2 = vectors1.second;
		}
		else
		{ // the nymbot was detected in both sides
			data.vector1 = vectors1.second;
			data.vector2 = vectors2.second;
		}
		data.id = id2;
		nymbots_data[id].robotsDetection.push_back(data);
	}
}

/********************** occlusions **********************/
// TODO: move to a new file

/**
 * @brief Performs the x-ray model calculation for the CInsectbotCircleFovSense class.
 *
 * This function calculates the x-ray model for the given insectbot's circle field of view (FOV) sensing.
 * It iterates through the robots detected by the insectbot and updates the angles array based on the extreme vectors.
 * The extreme vectors are calculated based on the angle between the center of the insectbot and the center of the detected robot.
 * The function handles cases where angle "0" is between the extreme vectors or not.
 *
 * @param nymbots_arr The array of nymbot location data.
 * @param id The ID of the insectbot.
 */
void CInsectbotCircleFovSense::xRayModel(nymbot_location_data nymbots_arr[], unsigned int id)
{
	for (auto it = nymbots_data[id].robotsDetection.begin(); it != nymbots_data[id].robotsDetection.end(); it++)
	{
		// find the angle of the line between the nymbot "id"s center and nymbot "it->id"s center
		int centerAngle = getCenterAngle(nymbots_arr[id].x, nymbots_arr[id].y, nymbots_arr[it->id].x, nymbots_arr[it->id].y);
		// get the extreme vectors of nymbot "it->id"
		NymbotVector extremeVector1 = getExtremeVector(nymbots_arr, id, nymbots_data[it->id].nymbot, centerAngle - 1, false);
		NymbotVector extremeVector2 = getExtremeVector(nymbots_arr, id, nymbots_data[it->id].nymbot, centerAngle, true);
		if (extremeVector1.direction < extremeVector2.direction)
		{ // if angle "0" is not between the extreme vectors
			if (it->vector1.direction > extremeVector1.direction)
			{
				updateAnglesArray(nymbots_arr, id, nymbots_data[id].robotsAngles, nymbots_data[it->id].nymbot, Cylinder(), Circle(), INT_MAX, floor(it->vector1.direction * ANGLE_ADJUSTMENT) - 1, false, true, false, false, it->id, it->type);
				it->vector1 = extremeVector1;
			}
			if (it->vector2.direction < extremeVector2.direction)
			{
				updateAnglesArray(nymbots_arr, id, nymbots_data[id].robotsAngles, nymbots_data[it->id].nymbot, Cylinder(), Circle(), INT_MAX, floor(it->vector2.direction * ANGLE_ADJUSTMENT) + 1, true, true, false, false, it->id, it->type);
				it->vector2 = extremeVector2;
			}
		}
		else
		{ // if angle "0" is between the extreme vectors
			if (it->vector1.direction < extremeVector1.direction)
			{ // angle "0" is between vector1 and extremeVector1
				updateAnglesArray(nymbots_arr, id, nymbots_data[id].robotsAngles, nymbots_data[it->id].nymbot, Cylinder(), Circle(), INT_MAX, floor(it->vector1.direction * ANGLE_ADJUSTMENT) - 1, false, true, false, false, it->id, it->type);
				it->vector1 = extremeVector1;
				if (it->vector2.direction < extremeVector2.direction)
				{
					updateAnglesArray(nymbots_arr, id, nymbots_data[id].robotsAngles, nymbots_data[it->id].nymbot, Cylinder(), Circle(), INT_MAX, floor(it->vector2.direction * ANGLE_ADJUSTMENT) + 1, true, true, false, false, it->id, it->type);
					it->vector2 = extremeVector2;
				}
			}
			else if (it->vector2.direction > extremeVector2.direction)
			{ // angle "0" is between vector2 and extremeVector2
				updateAnglesArray(nymbots_arr, id, nymbots_data[id].robotsAngles, nymbots_data[it->id].nymbot, Cylinder(), Circle(), INT_MAX, floor(it->vector2.direction * ANGLE_ADJUSTMENT) + 1, true, true, false, false, it->id, it->type);
				it->vector2 = extremeVector2;
				if (it->vector1.direction > extremeVector1.direction)
				{
					updateAnglesArray(nymbots_arr, id, nymbots_data[id].robotsAngles, nymbots_data[it->id].nymbot, Cylinder(), Circle(), INT_MAX, floor(it->vector1.direction * ANGLE_ADJUSTMENT) - 1, false, true, false, false, it->id, it->type);
					it->vector1 = extremeVector1;
				}
			}
			else
			{ // angle "0" is between vector1 and vector2
				if (it->vector1.direction > extremeVector1.direction)
				{
					updateAnglesArray(nymbots_arr, id, nymbots_data[id].robotsAngles, nymbots_data[it->id].nymbot, Cylinder(), Circle(), INT_MAX, floor(it->vector1.direction * ANGLE_ADJUSTMENT) - 1, false, true, false, false, it->id, it->type);
					it->vector1 = extremeVector1;
				}
				if (it->vector2.direction < extremeVector2.direction)
				{
					updateAnglesArray(nymbots_arr, id, nymbots_data[id].robotsAngles, nymbots_data[it->id].nymbot, Cylinder(), Circle(), INT_MAX, floor(it->vector2.direction * ANGLE_ADJUSTMENT) + 1, true, true, false, false, it->id, it->type);
					it->vector2 = extremeVector2;
				}
			}
		}
	}
}

// part of a robot != robot
void CInsectbotCircleFovSense::ignorePartiallyHiddenModel(nymbot_location_data nymbots_arr[], unsigned int id)
{
	for (auto it = nymbots_data[id].robotsDetection.begin(); it != nymbots_data[id].robotsDetection.end(); it++)
	{
		// find the angle of the line between the nymbot "id"s center and nymbot "it->id"s center
		int centerAngle = getCenterAngle(nymbots_arr[id].x, nymbots_arr[id].y, nymbots_arr[it->id].x, nymbots_arr[it->id].y);
		// get the extreme vectors of nymbot "it->id"
		NymbotVector extremeVector1 = getExtremeVector(nymbots_arr, id, nymbots_data[it->id].nymbot, centerAngle - 1, false);
		NymbotVector extremeVector2 = getExtremeVector(nymbots_arr, id, nymbots_data[it->id].nymbot, centerAngle, true);
		// check if the nymbot is partially out of the fov
		if (!(it->vector1.direction <= extremeVector1.direction + EPSILON1 && it->vector1.direction >= extremeVector1.direction - EPSILON1))
		{
			it = nymbots_data[id].robotsDetection.erase(it);
			it--;
			continue;
		}
		if (!(it->vector2.direction <= extremeVector2.direction + EPSILON1 && it->vector2.direction >= extremeVector2.direction - EPSILON1))
		{
			it = nymbots_data[id].robotsDetection.erase(it);
			it--;
			continue;
		}
		// remove if the robot is hidden behind other robots
		int startAngle = floor(it->vector1.direction * ANGLE_ADJUSTMENT), endAngle = floor(it->vector2.direction * ANGLE_ADJUSTMENT);
		int i = startAngle;
		if (endAngle == ANGLES_ARRAY_SIZE - 1)
		{
			endAngle = -1;
		}
		while (i != endAngle + 1)
		{
			if (nymbots_data[id].robotsAngles[i].at(0).id != it->id)
			{
				it = nymbots_data[id].robotsDetection.erase(it);
				it--;
				break;
			}
			i++;
			if (i == ANGLES_ARRAY_SIZE)
			{
				i = 0;
			}
		}
	}
}

// part of a robot = robot (and ignore completely hidden robots)
void CInsectbotCircleFovSense::ignoreCompletelyHiddenModel(nymbot_location_data nymbots_arr[], unsigned int id)
{
	for (auto it = nymbots_data[id].robotsDetection.begin(); it != nymbots_data[id].robotsDetection.end(); it++)
	{
		int startAngle = floor(it->vector1.direction * ANGLE_ADJUSTMENT), endAngle = floor(it->vector2.direction * ANGLE_ADJUSTMENT);
		bool found = false;
		int i = startAngle;
		if (endAngle == ANGLES_ARRAY_SIZE - 1)
		{
			endAngle = -1;
		}
		// remove if the robot is completely hidden behind other robots
		while (i != endAngle + 1)
		{
			if (nymbots_data[id].robotsAngles[i].at(0).id == it->id)
			{
				found = true;
				break;
			}
			i++;
			if (i == ANGLES_ARRAY_SIZE)
			{
				i = 0;
			}
		}
		if (!found)
		{
			it = nymbots_data[id].robotsDetection.erase(it);
			it--;
		}
	}
	xRayModel(nymbots_arr, id);
}

// part of a robot = small robot
void CInsectbotCircleFovSense::partiallyHiddenIsSmallRobotModel(nymbot_location_data nymbots_arr[], unsigned int id)
{
	nymbots_data[id].robotsDetection.clear();
	int startAngle = 0, endAngle = 0, robotId = nymbots_data[id].robotsAngles[0].at(0).id;
	// firstRangeEnd and firstRobot saves data for the robot in angle "0"
	int firstRangeEnd = -1, firstRobot = robotId;
	for (int i = 0; i < ANGLES_ARRAY_SIZE; i++)
	{
		if (nymbots_data[id].robotsAngles[i].at(0).id != robotId)
		{ // if the robot in angle i != the robot in angle i-1
			endAngle = i - 1;
			if (firstRangeEnd == -1)
			{ // end of first range
				firstRangeEnd = i - 1;
			}
			// not first range
			else if (robotId != -1)
			{
				updateRobotsDetection(nymbots_arr, id, robotId, (double)startAngle / ANGLE_ADJUSTMENT, (double)endAngle / ANGLE_ADJUSTMENT);
			}
			startAngle = i;
			robotId = nymbots_data[id].robotsAngles[i].at(0).id;
		}
	}
	// robotId = the robot in the last angle
	if (robotId == firstRobot && robotId != -1)
	{
		updateRobotsDetection(nymbots_arr, id, robotId, (double)startAngle / ANGLE_ADJUSTMENT, (double)firstRangeEnd / ANGLE_ADJUSTMENT);
	}
	if (robotId != firstRobot)
	{
		if (robotId != -1)
		{
			updateRobotsDetection(nymbots_arr, id, robotId, (double)startAngle / ANGLE_ADJUSTMENT, (double)(ANGLES_ARRAY_SIZE - 1) / ANGLE_ADJUSTMENT);
		}
		if (firstRobot != -1)
		{ // add firstRobot to robot "id"s robotsDetection
			updateRobotsDetection(nymbots_arr, id, firstRobot, 0.0, (double)firstRangeEnd / ANGLE_ADJUSTMENT);
		}
	}
}

/********************** occlusions end **********************/

// add new DetectionData element to robot "id"s DetectionData
void CInsectbotCircleFovSense::updateRobotsDetection(nymbot_location_data nymbots_arr[], unsigned int id, unsigned int id2, double startAngle, double endAngle)
{
	DetectionData data;
	data.id = id2;
	// robot "id" sees the robot "id2" from startAngle to endAngle, create vectors from robot "id" to the robot "id2" in these two angles
	data.vector1.direction = startAngle;
	// create a line from robot "id"s location and startAngle
	Line line = createLine(startAngle, nymbots_arr[id].x, nymbots_arr[id].y);
	// find the distance between robot "id" and the intersection point of line and the robot "id2"
	pair<double, pair<double, double>> intersection1 = findClosestIntersectionDistance(nymbots_arr, nymbots_data[id2].nymbot.lines, nymbots_data[id2].nymbot.points, line, id);
	data.vector1.magnitude = intersection1.first;
	data.vector1.point1[0] = nymbots_arr[id].x;
	data.vector1.point1[1] = nymbots_arr[id].y;
	data.vector1.point2[0] = intersection1.second.first;
	data.vector1.point2[1] = intersection1.second.second;
	data.vector2.direction = endAngle;
	line = createLine(endAngle, nymbots_arr[id].x, nymbots_arr[id].y);
	pair<double, pair<double, double>> intersection2 = findClosestIntersectionDistance(nymbots_arr, nymbots_data[id2].nymbot.lines, nymbots_data[id2].nymbot.points, line, id);
	data.vector2.magnitude = intersection2.first;
	data.vector2.point1[0] = nymbots_arr[id].x;
	data.vector2.point1[1] = nymbots_arr[id].y;
	data.vector2.point2[0] = intersection2.second.first;
	data.vector2.point2[1] = intersection2.second.second;
	nymbots_data[id].robotsDetection.push_back(data);
}

void CInsectbotCircleFovSense::findNymbots(nymbot_location_data nymbots_arr[], VISION_MODEL model)
{
	for (unsigned int id = 0; id < robotNumber; id++)
	{
		nymbots_data[id].robotsDetection.clear();
	}
	updateNymbotsData(nymbots_arr);
	for (unsigned int id = 0; id < robotNumber; id++)
	{
		for (int i = 0; i < ANGLES_ARRAY_SIZE; i++)
		{
			AngleDetectionData nymbotAngleData = {-1, -1, {INT_MAX, INT_MAX}};
			nymbots_data[id].robotsAngles[i].clear();
			nymbots_data[id].robotsAngles[i].push_back(nymbotAngleData);
		}

		DetectNymbots(nymbots_arr, id, RADIUS, model);
		// for each angle in robotsAngles, sort the obstacles detected by distance
		for (int i = 0; i < ANGLES_ARRAY_SIZE; i++)
		{
			sort(
				nymbots_data[id].robotsAngles[i].begin(), nymbots_data[id].robotsAngles[i].end(), [](AngleDetectionData &od1, AngleDetectionData &od2) -> bool
				{ return od1.distance < od2.distance; });
		}

		switch (model)
		{
		case X_RAY:
			xRayModel(nymbots_arr, id);
			break;
		case IGNORE_PARTIALLY_HIDDEN:
			ignorePartiallyHiddenModel(nymbots_arr, id);
			break;
		case IGNORE_COMPLETELY_HIDDEN:
			ignoreCompletelyHiddenModel(nymbots_arr, id);
			break;
		case PARTIALLY_HIDDEN_IS_SMALL_ROBOT:
			partiallyHiddenIsSmallRobotModel(nymbots_arr, id);
			break;
		}
	}
	createDrawCircleList(nymbots_arr);
	createDrawRayList();
}

// given an angle, return the nymbots/obstacles detected in this angle from robot "id"
vector<AngleDetectionData> CInsectbotCircleFovSense::getDetectionByAngle(nymbot_location_data nymbots_arr[], unsigned int id, Real angle, vector<AngleDetectionData> anglesArray[ANGLES_ARRAY_SIZE], vector<DetectionData> detectionVector)
{
	// norm robot "id"s orientation
	Real normOri = normOrientation(nymbots_arr[id].orientation);
	angle = digitRound(angle, 2);
	normOri = digitRound(normOri, 2);
	// convert angle from range -180-180 to global 0-360 oriantation
	Real convertedAngle = convertAngleToGlobalAngle(normOri, angle);
	int intAngle = floor(convertedAngle * ANGLE_ADJUSTMENT);
	if (intAngle == ANGLES_ARRAY_SIZE)
	{
		intAngle = 0;
	}
	vector<AngleDetectionData> data = anglesArray[intAngle];
	if (data.empty())
	{
		return data;
	}
	if (data.at(0).distance == -1)
	{
		data.clear();
		return data;
	}
	// delete from data nymbots that don't exist in detectionVector
	for (auto it = data.begin(); it != data.end(); it++)
	{
		bool found = false;
		for (auto it2 = detectionVector.begin(); it2 != detectionVector.end(); it2++)
		{
            // if both the type and the id are equal
			if (it->type == it2->type && it->id == it2->id)
			{
				found = true;
			}
		}
		if (!found)
		{
			data.erase(it);
			it--;
		}
	}
	return data;
}

// given an angle (in range -180-180), return the nymbots detected in this angle from robot "id"
vector<AngleDetectionData> CInsectbotCircleFovSense::getNymbotsDetectionByAngle(nymbot_location_data nymbots_arr[], unsigned int id, Real angle)
{
	return getDetectionByAngle(nymbots_arr, id, angle, nymbots_data[id].robotsAngles, nymbots_data[id].robotsDetection);
}

// given an angle (in range -180-180), return the obstacles detected in this angle from robot "id"
vector<AngleDetectionData> CInsectbotCircleFovSense::getObstaclesDetectionByAngle(nymbot_location_data nymbots_arr[], unsigned int id, Real angle)
{
	vector<AngleDetectionData> detectionData = getDetectionByAngle(nymbots_arr, id, angle, nymbots_data[id].obstaclesAngles, nymbots_data[id].obstaclesDetection);
	for (auto it = detectionData.begin(); it != detectionData.end(); it++)
	{
		if (it->type == WALL)
		{
			detectionData.erase(it);
			it--;
		}
	}
	return detectionData;
}

// given an angle (in range -180-180), return the walls detected in this angle from robot "id"
vector<AngleDetectionData> CInsectbotCircleFovSense::getWallsDetectionByAngle(nymbot_location_data nymbots_arr[], unsigned int id, Real angle)
{
	vector<AngleDetectionData> detectionData = getDetectionByAngle(nymbots_arr, id, angle, nymbots_data[id].obstaclesAngles, nymbots_data[id].obstaclesDetection);
	for (auto it = detectionData.begin(); it != detectionData.end(); it++)
	{
        if (it->type != WALL)
		{
			detectionData.erase(it);
			it--;
		}
	}
	return detectionData;
}

// return nymbots detection data
vector<DetectionData> CInsectbotCircleFovSense::getNymbotsDetectionVector(nymbot_location_data nymbots_arr[], unsigned int id)
{
	vector<DetectionData> data = nymbots_data[id].robotsDetection;
	for (auto it = data.begin(); it != data.end(); it++)
	{
		it->vector1.direction = convertAngleByOrientation(normOrientation(nymbots_arr[id].orientation), it->vector1.direction);
		it->vector2.direction = convertAngleByOrientation(normOrientation(nymbots_arr[id].orientation), it->vector2.direction);
	}
	return data;
}

// return obstacles detection data
vector<DetectionData> CInsectbotCircleFovSense::getObstaclesDetectionVector(nymbot_location_data nymbots_arr[], unsigned int id)
{
	vector<DetectionData> data = nymbots_data[id].obstaclesDetection;
	for (auto it = data.begin(); it != data.end(); it++)
	{
		if (it->type == WALL)
		{
			data.erase(it);
			it--;
		}
	}
	for (auto it = data.begin(); it != data.end(); it++)
	{
		it->vector1.direction = convertAngleByOrientation(normOrientation(nymbots_arr[id].orientation), it->vector1.direction);
		it->vector2.direction = convertAngleByOrientation(normOrientation(nymbots_arr[id].orientation), it->vector2.direction);
	}
	return data;
}

// return walls detection data
vector<DetectionData> CInsectbotCircleFovSense::getWallsDetectionVector(nymbot_location_data nymbots_arr[], unsigned int id)
{
	vector<DetectionData> data = nymbots_data[id].obstaclesDetection;
	for (auto it = data.begin(); it != data.end(); it++)
	{
		if (it->type != WALL)
		{
			data.erase(it);
			it--;
		}
	}
	for (auto it = data.begin(); it != data.end(); it++)
	{
		it->vector1.direction = convertAngleByOrientation(normOrientation(nymbots_arr[id].orientation), it->vector1.direction);
		it->vector2.direction = convertAngleByOrientation(normOrientation(nymbots_arr[id].orientation), it->vector2.direction);
	}
	return data;
}

#ifndef INSECTBOT_CIRCLE_FOV_SENSE_H
#define INSECTBOT_CIRCLE_FOV_SENSE_H

#include <argos3/core/simulator/loop_functions.h>
#include <utils/includes/nymbot_data.h>
#include <includes/nymbot_location_data.h>
#include <utils/includes/obstacles.h>
#include <utils/includes/draw_circle.h>
#include <utils/includes/draw_ray.h>
#include <utils/includes/line.h>

#define ROBOT_MAX_NUMBER 8
#define RADIUS 0.25 // fov radius
// robot's sizes
#define FRONT_DIST 0.02
#define BACK_DIST 0.03
#define SIDE_DIST 0.01
#define EPSILON1 1e-5
#define digitRound(x, d) round(x *pow(10, d)) / pow(10, d)
#define Z_VAL 0.001
#define VECTOR_COLOR CColor::ORANGE
#define OBSTACLE_VECTOR_COLOR CColor::PURPLE

enum BORDERS
{
	FRONT,
	BACK,
	RIGHT,
	LEFT
};
enum POINT_INDICATION
{
	X,
	Y,
    Z
};
enum RANGE
{
	_MIN,
	_MAX
};
enum OBSTACLES_ID
{
	BOX,
	CYLINDER,
	CIRCLE,
	WALL,
    RECTANGLE,
	WALL_NORTH,
	WALL_SOUTH,
	WALL_EAST,
	WALL_WEST
};

enum VISION_MODEL
{
	X_RAY,
	IGNORE_PARTIALLY_HIDDEN,
	IGNORE_COMPLETELY_HIDDEN,
	PARTIALLY_HIDDEN_IS_SMALL_ROBOT
};

using namespace std;
using namespace argos;

Real normOrientation(CDegrees orientation);
Real convertAngleByOrientation(Real orientation, Real angle);
Real convertAngleToGlobalAngle(Real orientation, Real angle);

class CInsectbotCircleFovSense
{

public:
	vector<DrawRay> *drawRayList;
	vector<DrawCircle> *drawCircleList;

	CInsectbotCircleFovSense();
	CInsectbotCircleFovSense(unsigned int robotMaxNumber);
	virtual ~CInsectbotCircleFovSense() {}
	void findObstacles(nymbot_location_data nymbots_arr[], Obstacles obstacles);
	void findNymbots(nymbot_location_data nymbots_arr[], VISION_MODEL model);
	vector<AngleDetectionData> getNymbotsDetectionByAngle(nymbot_location_data nymbots_arr[], unsigned int id, Real angle);
	vector<AngleDetectionData> getObstaclesDetectionByAngle(nymbot_location_data nymbots_arr[], unsigned int id, Real angle);
	vector<AngleDetectionData> getWallsDetectionByAngle(nymbot_location_data nymbots_arr[], unsigned int id, Real angle);
	vector<DetectionData> getNymbotsDetectionVector(nymbot_location_data nymbots_arr[], unsigned int id);
	vector<DetectionData> getObstaclesDetectionVector(nymbot_location_data nymbots_arr[], unsigned int id);
	vector<DetectionData> getWallsDetectionVector(nymbot_location_data nymbots_arr[], unsigned int id);
    static void calcLines(Real rightFront[2], Real leftFront[2], Real rightBack[2], Real leftBack[2], Line lines[4]);
	unsigned int robotNumber;

private:
	nymbot_data *nymbots_data;

	static Real calcM(Real point1[2], Real point2[2]);
	static Real calcB(Real point[2], Real m);
	void updateNymbot(Real x, Real y, CDegrees orientation, unsigned int id);
	void updateNymbotsData(nymbot_location_data nymbots_arr[]);
	void createDrawRayList();
	void createDrawCircleList(nymbot_location_data nymbots_arr[]);
	void printRobotsDetection();
	double calcDistance(double x1, double y1, double x2, double y2);
	Line createLine(double angle, float x, float y);
	pair<double, double> findIntersectionPoint(Line line1, Line line2);
	pair<double, pair<double, double>> findClosestIntersectionDistance(nymbot_location_data nymbots_arr[], Line lines[4], Real points[4][2], Line line, unsigned int id);
	bool isInsideRectangle(nymbot_location_data nymbots_arr[], Real points[4][2], double x, double y);

    AngleDetectionData createRectangleDetectionData(nymbot_location_data nymbots_arr[], unsigned int id, Line lines[4], Real points[4][2], Line line);
	AngleDetectionData createCylinderObstacleData(nymbot_location_data nymbots_arr[], unsigned int id, double position[2], double radius, Line line);
	pair<AngleDetectionData, AngleDetectionData> createCircleObstacleData(nymbot_location_data nymbots_arr[], unsigned int id, double position[2], double radius, Line line);
	pair<NymbotVector, NymbotVector> updateAnglesArray(nymbot_location_data nymbots_arr[], unsigned int id, vector<AngleDetectionData> anglesArray[], Rectangle rectangle, Cylinder cylinder, Circle circle, double maxDist, int angle, bool add, bool isRectangle, bool isCylinder, bool isCircle, unsigned int id2, int type2);
	pair<NymbotVector, NymbotVector> updateAnglesArray(nymbot_location_data nymbots_arr[], unsigned int id, vector<AngleDetectionData> anglesArray[], Rectangle rectangle, Cylinder cylinder, Circle circle, double maxDist, int angle, bool add, bool isRectangle, bool isCylinder, bool isCircle, unsigned int id2, int type2, CDegrees orientation);
	NymbotVector updateCircleAngles(nymbot_location_data nymbots_arr[], unsigned int id, Circle circle, double maxDist);
	int getCenterAngle(double xRobot, double yRobot, double xObstacle, double yObstacle);
	void findBoxes(nymbot_location_data nymbots_arr[], unsigned int id, Obstacles obstacles, double maxDist);
	void findWalls(nymbot_location_data nymbots_arr[], unsigned int id, Obstacles obstacles, double maxDist);
	vector<pair<double, double>> findIntersectionBetweenLineAndCircle(double cx, double cy, double circle_radius, double p1x, double p1y, double p2x, double p2y);
	void findCylinders(nymbot_location_data nymbots_arr[], unsigned int id, Obstacles obstacles, double maxDist);
	void findCircles(nymbot_location_data nymbots_arr[], unsigned int id, Obstacles obstacles, double maxDist);
	NymbotVector getExtremeVector(nymbot_location_data nymbots_arr[], unsigned int id, Rectangle rectangle, int angle, bool add);
	void DetectNymbots(nymbot_location_data nymbots_arr[], unsigned int id, double maxDist, int model);
	void xRayModel(nymbot_location_data nymbots_arr[], unsigned int id);
	void ignorePartiallyHiddenModel(nymbot_location_data nymbots_arr[], unsigned int id);
	void ignoreCompletelyHiddenModel(nymbot_location_data nymbots_arr[], unsigned int id);
	void updateRobotsDetection(nymbot_location_data nymbots_arr[], unsigned int id, unsigned int id2, double startAngle, double endAngle);
	void partiallyHiddenIsSmallRobotModel(nymbot_location_data nymbots_arr[], unsigned int id);
	vector<AngleDetectionData> getDetectionByAngle(nymbot_location_data nymbots_arr[], unsigned int id, Real angle, vector<AngleDetectionData> anglesArray[ANGLES_ARRAY_SIZE], vector<DetectionData> detectionVector);
};

#endif

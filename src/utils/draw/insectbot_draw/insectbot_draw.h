#ifndef INSECTBOT_DRAW_H
#define INSECTBOT_DRAW_H

#include <argos3/plugins/simulator/visualizations/qt-opengl/qtopengl_user_functions.h>
#include <loop_functions/api_loop_function/api_loop_function.h>
#include <utils/includes/draw_ray.h>
#include <utils/includes/draw_circle.h>

#define ROBOT_ID 0
#define RAY_SIZE (2.0)

class CInsectbotDraw : public CQTOpenGLUserFunctions
{

public:
    CInsectbotDraw();
    virtual ~CInsectbotDraw() {}
    virtual void DrawInWorld();

private:
    CApi_loop_function &loopFunction;
};

#endif

#include "insectbot_draw.h"


CInsectbotDraw::CInsectbotDraw() : loopFunction(dynamic_cast<CApi_loop_function &>(CSimulator::GetInstance().GetLoopFunctions())) {}


void CInsectbotDraw::DrawInWorld(){
	// loopFunction.LastSeenPosition(); // Needed only for the first time, seems not relevant.
	for (auto it = loopFunction.sensor.drawRayList[ROBOT_ID].begin(); it != loopFunction.sensor.drawRayList[ROBOT_ID].end(); it++){
		DrawRay(it->ray, it->color, RAY_SIZE);
	}
	for (auto it = loopFunction.sensor.drawCircleList[ROBOT_ID].begin(); it != loopFunction.sensor.drawCircleList[ROBOT_ID].end(); it++){
		DrawCircle(it->center, CQuaternion(0.0, 0.0, 0.0, 0.0), it->radius, CColor::RED, false, 100U);
	}
}


REGISTER_QTOPENGL_USER_FUNCTIONS(CInsectbotDraw, "insectbot_draw")
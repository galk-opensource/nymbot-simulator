#ifndef API_LOOP_FUNCTION_H
#define API_LOOP_FUNCTION_H

#include <argos3/core/simulator/loop_functions.h>
#include <argos3/core/simulator/entity/floor_entity.h>
#include <argos3/core/utility/math/range.h>
#include <argos3/core/utility/math/rng.h>
#include <utils/utils_header.h>
#include <controllers/diff_nymbot/diff_nymbot.h>
// #include <controllers/dummy_nymbot/dummy_nymbot.h>
#include <includes/nymbot_location_data.h>
#include <utils/includes/obstacles.h>
#include <time.h>
#include <argos3/core/simulator/simulator.h>
#include <argos3/core/utility/configuration/argos_configuration.h>
#include <plugins/robots/insectbot/simulator/insectbot_entity.h>
#include <argos3/plugins/simulator/entities/box_entity.h>
#include <argos3/plugins/simulator/entities/cylinder_entity.h>
#include <utils/insectbot_circle_fov_sense/insectbot_circle_fov_sense.h>


class CApi_loop_function : public CLoopFunctions
{

public:
   CApi_loop_function();
   virtual ~CApi_loop_function() {}
   // Functions from CLoopFunctions of ARGoS.
   virtual void Init(TConfigurationNode &t_tree);
   virtual void PreStep();
   virtual void PostStep();
   virtual bool IsExperimentFinished();
   virtual void PostExperiment();
   virtual void Destroy();

   virtual void nymbot_update_velocity(int id, float speed, float angularVel);
   virtual void LastSeenPosition();

   CInsectbotCircleFovSense sensor;

protected:
   unsigned int robotNumber;

   nymbot_location_data *nymbots_arr;
   Obstacles obstacles;

   unsigned int frame;

    void updateBox(Box *box, Real x, Real y, CDegrees angle);
   void initBoxesMap();
   void initCylindersMap();
   void getRobotsCount(string type);
   //TODO: think to delete this function.
   double getOrientationsAverage(nymbot_location_data nymbots_arr[], int id);
};

#endif

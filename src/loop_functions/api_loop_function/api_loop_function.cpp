#include "api_loop_function.h"

#define ID 1
#define FIRST 0

CApi_loop_function::CApi_loop_function() {}

CDegrees getOrientation(CQuaternion cQuat)
{
    vector<Real> orient;
    CRadians cZAngle, cYAngle, cXAngle;
    CDegrees xDegree;
    cQuat.ToEulerAngles(cZAngle, cYAngle, cXAngle);
    xDegree = ToDegrees(cZAngle);
    return xDegree;
}

// fill the obstacles structure with the information recieved from argos about the walls and the boxes in the arena
void CApi_loop_function::initBoxesMap()
{
    CSpace::TMapPerType &boxesMap = GetSpace().GetEntitiesByType("box");
    if (!boxesMap.empty())
    {

        for (auto it = boxesMap.begin(); it != boxesMap.end(); ++it)
        {
            CBoxEntity &box = *any_cast<CBoxEntity *>(it->second);
            Box b;
            string id = box.GetId();
            bool isWall = false;
            // checking if id starts with 'w' that signals it's a wall
            if (id[FIRST] == 'w') {
                isWall = true;
            }
            CVector3 position = box.GetEmbodiedEntity().GetOriginAnchor().Position;
            b.position[X] = position.GetX();
            b.position[Y] = position.GetY();
            CQuaternion orientation = box.GetEmbodiedEntity().GetOriginAnchor().Orientation;
            b.orientation = getOrientation(orientation);
            CVector3 size = box.GetSize();
            b.size[X] = size.GetX();
            b.size[Y] = size.GetY();
            b.size[Z] = size.GetZ();
            updateBox(&b, b.position[X], b.position[Y], b.orientation);
            // making sure the rest of the id is a valid number
            if (!all_of(id.begin() + ID, id.end(), ::isdigit)) {
                cerr << "Error: wall id is not a number\n";
            } else
                b.id = atoi(id.substr(ID).c_str());
            if (isWall)
            {
                obstacles.walls.push_back(b);
            }
            else
            {
                obstacles.boxes.push_back(b);
            }
        }
    }
}

/** start of section **/

void CApi_loop_function::updateBox(Box *box, Real x, Real y, CDegrees angle)
{
    CRadians radAngle = ToRadians(angle);
    Real sin = argos::Sin(radAngle);
    Real cos = argos::Cos(radAngle);
    double xDist = box->size[Y] / 2, yDist = box->size[X] / 2;
    box->box.points[0][X] = x + cos * (yDist) + sin * (xDist);
    box->box.points[0][Y] = y + sin * (yDist)-cos * (xDist);

    box->box.points[1][X] = x + cos * (yDist)-sin * (xDist);
    box->box.points[1][Y] = y + sin * (yDist) + cos * (xDist);

    box->box.points[2][X] = x - cos * (yDist) + sin * (xDist);
    box->box.points[2][Y] = y - sin * (yDist)-cos * (xDist);

    box->box.points[3][X] = x - cos * (yDist)-sin * (xDist);
    box->box.points[3][Y] = y - sin * (yDist) + cos * (xDist);
    CInsectbotCircleFovSense::calcLines(box->box.points[0], box->box.points[1], box->box.points[2], box->box.points[3], box->box.lines);
    box->orientation = angle;
}


/** end of section **/


// fill the obstacles structure with the information recieved from argos about the cylinders in the arena
void CApi_loop_function::initCylindersMap()
{
    try
    {
        // Get the number of entities of a specific type (e.g., robots)
        CSpace::TMapPerType &cylindersMap = GetSpace().GetEntitiesByType("cylinder");
        if (!cylindersMap.empty())
        {
            for (auto it = cylindersMap.begin(); it != cylindersMap.end(); ++it)
            {
                CCylinderEntity &cylinder = *any_cast<CCylinderEntity *>(it->second);
                Cylinder c;
                CVector3 position = cylinder.GetEmbodiedEntity().GetOriginAnchor().Position;
                c.position[X] = position.GetX();
                c.position[Y] = position.GetY();
                c.radius = double(cylinder.GetRadius());
                string id = cylinder.GetId();
                if (!all_of(id.begin() + ID, id.end(), ::isdigit)) {
                    cerr << "Error: cylinder id is not a number\n";
                } else
                    c.id = atoi(id.substr(ID).c_str());
                obstacles.cylinders.push_back(c);
            }
        }
    }
    catch (const std::exception &ex)
    {
        // Handle the exception (e.g., print an error message)
        std::cerr << "Exception: " << ex.what() << std::endl;
    }
    catch (...)
    {
        // Handle other exceptions
        std::cerr << "An unexpected exception occurred." << std::endl;
    }
}

void CApi_loop_function::Init(TConfigurationNode &t_node)
{
    getRobotsCount("insectbot"); // TODO: get the id from the argos file.
    nymbots_arr = new nymbot_location_data[robotNumber];
    sensor = CInsectbotCircleFovSense(robotNumber);

    for (unsigned int i = 0; i < robotNumber; i++)
    {
        nymbots_arr[i].state = INSECTBOT_STATE_STOP;
    }
    initBoxesMap();
    initCylindersMap();

    frame = 0;
}

void CApi_loop_function::PostStep() {}
bool CApi_loop_function::IsExperimentFinished() { return false; }
void CApi_loop_function::PostExperiment() {}
void CApi_loop_function::Destroy() { exit(EXIT_SUCCESS); }

void CApi_loop_function::PreStep()
{
    LastSeenPosition();

    // frame++;
}

void CApi_loop_function::nymbot_update_velocity(int id, float speed, float angularVel)
{
    int i = 0;
    CSpace::TMapPerType &m_cInsectbots = GetSpace().GetEntitiesByType("insectbot");
    for (auto it = m_cInsectbots.begin();
         it != m_cInsectbots.end();
         ++it)
    {
        if (i == id)
        {
            CInsectbotEntity &cInsectbots = *any_cast<CInsectbotEntity *>(it->second);
            auto &IController = dynamic_cast<CDiffNymbot &>(cInsectbots.GetControllableEntity().GetController());
            IController.setVelocity(speed, angularVel);
            return;
        }
        i++;
    }
}

void CApi_loop_function::LastSeenPosition()
{
    int nymbotID = 0;
    CSpace::TMapPerType &m_cInsectbots = GetSpace().GetEntitiesByType("insectbot");
    for (auto it = m_cInsectbots.begin(); it != m_cInsectbots.end(); ++it)
    {
        CInsectbotEntity &cInsectbots = *any_cast<CInsectbotEntity *>(it->second);
        auto &IController = dynamic_cast<CDiffNymbot &>(cInsectbots.GetControllableEntity().GetController());
        CDegrees orientation = getOrientation(cInsectbots.GetEmbodiedEntity().GetOriginAnchor().Orientation);
        vector<double> l = IController.getLocation();
        nymbots_arr[nymbotID].x = l.at(0);
        nymbots_arr[nymbotID].y = l.at(1);
        nymbots_arr[nymbotID].orientation = orientation;
        nymbotID++;
    }
    // detect obstacles every 5 frames
    if (frame % 5 == 0)
    {
        sensor.findObstacles(nymbots_arr, obstacles);
    }
    sensor.findNymbots(nymbots_arr, X_RAY);
}

// get the orientations average of all nymbots inside nymbot "id"s fov
double CApi_loop_function::getOrientationsAverage(nymbot_location_data nymbots_arr[], int id)
{ // working well
    double sum = 0;
    vector<DetectionData> detectionData = sensor.getNymbotsDetectionVector(nymbots_arr, id);
    for (auto it = detectionData.begin(); it != detectionData.end(); it++)
    {
        sum += convertAngleByOrientation(normOrientation(nymbots_arr[id].orientation), normOrientation(nymbots_arr[it->id].orientation));
    }
    if (sum == 0)
    {
        return 0.0;
    }
    return sum / detectionData.size();
}

/**
 * @brief Gets all the entities with the given type name and counts them into robotNumber.
 *
 * @param type Name given in the ARGoS experiment file under arena.
 * @return Distance to the edge of the rectangle at the specified angle (in meters).
 */
void CApi_loop_function::getRobotsCount(string type)
{
    // Access the space
    argos::CSimulator &cSimulator = argos::CSimulator::GetInstance();
    argos::CSpace &cSpace = cSimulator.GetSpace();
    // Get the number of entities of a specific type (e.g., robots)
    robotNumber = cSpace.GetEntitiesByType(type).size();
}
REGISTER_LOOP_FUNCTIONS(CApi_loop_function, "api_loop_function")

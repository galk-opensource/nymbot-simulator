#include "insectbot_motion.h"

CInsectbotMotion::CInsectbotMotion() {}


void CInsectbotMotion::Init(TConfigurationNode &t_node)
{
    CApi_loop_function::Init(t_node);

    stayCounter = new int[robotNumber];
    hitTurn = new int[robotNumber];
    passedWall = new int[robotNumber];
    averageO = new double[robotNumber];

    for (unsigned int i = 0; i < robotNumber; i++)
    {
        nymbots_arr[i].state = INSECTBOT_STATE_STOP;
        stayCounter[i] = 0;
        hitTurn[i] = 0;
        passedWall[i] = 0;
        averageO[i] = 0;
    }
}

void CInsectbotMotion::turnHandler(int id, int wallId, double heading)
{
    switch (wallId)
    {
    case WALL_NORTH:
        if (heading > 90)
        {
            hitTurn[id] = TURN_SHARP_LEFT;
        }
        else
        {
            hitTurn[id] = TURN_SHARP_RIGHT;
        }
        break;
    case WALL_WEST:
        if (heading > 180)
        {
            hitTurn[id] = TURN_SHARP_LEFT;
        }
        else
        {
            hitTurn[id] = TURN_SHARP_RIGHT;
        }
        break;
    case WALL_SOUTH:
        if (heading > 270)
        {
            hitTurn[id] = TURN_SHARP_LEFT;
        }
        else
        {
            hitTurn[id] = TURN_SHARP_RIGHT;
        }
        break;
    case WALL_EAST:
        if (heading > 0 && heading < 180)
        {
            hitTurn[id] = TURN_SHARP_LEFT;
        }
        else
        {
            hitTurn[id] = TURN_SHARP_RIGHT;
        }
        break;
    }
}

void CInsectbotMotion::passedWallHandler(int id, int wallId, double heading)
{
    switch (wallId)
    {
    case WALL_NORTH:
        if (hitTurn[id] == TURN_SHARP_LEFT)
        {
            if (heading > 180 + PASSED_WALL_EPSILON && heading < 270)
            {
                passedWall[id] = 1;
            }
            else
            {
                passedWall[id] = 0;
            }
        }
        else
        {
            if (heading > 270 && heading < 360 - PASSED_WALL_EPSILON)
            {
                passedWall[id] = 1;
            }
            else
            {
                passedWall[id] = 0;
            }
        }
        break;
    case WALL_WEST:
        if (hitTurn[id] == TURN_SHARP_LEFT)
        {
            if (heading > 270 + PASSED_WALL_EPSILON && heading < 360)
            {
                passedWall[id] = 1;
            }
            else
            {
                passedWall[id] = 0;
            }
        }
        else
        {
            if (heading > 0 && heading < 90 - PASSED_WALL_EPSILON)
            {
                passedWall[id] = 1;
            }
            else
            {
                passedWall[id] = 0;
            }
        }
        break;
    case WALL_SOUTH:
        if (hitTurn[id] == TURN_SHARP_LEFT)
        {
            if (heading > 0 + PASSED_WALL_EPSILON && heading < 90)
            {
                passedWall[id] = 1;
            }
            else
            {
                passedWall[id] = 0;
            }
        }
        else
        {
            if (heading > 90 && heading < 180 - PASSED_WALL_EPSILON)
            {
                passedWall[id] = 1;
            }
            else
            {
                passedWall[id] = 0;
            }
        }
        break;
    case WALL_EAST:
        if (hitTurn[id] == TURN_SHARP_LEFT)
        {
            if (heading > 90 + PASSED_WALL_EPSILON && heading < 180)
            {
                passedWall[id] = 1;
            }
            else
            {
                passedWall[id] = 0;
            }
        }
        else
        {
            if (heading > 180 && heading < 270 - PASSED_WALL_EPSILON)
            {
                passedWall[id] = 1;
            }
            else
            {
                passedWall[id] = 0;
            }
        }
        break;
    }
}

// check if nymbot "id" hits a wall. return TURN if it does, NO_HIT otherwise
double CInsectbotMotion::hitWall(nymbot_location_data nymbots_arr[], int id)
{
    vector<AngleDetectionData> detectionData;
    for (int i = 0; i <= 130; i++)
    {
        detectionData = sensor.getWallsDetectionByAngle(nymbots_arr, id, i);
        if (!(detectionData.empty()) && detectionData.at(0).distance <= ROBOT_MAX_DIAGONAL + WALL_EPSILON)
        {
            if (hitTurn[id] == 0)
            {
                turnHandler(id, detectionData.at(0).id, normOrientation(nymbots_arr[id].orientation));
            }
            passedWallHandler(id, detectionData.at(0).id, normOrientation(nymbots_arr[id].orientation));
            if (abs(averageO[id]) < 0.7393)
            {
                if (i > 100)
                {
                    return 0;
                }
            }
            return hitTurn[id];
        }
        detectionData = sensor.getWallsDetectionByAngle(nymbots_arr, id, -1 * i);
        if (!(detectionData.empty()) && detectionData.at(0).distance <= ROBOT_MAX_DIAGONAL + WALL_EPSILON)
        {
            if (hitTurn[id] == 0)
            {
                turnHandler(id, detectionData.at(0).id, normOrientation(nymbots_arr[id].orientation));
            }
            passedWallHandler(id, detectionData.at(0).id, normOrientation(nymbots_arr[id].orientation));
            if (abs(averageO[id]) < 0.7393)
            {
                if (i > 100)
                {
                    return 0;
                }
            }
            return hitTurn[id];
        }
    }
    hitTurn[id] = 0;
    return NO_HIT;
}

// check if nymbot "id" hits another nymbot
int CInsectbotMotion::hitNymbot(nymbot_location_data nymbots_arr[], int id)
{
    stayCounter[id]++;
    // check for hits in the nymbots front
    for (int i = 0; i <= ROBOT_VIEW_ANGLES; i++)
    {
        vector<AngleDetectionData> data = sensor.getNymbotsDetectionByAngle(nymbots_arr, id, i);
        if (!data.empty() && data.at(0).distance <= ROBOT_MAX_DIAGONAL + NYMBOT_EPSILON)
        {
            return STAY;
        }
        data = sensor.getNymbotsDetectionByAngle(nymbots_arr, id, -1 * i);
        if (!data.empty() && data.at(0).distance <= ROBOT_MAX_DIAGONAL + NYMBOT_EPSILON)
        {
            return STAY;
        }
    }
    stayCounter[id] = 0;
    // check for hits in the front sides
    for (int i = ROBOT_VIEW_ANGLES + 1; i <= 60; i++)
    {
        vector<AngleDetectionData> data = sensor.getNymbotsDetectionByAngle(nymbots_arr, id, i);
        // if the nymbot hits another nymbot in its right front side
        if (!data.empty() && data.at(0).distance <= ROBOT_MAX_DIAGONAL + NYMBOT_EPSILON)
        {
            return GO_LEFT;
        }
        data = sensor.getNymbotsDetectionByAngle(nymbots_arr, id, -1 * i);
        // if the nymbot hits another nymbot in its left front side
        if (!data.empty() && data.at(0).distance <= ROBOT_MAX_DIAGONAL + NYMBOT_EPSILON)
        {
            return GO_RIGHT;
        }
    }
    return NO_HIT;
}

void CInsectbotMotion::hitWallHandler(int id, double angularVel)
{
    if (passedWall[id] == 0)
    {
        nymbot_update_velocity(id, BASIC_SPEED, angularVel);
    }
    else
    {
        if (averageO[id] == 0)
        {
            nymbot_update_velocity(id, BASIC_SPEED, 0);
        }
        else if (abs(averageO[id]) > 2.2179)
        {
            averageO[id] > 0 ? nymbot_update_velocity(id, 190, -15) : nymbot_update_velocity(id, 190, 15);
        }
        else if (abs(averageO[id]) > 1.4786)
        {
            averageO[id] > 0 ? nymbot_update_velocity(id, 190, -10) : nymbot_update_velocity(id, 190, 10);
        }
        else if (abs(averageO[id]) > 0.7393)
        {
            averageO[id] > 0 ? nymbot_update_velocity(id, 190, -5) : nymbot_update_velocity(id, 190, 5);
        }
        else
        {
            nymbot_update_velocity(id, 190, 0);
        }
    }
}

void CInsectbotMotion::hitNymbotHandler(int id, int angularVel)
{
    if (angularVel != STAY)
    {
        nymbot_update_velocity(id, BASIC_SPEED, angularVel);
    }
}

/**
 * in the simulator in speed 190:
 * angular velocity 1 - 5 (or -1 - -5) : one wheel 9, the second 8
 * angular velocity 6 - 10 (or -6 - -10) : one wheel 9, the second 7
 * angular velocity 11 - 15 (or -11 - -15) : one wheel 9, the second 6
 *
 * find change in degrees for each of this ranges
 * the code:
 * * printf("heading: %f\n", normOrientation(nymbots_arr[id].orientation));
 * * nymbot_update_velocity(id, 190, 1); // 1/6/11
 * find the differnece between two consecutive headings
 */
void CInsectbotMotion::PreStep()
{
    CApi_loop_function::PreStep();
    for (unsigned int id = 0; id < robotNumber; id++)
    {
        double averageOrientation = getOrientationsAverage(nymbots_arr, id);
        averageO[id] = averageOrientation;
        int hitN = hitNymbot(nymbots_arr, id);
        double hitW = hitWall(nymbots_arr, id);
        if (hitW != NO_HIT)
        {
            hitWallHandler(id, hitW);
        }
        if (hitN != NO_HIT)
        {
            // if the nymbot is hitting another nymbot and not moving for 10 frames or above
            if (stayCounter[id] >= 10)
            {
                nymbot_update_velocity(id, 80, 0);
            }
            else
            {
                if (hitW == NO_HIT || (hitW != NO_HIT && hitN * hitW > 0))
                {
                    hitNymbotHandler(id, hitN);
                }
                else
                {
                    hitNymbotHandler(id, 0);
                }
            }
        }
        if (hitW == NO_HIT && hitN == NO_HIT)
        {
            if (abs(averageOrientation) > 2.2179)
            {
                averageOrientation > 0 ? nymbot_update_velocity(id, 190, -15) : nymbot_update_velocity(id, 190, 15);
            }
            else if (abs(averageOrientation) > 1.4786)
            {
                averageOrientation > 0 ? nymbot_update_velocity(id, 190, -10) : nymbot_update_velocity(id, 190, 10);
            }
            else if (abs(averageOrientation) > 0.7393)
            {
                averageOrientation > 0 ? nymbot_update_velocity(id, 190, -5) : nymbot_update_velocity(id, 190, 5);
            }
            else
            {
                nymbot_update_velocity(id, 190, 0);
            }
        }
    }
    frame++;
}

REGISTER_LOOP_FUNCTIONS(CInsectbotMotion, "insectbot_motion")

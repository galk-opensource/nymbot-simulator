#ifndef INSECTBOT_MOTION_H
#define INSECTBOT_MOTION_H

#include <argos3/core/simulator/loop_functions.h>
#include <argos3/core/simulator/entity/floor_entity.h>
#include <argos3/core/utility/math/range.h>
#include <argos3/core/utility/math/rng.h>
#include <utils/utils_header.h>
#include <loop_functions/api_loop_function/api_loop_function.h>
#include <includes/nymbot_location_data.h>
#include <includes/obstacles.h>
#include <time.h>
#include <argos3/core/simulator/simulator.h>
#include <argos3/core/utility/configuration/argos_configuration.h>
#include <plugins/robots/insectbot/simulator/insectbot_entity.h>
#include <argos3/plugins/simulator/entities/box_entity.h>
#include <argos3/plugins/simulator/entities/cylinder_entity.h>



#define ROBOT_MAX_DIAGONAL sqrt(pow(0.03,2)+pow(0.01,2))
#define ROBOT_VIEW_ANGLES atan2(0.01,0.02)
#define NYMBOT_EPSILON 0.05
#define WALL_EPSILON 0.07
#define PASSED_WALL_EPSILON 10
#define NO_HIT 20 //?
#define STAY 0
#define BASIC_SPEED 190
#define TURN_SHARP_LEFT 6
#define TURN_SHARP_RIGHT -6
#define GO_LEFT 1
#define GO_RIGHT -1


using namespace argos;
using namespace std;


class CInsectbotMotion : public CApi_loop_function {

public:

   CInsectbotMotion();
   virtual ~CInsectbotMotion() {}
   virtual void Init(TConfigurationNode& t_tree);
   virtual void PreStep();

private:

   int *stayCounter;
   int *hitTurn;
   int *passedWall;
   double *averageO;


   void turnHandler(int id, int wallId, double heading);
   void passedWallHandler(int id, int wallId, double heading);
   double hitWall(nymbot_location_data nymbots_arr[], int id);
   int hitNymbot(nymbot_location_data nymbots_arr[], int id);
   void hitWallHandler(int id, double angularVel);
   void hitNymbotHandler(int id, int angularVel);
};

#endif

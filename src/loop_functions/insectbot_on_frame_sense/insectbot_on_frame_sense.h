#ifndef INSECTBOT_ON_FRAME_SENSE_H
#define INSECTBOT_ON_FRAME_SENSE_H

#include <loop_functions/api_loop_function/api_loop_function.h>
#include <argos3/core/simulator/entity/floor_entity.h>
#include <argos3/core/utility/math/range.h>
#include <argos3/core/utility/math/rng.h>
#include <utils/utils_header.h>
#include <argos3/core/simulator/simulator.h>
#include <argos3/core/utility/configuration/argos_configuration.h>
#include <plugins/robots/insectbot/simulator/insectbot_entity.h>
#include <argos3/plugins/simulator/entities/box_entity.h>
#include <argos3/plugins/simulator/entities/cylinder_entity.h>
#include <includes/nymbot_location_data.h>
#include <includes/obstacles.h>
#include <time.h>

#define DEFAULT_ANGULAR_VELOCITY 5
#define DEFAULT_SPEED 148

#define MIN_SPEED 0
#define MAX_SPEED 200 
#define MAX_SPEED_ENV 8
#define MIN_SPEED_ENV -8

#define MAX_ANG_VEL 15
#define MIN_ANG_VEL -15
#define MAX_ANG_VEL_ENV 3
#define MIN_ANG_VEL_ENV -3
#define BORDER 0.35

#define TURN_SPEED 150
#define TURN_ANG_VEL 10
#define NO_TURN 0

using namespace argos;
using namespace std;


class CInsectbotOnFrameSense : public CApi_loop_function {

public:

   CInsectbotOnFrameSense();
   virtual ~CInsectbotOnFrameSense() {}

   virtual void Init(TConfigurationNode& t_tree);
   virtual void PreStep();

   //follow wall
   void goStraight(unsigned int index);
   void turn(unsigned int index);
   bool turnRightAngle(Real currOrientation, unsigned int index);
   void updateWantedOrientation(unsigned int index, Real orientVal);

private:
};

#endif

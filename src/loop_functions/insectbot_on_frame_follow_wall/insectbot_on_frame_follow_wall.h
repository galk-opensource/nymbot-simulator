#ifndef INSECTBOT_ONFRAME_FOLLOW_WALL_H
#define INSECTBOT_ONFRAME_FOLLOW_WALL_H

#include <argos3/core/simulator/entity/floor_entity.h>
#include <argos3/core/utility/math/range.h>
#include <argos3/core/utility/math/rng.h>
#include <utils/utils_header.h>
#include <loop_functions/api_loop_function/api_loop_function.h>

#include <includes/nymbot_location_data.h>
#include <time.h>

using namespace argos;
using namespace std;

#define DEFAULT_ANGULAR_VELOCITY 5
#define DEFAULT_SPEED 148

class CInsectbotOnFrameFollowWall : public CApi_loop_function
{

public:
   CInsectbotOnFrameFollowWall();
   virtual ~CInsectbotOnFrameFollowWall() {}

   virtual void Init(TConfigurationNode &t_tree);

   virtual void PreStep();
   void goStraight(unsigned int index);
   void turn(unsigned int index);


private:
   void getRobotsCount(string type);
   bool turnRightAngle(Real currOrientation, unsigned int index);
   void updateWantedOrientation(unsigned int index, Real orientVal);

};

#endif

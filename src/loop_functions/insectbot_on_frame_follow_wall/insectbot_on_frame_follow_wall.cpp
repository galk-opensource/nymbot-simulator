#include "insectbot_on_frame_follow_wall.h"
#include <argos3/core/simulator/simulator.h>
#include <argos3/core/utility/configuration/argos_configuration.h>
#include <plugins/robots/insectbot/simulator/insectbot_entity.h>
#include <controllers/messy_nymbot/messy_nymbot.h>

using namespace std;

#define MIN_SPEED 0
#define MAX_SPEED 200
#define MAX_SPEED_ENV 8
#define MIN_SPEED_ENV -8

#define MAX_ANG_VEL 15
#define MIN_ANG_VEL -15
#define MAX_ANG_VEL_ENV 3
#define MIN_ANG_VEL_ENV -3
#define BORDER 0.35

#define TURN_SPEED 150
#define TURN_ANG_VEL 10
#define NO_TURN 0

/****************************************/
/****************************************/

CInsectbotOnFrameFollowWall::CInsectbotOnFrameFollowWall() {}

/****************************************/
/****************************************/

bool equal(double d1, double d2)
{
    if (d1 < 0)
    {
        d1 = d1 * -1;
    }
    bool res = fabs(d1 - d2) < 1;
    return res;
}

bool equalDegrees(Real d1, Real d2)
{
    float diff = abs(d1 - d2);
    bool res = (diff < 1) || (diff > 359);

    return res;
}

void CInsectbotOnFrameFollowWall::Init(TConfigurationNode &t_node)
{
    CApi_loop_function::Init(t_node);
}

Real closestOrientation(Real orientation)
{
    if (315 < orientation || orientation <= 45)
    {
        return 0;
    }
    if (45 < orientation && orientation <= 135)
    {
        return 90;
    }
    if (135 < orientation && orientation <= 225)
    {
        return 180;
    }
    if (225 < orientation && orientation <= 315)
    {
        return 270;
    }
    return -1;
}

bool hitBorders(vector<double> location, CDegrees orientation)
{

    Real orientVal = normOrientation(orientation);
    int angleVal = int(closestOrientation(orientVal));
    bool res;

    switch (angleVal)
    {
    case (0):
        res = location[0] > (BORDER);
        break;
    case (90):
        res = location[1] > (BORDER);
        break;
    case (180):
        res = location[0] < (-1 * BORDER);
        break;
    case (270):
        res = location[1] < (-1 * BORDER);
        break;
    default:
        res = false;
    }
    return res;
}

bool CInsectbotOnFrameFollowWall::turnRightAngle(Real currOrientation, unsigned int index)
{
    Real wantedOrientation = nymbots_arr[index].wantedOrientation;
    bool res = (equalDegrees(currOrientation, wantedOrientation));
    return res;
}

double minMaxNorm(double max, double min, double x, double maxRange, double minRange)
{
    return (((x - min) / (max - min)) * (maxRange - minRange)) + minRange;
}

/*
 * 0 < speed < 200
 * 100- stay in place
 */
double normSpeed(int x)
{
    return minMaxNorm(MAX_SPEED, MIN_SPEED, x, MAX_SPEED_ENV, MIN_SPEED_ENV);
}

/*
 * -15 < angVel < 15
 * 0- stay in place
 * angVel > 0 - turn left
 */

double normAngularVel(int x)
{
    return minMaxNorm(MAX_ANG_VEL, MIN_ANG_VEL, x, MAX_ANG_VEL_ENV, MIN_ANG_VEL_ENV);
}

void CInsectbotOnFrameFollowWall::turn(unsigned int index)
{
    nymbot_update_velocity(index, TURN_SPEED, TURN_ANG_VEL);
}

void CInsectbotOnFrameFollowWall::goStraight(unsigned int index)
{
    nymbot_update_velocity(index, MAX_SPEED, NO_TURN);
}

void CInsectbotOnFrameFollowWall::updateWantedOrientation(unsigned int index, Real orientVal)
{
    nymbots_arr[index].wantedOrientation = int((closestOrientation(orientVal) + 90)) % 360;
}

void CInsectbotOnFrameFollowWall::PreStep()
{
    CApi_loop_function::PreStep();
    for (unsigned int index = 0; index < robotNumber; index++)
    {
        CDegrees orientation = nymbots_arr[index].orientation;
        nymbot_location_data nymbot = nymbots_arr[index];
        bool res = hitBorders({nymbot.x, nymbot.y}, orientation);
        TStateNames currState = nymbot.state;
        Real orientVal = normOrientation(orientation);
        switch (currState)
        {
        case INSECTBOT_STATE_STOP:
            currState = INSECTBOT_STATE_MOVING;
            updateWantedOrientation(index, orientVal);
            break;
        case INSECTBOT_STATE_TURNING:
            if (turnRightAngle(orientVal, index))
            {
                currState = INSECTBOT_STATE_MOVING;
                break;
            }
            else
            {
                turn(index);
            }
            break;
        case INSECTBOT_STATE_MOVING:
            if (res)
            {
                currState = INSECTBOT_STATE_TURNING;
                updateWantedOrientation(index, orientVal);
                turn(index);
            }
            else
            {
                goStraight(index);
            }
            break;
        }
        nymbots_arr[index].state = currState;
        frame++;
    }
}
void CInsectbotOnFrameFollowWall::getRobotsCount(string type)
{
    // Access the space
    argos::CSimulator &cSimulator = argos::CSimulator::GetInstance();
    argos::CSpace &cSpace = cSimulator.GetSpace();
    // Get the number of entities of a specific type (e.g., robots)
    robotNumber = cSpace.GetEntitiesByType(type).size();
}

/****************************************/
/****************************************/

REGISTER_LOOP_FUNCTIONS(CInsectbotOnFrameFollowWall, "insectbot_on_frame_follow_wall")

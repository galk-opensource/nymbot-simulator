#ifndef INSECTBOT_TEMPLATE_LOOP_FUNCION_H
#define INSECTBOT_TEMPLATE_LOOP_FUNCION_H


#include <argos3/core/simulator/loop_functions.h>
#include <argos3/core/simulator/entity/floor_entity.h>
#include <argos3/core/utility/math/range.h>
#include <argos3/core/utility/math/rng.h>
#include <controllers/messy_nymbot/messy_nymbot.h>
#include <time.h>
#include <../../nymbot-workspace/config/sim.h>
#include <argos3/core/simulator/simulator.h>
#include <argos3/core/utility/configuration/argos_configuration.h>
#include <plugins/robots/insectbot/simulator/insectbot_entity.h>
#include <controllers/diff_nymbot/diff_nymbot.h>
#include <time.h>
#include <random>
#include <../../nymbot-workspace/lib/structs/nymbot_location_data.h>
#include <../../nymbot-workspace/programs/wave/wave.h>



using namespace argos;
using namespace std;

//extern void onFrame();
extern nymbot_location_data nymbots_arr[ROBOT_MAX_NUMBER];

class CInsectbotTemplate : public CLoopFunctions {
public:
   CInsectbotTemplate();
   virtual ~CInsectbotTemplate() {}
   virtual void Init(TConfigurationNode& t_tree);
   virtual void PreStep();
   virtual void nymbot_update_velocity(int id, int speed, int angularVel);
   virtual void LastSeenPosition();
   virtual double normOrientation(CDegrees orientation);

private:
	int numOfBots;
	//nymbot_location_data nymbots_arr[ROBOT_MAX_NUMBER];

};

void onframe(); 
void init();

#endif

#include "insectbot_template_loop_function.h"

using namespace std;

#define MAX_SPEED 8
#define MIN_SPEED -8
#define MAX_ANGULAR_VELOCITY 3
#define MIN_ANGULAR_VELOCITY -3
#define startPositionX 0.35f
#define startPositionY -0.4f
#define interval 0.3f
#define forwardSpeed 170
#define backwardSpeed 30
#define noSpeed 100
#define threshold 3
#define wantedAngle 180


nymbot_location_data nymbots_arr[ROBOT_MAX_NUMBER];
CInsectbotTemplate* sim;
extern void onframe();

/****************************************/
/****************************************/

CInsectbotTemplate::CInsectbotTemplate() :
   numOfBots(1){}
   

/****************************************/
/****************************************/



/*
 * 0 < speed < 200. speed = 100 -> stay in place
 * 0 < angularVel < 15. angularVel = 0 -> move straight
 */
void CInsectbotTemplate::nymbot_update_velocity(int id, int speed, int angularVel) {
    int i = 0;
    CSpace::TMapPerType &m_cInsectbots = GetSpace().GetEntitiesByType("insectbot");
    for (CSpace::TMapPerType::iterator it = m_cInsectbots.begin();
         it != m_cInsectbots.end();
         ++it) {

        if (i == id) {
            CInsectbotEntity &cInsectbots = *any_cast<CInsectbotEntity *>(it->second);
            CDiffNymbot &IController = dynamic_cast<CDiffNymbot &>(cInsectbots.GetControllableEntity().GetController());
            IController.setVelocity(speed, angularVel);
            return;
        }
        i++;
    }
    // no such robot
}

void CInsectbotTemplate::Init(TConfigurationNode& t_node) {
        for (int i=0 ; i<ROBOT_MAX_NUMBER;i++){
        	nymbots_arr[i].state = INSECTBOT_STATE_STOP;
        }
        //sim = this;

}



CDegrees getOrientation(CInsectbotEntity &cInsectbots){
    vector<Real> orient;
    CQuaternion cQuat = cInsectbots.GetEmbodiedEntity().GetOriginAnchor().Orientation;
    CRadians cZAngle, cYAngle, cXAngle;
    CDegrees xDegree;
    cQuat.ToEulerAngles(cZAngle, cYAngle, cXAngle);
    xDegree = ToDegrees(cZAngle);
    return xDegree;
}


Real CInsectbotTemplate::normOrientation(CDegrees orientation) {
    Real orientVal = orientation.UnsignedNormalize().GetValue();
    return orientVal;
}


void CInsectbotTemplate::LastSeenPosition() {
    int nymbotID = 0;
    CSpace::TMapPerType &m_cInsectbots = GetSpace().GetEntitiesByType("insectbot");
    for (CSpace::TMapPerType::iterator it = m_cInsectbots.begin();
         it != m_cInsectbots.end();
         ++it) {
        CInsectbotEntity &cInsectbots = *any_cast<CInsectbotEntity *>(it->second);
        CDummyNymbot &IController = dynamic_cast<CDummyNymbot &>(cInsectbots.GetControllableEntity().GetController());

        CDegrees orientation = getOrientation(cInsectbots);
        vector<double> l = IController.getLocation();
        nymbots_arr[nymbotID].x = l.at(0);
        nymbots_arr[nymbotID].y = l.at(1);
        nymbots_arr[nymbotID].orientation = normOrientation(orientation);
        nymbotID++;
    }
}

nymbot_location_data* getNymbotArr() {
	return nymbots_arr;
}


void CInsectbotTemplate::PreStep() {
	onFrame();
}

/****************************************/
/****************************************/

REGISTER_LOOP_FUNCTIONS(CInsectbotTemplate, "insectbot_template_loop_function")

/****************************************/
/****************************************/

/*
void LastSeenPosition(){
	sim->LastSeenPosition();
}

void nymbot_update_velocity(int id, int speed, int angularVel){
	sim->nymbot_update_velocity(id,speed,angularVel);
}

void onframe(){
	onFrame();
}
*/

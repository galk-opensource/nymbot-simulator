#include "insectbot_on_frame_wave.h"

using namespace std;

#define MAX_SPEED 8
#define MIN_SPEED -8
#define MAX_ANGULAR_VELOCITY 3
#define MIN_ANGULAR_VELOCITY -3
#define startPositionX 0.35f
#define startPositionY -0.4f
#define interval 0.3f
#define forwardSpeed 170
#define backwardSpeed 30
#define noSpeed 100
#define threshold 3
#define wantedAngle 180

/****************************************/
/****************************************/

CInsectbotOnFrameWave::CInsectbotOnFrameWave() {}

/****************************************/
/****************************************/

void CInsectbotOnFrameWave::Init(TConfigurationNode &t_node)
{
    CApi_loop_function::Init(t_node);
}

void CInsectbotOnFrameWave::PreStep()
{
    CApi_loop_function::PreStep();

    static int inPos = -1;
    if (inPos != 1)
    {
        inPos = allInPlace();
        if (inPos != 1)
        {
            return;
        }
    }
    // already fix places
    static int isMoving = -1;
    isMoving = allInPlace();
    if (frame % 20 == 0)
    {
        for (unsigned int index = 0; index < robotNumber; index++)
        {
            nymbot_location_data nymbot = nymbots_arr[index];
            Real angle = normOrientation(nymbot.orientation);
            int angVel = fixAngle(angle);
            int last_index = (index - 1) % robotNumber;

            nymbot_location_data last_nymbot = nymbots_arr[last_index];
            TStateNames currState = nymbot.state;
            if (isMoving || (currState == INSECTBOT_STATE_STOP && last_nymbot.state == INSECTBOT_STATE_MOVING))
            { // if its the first or
                setState(INSECTBOT_STATE_MOVING, index);
                nymbot_update_velocity(index, forwardSpeed, 0);
                break;
            }
            else if (currState == INSECTBOT_STATE_MOVING && nymbot.x < 0)
            {
                setState(INSECTBOT_STATE_TURNING, index);
                nymbot_update_velocity(index, backwardSpeed, angVel);
                break;
            }
            else if ((currState == INSECTBOT_STATE_TURNING) && (nymbot.x > startPositionX))
            {
                nymbot_update_velocity(index, noSpeed, 0);
                setState(INSECTBOT_STATE_STOP, index);
                break;
            }
            else
            {
                int speed = noSpeed;
                if (currState == INSECTBOT_STATE_TURNING)
                {
                    speed = backwardSpeed;
                }
                else if (currState == INSECTBOT_STATE_MOVING)
                {
                    speed = forwardSpeed;
                }
                nymbot_update_velocity(index, speed, angVel);
            }
        }
    }
    frame++;
}

template <class T>
bool equal(T a1, T a2)
{
    return fabs(a1 - a2) < 0.05;
}


Real normOrientation(CDegrees orientation)
{
    Real orientVal = orientation.UnsignedNormalize().GetValue();
    return orientVal;
}


void CInsectbotOnFrameWave::setState(TStateNames state, unsigned int index)
{
    nymbots_arr[index].state = state;
}

bool CInsectbotOnFrameWave::allInPlace()
{
    for (unsigned int index = 0; index < robotNumber; index++)
    {
        nymbot_location_data nymbot = nymbots_arr[index];
        float reqPos = startPositionY + index * interval;
        if (!(equal(nymbot.x, startPositionX) && equal(reqPos, nymbot.y) && nymbot.state == INSECTBOT_STATE_STOP))
        {

            return false;
        }
    }
    return true;
}

int CInsectbotOnFrameWave::fixAngle(Real angle)
{
    if (angle > wantedAngle + threshold)
        return -2;
    if (angle < wantedAngle - threshold)
        return 2;
    return 0;
}

/****************************************/
/****************************************/

REGISTER_LOOP_FUNCTIONS(CInsectbotOnFrameWave, "insectbot_on_frame_wave")

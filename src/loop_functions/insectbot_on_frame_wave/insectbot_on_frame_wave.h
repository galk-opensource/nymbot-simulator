#ifndef INSECTBOT_ONFRAME_WAVE_H
#define INSECTBOT_ONFRAME_WAVE_H

#include <loop_functions/api_loop_function/api_loop_function.h>
#include <argos3/core/simulator/entity/floor_entity.h>
#include <argos3/core/utility/math/range.h>
#include <argos3/core/utility/math/rng.h>
#include <time.h>
#include <utils/utils_header.h>
#include <includes/nymbot_location_data.h>

using namespace argos;
using namespace std;

#define DEFAULT_ANGULAR_VELOCITY 5
#define DEFAULT_SPEED 148

class CInsectbotOnFrameWave : public CApi_loop_function
{

public:
    CInsectbotOnFrameWave();
    virtual ~CInsectbotOnFrameWave() {}

    virtual void Init(TConfigurationNode &t_tree);

    virtual void PreStep();

private:
    void setState(TStateNames state, unsigned int index);
    bool allInPlace();
    int fixAngle(Real angle);
};

#endif

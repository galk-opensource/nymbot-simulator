import os
import sys
import xml.etree.ElementTree as ET
import string
import json
import types
from enum import Enum
from tests.test_mandatory_components import test_mandatory_attributes
constants_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), './pipeline'))
sys.path.append(constants_dir)

#from pipeline import consts

def prRed(skk): print("\033[91m {}\033[00m" .format(skk))
def prGreen(skk): print("\033[92m {}\033[00m" .format(skk))
def prYellow(skk): print("\033[93m {}\033[00m" .format(skk))
def prPurple(skk): print("\033[95m {}\033[00m" .format(skk))
def prCyan(skk): print("\033[96m {}\033[00m".format(skk))

FINDSTR = "GetNodeAttribute"
LOOP_FUNCTION_ELEM = "loop_functions"
CONTROLLER_ELEM = "controllers"
consts_file = "./scripts/pipeline/consts.py"

def convert_to_cpp_path(file_path):
    '''
    This function converts a given library file path from build,
    to a .cpp file path from src.
    '''
    dir_name, filename = os.path.split(file_path)
    name, ext = os.path.splitext(filename)
    
    if ext:
        new_filename = name + '.cpp'
    else:
        new_filename = filename + '.cpp'
    dir_name = dir_name.replace("build", "src")
    cpp_file = os.path.join(dir_name, new_filename).replace("/lib","/")
    return cpp_file


def get_lines_containing_keyword(file_path, keyword=FINDSTR):
    with open(file_path, 'r') as file:
        # Read all lines and filter those that contain the keyword
        lines_with_keyword = [line.strip() for line in file if keyword in line]
    return lines_with_keyword

# Function to scan for all Enums in the module
def scan_enums_in_module(module):
    enums = []
    # Scan all attributes in the module
    for name, obj in vars(module).items():
        if isinstance(obj, type) and issubclass(obj, Enum):
            enums.append(obj)
    
    return enums

# def check_if_value_exists_in_consts(value):

#     # Check if the value exists in any Enum
#     enums = scan_enums_in_module(consts)  
#     for enum in enums:
#         if value in enum._value2member_map_:  # Check if the value is part of the Enum
#             return True
        
#     # Non-Enum constants
#     for const_name, const_value in vars(consts).items():
#         if const_value == value:
#             return True
#     return False

def check_cpp_sync_with_params(cpp_file, params, element):
    sync = True
    # Get the params from the .cpp file that should correspond to the params from the argos file
    lines_from_src = get_lines_containing_keyword(cpp_file)
    for line in lines_from_src:
        if not line.startswith("//"):
            param_from_src = line.split(',')[1].strip().strip('"')
            if param_from_src not in params:
                prRed(f"The parameter {param_from_src} from .cpp file is not in sync with the {element} in the argos file.")
                sync = False
    

    return sync

def extract_attribute_names(element):
    attribute_names = set()
    
    # Extract attribute names from the current element
    if element.attrib:
        attribute_names.update(element.attrib.keys())
    
    # Recursively extract attribute names from child elements
    for child in element:
        attribute_names.update(extract_attribute_names(child))
    
    return attribute_names

def check_sync_controller_element(element, json_file):
    sync = True
    if element is not None:
        for controller in element:
            print(f"Checking controller {controller.tag}")

            # get the controller library and convert to the .cpp path
            library = controller.attrib["library"]
            cpp_file = convert_to_cpp_path(library)

            if not os.path.isfile(cpp_file):
                prYellow(f"File {cpp_file} was not found. Exiting sync check for this controller.")
                continue

            print(f"Checking sync with {cpp_file}")

            params = extract_attribute_names(controller.find("params"))
        
            sync = check_cpp_sync_with_params(cpp_file, params, element)

            sync = sync and test_mandatory_attributes(json_file, params)
            
    else:
        prYellow("No 'controllers' element found.")
    return sync


def check_sync_loop_function_element(element, json_file):
    print(f"Checking {element}")
    sync = True
    library = None
    params = []

    # If the element is not found, nothing to check
    if not element:
        prYellow(f"No {element} element found.")
        return sync

    children = list(element)
    for child in children:
        # Iterate over all attributes of each child element
        for key, value in child.attrib.items():
            params.append(key)  # Add the key (parameter name)
        
    
                    
    if 'library' in element.attrib:
        library = element.attrib["library"]
    else:
        print(f"No library found for {element}. Skipping...")
        return
    
    cpp_file = convert_to_cpp_path(library)
        
    if not os.path.isfile(cpp_file):
        prYellow(f"File {cpp_file} was not found. Exiting sync check for this controller.")
        return sync

    print(f"Checking sync with {cpp_file}")
    

    sync = check_cpp_sync_with_params(cpp_file, params, element)
    sync = sync and test_mandatory_attributes(json_file, params)
    return sync


def check_tags_sync(template_file, json_file):
    prCyan("########## START XML TAG SYNC TEST ##########")
    tree = ET.parse(template_file)
    root = tree.getroot()

    print(f"Checking {template_file}")
    controllers_in_sync = check_sync_controller_element(root.find(CONTROLLER_ELEM), json_file)
    loopfunc_in_sync =  check_sync_loop_function_element(root.find(LOOP_FUNCTION_ELEM), json_file)
                    
    if controllers_in_sync and loopfunc_in_sync:
        prGreen(f"All parameters are in sync in {template_file}.")
        prCyan("########## END XML TAG SYNC TEST ##########")
        return True
    
    prPurple(f"Some parameters are not in sync. Please review them.")
    prCyan("########## END XML TAG SYNC TEST ##########")
    return False




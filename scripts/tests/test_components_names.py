import os

BASE_PATH = "./src/"  
SUFFIX = ".cpp"

def prRed(skk): print("\033[91m {}\033[00m" .format(skk))
def prCyan(skk): print("\033[96m {}\033[00m".format(skk))

def check_components_names(names_list, component):
    passed = True
    for name in names_list:
        file_path = BASE_PATH + component + "/" + name + "/" + name + SUFFIX
        if not os.path.exists(file_path):
            passed = False
            prRed(f"{name} does not exist in {component}")
        
    return passed


prCyan("########## START COMPONENTS NAMES SYNC TEST ##########")
controller_list_input = ["insectbot_AAA_flock"]

check_components_names(controller_list_input, "loop_functions")
check_components_names(controller_list_input, "controllers")
prCyan("########## END COMPONENTS NAMES SYNC TEST ##########")

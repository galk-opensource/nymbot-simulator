import xml.etree.ElementTree as ET
import re 
from pathlib import Path
import unittest

def check_relative_path(xml_file_path):
    """
     Checks that a given xml file has all relative path values
    """
    tree = ET.parse(xml_file_path)
    root = tree.getroot()

    libraries = root.findall(".//*[@library]")

    for elem in libraries:
        path = elem.attrib['library']
        match = re.search(r'/home/[^/]+/.*/(build/.+)', path)
        if match:
            print(f"Hardcoded path: {path} found in {xml_file_path}")
            return False
    return True

class TestArgosFiles(unittest.TestCase):

    def test_relative_path(self):
        directory_path = Path('experiments/')
        argos_files =  [file for file in directory_path.rglob('*.argos')]
        passed = True
        for file_path in argos_files:
            passed = check_relative_path(file_path) and passed
        if not passed:
            print("PLEASE run utils/fix_my_argos.py before pushing!!!") 
        self.assertEqual(passed, True)


if __name__ == '__main__':
    unittest.main()




    



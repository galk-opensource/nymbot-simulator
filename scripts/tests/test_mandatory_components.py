import xml.etree.ElementTree as ET
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from utils.printing import prRed, prGreen, prYellow, prPurple, prCyan
from utils.json import fetch

def test_component_exists(template_file, component, attribute = None):
    tree = ET.parse(template_file)
    root = tree.getroot()
    element = root.find(component)
    if element is None:
        prRed(f"No {component} found in {template_file}")
        return 0
    if attribute in element.attrib:
        attribute_value = element.attrib[attribute]
        if attribute_value:
            prGreen(f"Found {component} with {attribute} = {attribute_value}")
            return 1
        prRed(f"No {attribute} value found found for {element}. Aborting.")
    else:
        prRed(f"No {attribute} found for {element}. Aborting.")
    return 0

def test_mandatory_attributes(json_file, attributes, ):
    found_attributes = fetch(json_file, attributes)
    diff = [item for item in attributes if found_attributes[item] is None ]
    if diff == []:
        prGreen(f"All mandatory attrubutes exist in {json_file}")
        return 1
    for attr in diff:
        prRed(f"Mandatory attribute {attr} does not exist in {json_file}.")
    return 0
    
    
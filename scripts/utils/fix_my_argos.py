from lxml import etree
import re
from pathlib import Path

def fix_hardcoded_path(xml_file_path):
    """
    Accepts an XML file path and processes it to replace absolute paths with relative paths,
    while preserving comments and the XML declaration.
    """
    parser = etree.XMLParser(remove_blank_text=True)
    
    # Parse XML with lxml to preserve comments and declaration
    tree = etree.parse(xml_file_path, parser)
    root = tree.getroot()

    libraries = root.findall(".//*[@library]")

    modified = False
    for elem in libraries:
        path = elem.attrib['library']
        match = re.search(r'/home/[^/]+/.*/(build/.+)', path)
        if match:
            print(f"Fixing {xml_file_path}")
            relative_path = match.group(1)
            elem.attrib['library'] = relative_path
            modified = True

    if modified:
        # Write back preserving XML declaration
        tree.write(xml_file_path, xml_declaration=True, encoding="utf-8", pretty_print=True)

directory_path = Path('experiments/')
argos_files = list(directory_path.rglob('*.argos'))

for file_path in argos_files:
    fix_hardcoded_path(file_path)

import json

def find_key(d, target_key):
    # If the current element is a dictionary, recurse into it
    if isinstance(d, dict):
        for key, value in d.items():
            if key == target_key:
                return value  # Return the value associated with the key
            # Otherwise, recurse into the value (which can be a list or dictionary)
            result = find_key(value, target_key)
            if result is not None:
                return result

    # If the current element is a list, check each element recursively
    elif isinstance(d, list):
        for item in d:
            result = find_key(item, target_key)
            if result is not None:
                return result

    return None  # Return None if the key was not found

def fetch(json_path, attributes):
    results = {}
    with open(json_path, 'r') as file:
        json_data = json.load(file)
        for attr in attributes:
            results[attr] = find_key(json_data, attr)
    return results

def fetch_one(json_path, attribute):
    with open(json_path, 'r') as file:
        json_data = json.load(file)
        return find_key(json_data, attribute)
    return None

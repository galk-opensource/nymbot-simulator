# Nymbot Simulator Pipeline

This repository contains a pipeline for running experiments with the Nymbot simulator. The pipeline automates the execution of experiments, including parameter combinations, data collection, and result processing.


### ./scripts

#### `loop_pipeline.py`
This script performs a series of experiments by running a set of combinations of parameters, executing certain pipeline tasks, and processing results.

### ./scripts/pipeline

#### `running.py`
This script is designed to set up and run multiple experiments using the ARGoS simulator.

#### `create_output.py`
This script is focused on the analysis and visualization of experimental data from simulations, particularly in the context of metrics such as the "order" parameter or average reward across multiple runs. It is designed to process raw experiment data stored in CSV files, generate meaningful plots, and organize results effectively.

### ./scripts/pipeline/config_modules
This directory contains experiment components which exist in the template `.argos` file that are executed in `running.py`.
For example, `arena_component.py` applies the experiment configuration parameters to the arena.  
To add a new component:
1. Create a new component class which inherits from `Component`.
2. In `running.py`, import your new component class and add it to the `components` list.  
The `create_argos_config` iterates the component list and applies the experiment configuration.

### ./scripts/parameters
This directory contains important configuration files:

#### `experiment.json`
This configuration file defines both mandatory and optional parameters for running an experiment. Mandatory parameters must be specified to run the experiment, while optional parameters allow for customization of behaviors. The file enables flexibility in setting up various aspects of the experiment while ensuring essential parameters are always provided. You can modify this file as much as you like, but make sure you provide values for the mandatory parameters.

#### `pipeline.json`
This configuration file defines essential pipeline settings, including the source and destination directories for data, a temporary directory for experiment files, and the number of threads to be used during the pipeline execution. The `prefix_data` parameter sets the base directory where the experiment data will be stored, while `src_dir` and `dest_dir` control the locations for data movement.

Basically you can create configuration files of your own but make sure the pipeline references to them instead of the default ones.

### ./scripts/tests
This directory contains tests.

#### `test_components_names.py`
This test verifies if the specified component files (e.g., "insectbot_AAA_flock") exist in the correct directories for both "loop_functions" and "controllers."

#### `test_mandatory_components.py`
This script defines two functions to validate certain conditions in XML and JSON files:

- `test_component_exists`: It checks whether a specified config component exists in an XML template and if an optional attribute is present and has a value.
- `test_mandatory_attributes`: It ensures that all specified mandatory attributes are present in a JSON file, and if any are missing, it reports them as errors.

#### `test_xml_tags_sync.py`
This script performs synchronization checks between XML template files and corresponding C++ source files and JSON file.

### ./scripts/utils
This directory contains utility functions.

#### `printing.py`
These are utility functions designed to print colored text to the terminal:

- `prRed(skk)`: Prints text in red.
- `prGreen(skk)`: Prints text in green.
- `prYellow(skk)`: Prints text in yellow.
- `prPurple(skk)`: Prints text in purple.
- `prCyan(skk)`: Prints text in cyan.

#### `json.py`
These are utility functions designed to search for specific keys in a JSON file:

- `find_key`: Recursively searches for a key in nested structures (both dictionaries and lists).
- `fetch`: Searches for multiple attributes and returns a dictionary of results.
- `fetch_one`: Fetches a single attribute's value from the JSON file.

#### `fix_my_argos.py`
This script fixes .argos files

- `fix_hardcoded_path`: Accepts an XML file path and processes it to replace absolute paths with relative paths.


## How to run
After building the project, In the command line terminal, ensure you are in the `nymbot-simulator` directory and run:

 `python3 ./scripts/loop_pipeline.py -m <experiment_name> -c <experiment_json_config> -t <experiment_argos_template>`

Command line parameters:

- `-m` or `--experiment_name`: The name of the experiment. For example "motion_diff".

- `-c` or `--experiment_config`: Path to the experiment JSON. For example "scripts/parameters/experiment.json".

- `-t` or `--argos_template`: Path to the .argos template file. For example "experiments/examples/insectbot_motion_diff.argos"

If the tests indicate that some parameters are not in sync, please correct them and rebuild the project if any .cpp files were modified.

### Output:
The Pipeline will generate log files and plots in a directory named `data`, located outside of `nymbot-simulator`.
- This happens only if logging is implemented in the loop function source code (see flocking_controller branch).
- If logging is not implemented, only experiment.settings will be logged.

This is the directory structure of the output:

![alt text](image.png)

> [!Note]  
> If you stop the pipeline run before it completes, you will need to manually delete any log files created in the `nymbot-simulator/experiments` directory. The script only moves the files to the `data` directory once the run finishes.

> [!Info]  
> To tweak the experiment according to the provided code, you would generally need to adjust the configuration settings in the JSON files and potentially the component scripts.

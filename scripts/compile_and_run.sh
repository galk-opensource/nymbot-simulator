
if [ $# -eq 0 ]
  then
    echo "No arguments recieved"
    exit
fi

cd build


#set -e


make
if [ $? -eq 0 ]
then
	cd ..
	argos3 -c experiments/$1
fi



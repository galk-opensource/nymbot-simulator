#!/bin/bash -c


# install argos3 simulator
echo "Downloading and Installing ARGoS3 Simulator"

echo "This script is out of date, and likely is downloading the wrong version for your installation. It is maintained for historical reasons. Use at your own risk."
echo "If this fails, follow the instructions at:  "https://www.argos-sim.info/core.php"

VERSION=59
package=argos3_simulator-3.0.0-x86_64-beta$VERSION.deb

wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1OVKPMZFa0GzSU1RwioaEReUCUl-vngkR' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1OVKPMZFa0GzSU1RwioaEReUCUl-vngkR" -O /tmp/$package && rm -rf /tmp/cookies.txt ;
# sudo dpkg -i $package
sudo apt install /tmp/$package
rm /tmp/$package && rm /tmp/cookies.txt


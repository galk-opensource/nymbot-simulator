import csv
import os
from collections import defaultdict

def split_data_to_files(input_dir, buffer_limit=10000):
    """
    Iterates all subdirectories and splits each file based on the first column.
    """
    for subdir, _, files in os.walk(input_dir):
        for file in files:
            if file.endswith("data.csv"):
                file_path = os.path.join(subdir, file)
                split_to_files_based_on_first_column(file_path, buffer_limit)

def split_to_files_based_on_first_column(data, buffer_limit):
    """
    Split the data into multiple files based on the first column, writing in chunks
    to limit memory usage.
    """
    input_dir = os.path.dirname(data)
    
    # Dictionary to hold lists of rows for each target file
    data_buffer = defaultdict(list)
    
    # Read and categorize rows based on the first column
    with open(data, 'r') as csvfile:
        reader = csv.reader(csvfile)
        
        for row in reader:
            if not row:
                continue
            
            # The first column indicates the target file
            target_file = row[0] + ".csv"
            # Append row (excluding the first column) to the appropriate buffer
            data_buffer[target_file].append(','.join(row[1:]))
            
            # If buffer limit is reached, write and clear buffer for each file
            if len(data_buffer[target_file]) >= buffer_limit:
                write_buffer_to_file(data_buffer, input_dir)
    
    # Write any remaining data in buffer
    write_buffer_to_file(data_buffer, input_dir, final_write=True)

def write_buffer_to_file(data_buffer, input_dir, final_write=False):
    """
    Writes buffered data to respective files and clears the buffer.
    """
    for target_file, rows in list(data_buffer.items()):
        target_file_path = os.path.join(input_dir, target_file)
        # Append to target file, creating it if it doesn't exist
        with open(target_file_path, 'a', newline='') as f:
            f.write('\n'.join(rows) + '\n')
        
        # Clear the buffer after writing, unless this is the final write
        if not final_write:
            data_buffer[target_file].clear()


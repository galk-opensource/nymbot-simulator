from .component import Component
import json
from utils.json import fetch_one
import xml.etree.ElementTree as ET

LOG_OUTPUT = "output"
TIMESTAMP = "timestamp"
NUMBER_OF_ROBOTS = "num_of_robots"
PRIVATE_TIMESTAMP = "private_timestamp"
pipeline_conf_file = './scripts/parameters/pipeline.json'

class LogsComponent(Component):
    def apply(self, root, i=None, params=None):
        prefix_data = fetch_one(pipeline_conf_file, "prefix_data")
        logs = root.find("loop_functions/logs")
        if logs is None:
            loop_function  = root.find("loop_functions")
            logs = ET.SubElement(loop_function, "logs")
        data_path = f"{prefix_data}{params['experiment_name']}/{params[TIMESTAMP]}/{params[NUMBER_OF_ROBOTS]}/{params[PRIVATE_TIMESTAMP]}"
        logs.attrib[LOG_OUTPUT] = f"{data_path}/logs/{i}/data.csv"
        #logs.attrib[LOG_OUTPUT] = f"{prefix_data}/{params[TIMESTAMP]}/{params[NUMBER_OF_ROBOTS]}/{params[PRIVATE_TIMESTAMP]}/logs/{i}/data.csv"


            
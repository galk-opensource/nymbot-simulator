from .component import Component

LOCOMOTION_TYPE = "locomotion_type"
MIN_PAUSE_DURATION_ATTRIBUTE = "min_pause_duration"
MAX_PAUSE_DURATION_ATTRIBUTE = "max_pause_duration"
MIN_GO_DURATION_ATTRIBUTE = "min_go_duration"
MAX_GO_DURATION_ATTRIBUTE = "max_go_duration"
EARLY_PAUSE_TERMINATION = "early_pause_termination"

class LocomotionComponent(Component):
    def apply(self, root, i=None, params=None):
        locomotion = root.find("controllers/flock_controller/params/locomotion")
        locomotion.attrib[LOCOMOTION_TYPE] = str(params[LOCOMOTION_TYPE])
        locomotion.attrib[MIN_PAUSE_DURATION_ATTRIBUTE] = str(params[MIN_PAUSE_DURATION_ATTRIBUTE])
        locomotion.attrib[MAX_PAUSE_DURATION_ATTRIBUTE] = str(params[MAX_PAUSE_DURATION_ATTRIBUTE])
        locomotion.attrib[MIN_GO_DURATION_ATTRIBUTE] = str(params[MIN_GO_DURATION_ATTRIBUTE])
        locomotion.attrib[MAX_GO_DURATION_ATTRIBUTE] = str(params[MAX_GO_DURATION_ATTRIBUTE])
        locomotion.attrib[EARLY_PAUSE_TERMINATION] = str(params[EARLY_PAUSE_TERMINATION])

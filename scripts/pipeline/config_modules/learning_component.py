from .component import Component

STATE_TYPE = "state_type"
STATE_COUNT = "state_count"
CHOOSE_ACTION_METHOD = "choose_action_method"
LEARNING_TIME = "learning_time"
ACTION_LIST = "action_list"
ACTION_DURATION = "action_duration"

class LearningComponent(Component):
    def apply(self, root, i=None, params=None):
        learning = root.find("controllers/flock_controller/params/learning")
        learning.attrib[STATE_TYPE] = str(params[STATE_TYPE])
        learning.attrib[STATE_COUNT] = str(params[STATE_COUNT])
        learning.attrib[CHOOSE_ACTION_METHOD] = str(params[CHOOSE_ACTION_METHOD])
        learning.attrib[LEARNING_TIME] = str(params[LEARNING_TIME])
        learning.attrib[ACTION_DURATION] = str(params[ACTION_DURATION])
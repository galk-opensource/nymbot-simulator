class Component:
    def apply(self, root, params=None, i=None):
        """Modify the XML tree. This will be implemented by subclasses."""
        raise NotImplementedError("Subclasses should implement this method")
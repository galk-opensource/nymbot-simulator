from .component import Component

TARGET_DISTANCE = "target_distance"
GAIN = "gain"
EXPONENT = "exponent"
SENSING_RADIUS = "r_sense"

class FlockingComponent(Component):
    def apply(self, root, i=None, params=None):
        flocking = root.find("controllers/flock_controller/params/flocking")
        flocking.attrib[TARGET_DISTANCE] = str(params[TARGET_DISTANCE])
        flocking.attrib[GAIN] = str(params[GAIN])
        flocking.attrib[EXPONENT] = str(params[EXPONENT])
        flocking.attrib[SENSING_RADIUS] = str(params[SENSING_RADIUS])
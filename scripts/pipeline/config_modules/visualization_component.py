from .component import Component

class VisualizationComponent(Component):
    def apply(self, root, i=None, params=None):
        vis = root.find("visualization")
        if vis is not None:
            root.remove(vis)
from .component import Component

FAULTY_ROBOTS_COUNT_ATTRIBUTE = "faulty_robots_count"

class FaultyComponent(Component):
    def apply(self, root, i=None, params=None):
        faulty = root.find("loop_functions/robot_failures")
        faulty.attrib[FAULTY_ROBOTS_COUNT_ATTRIBUTE] = str(params[FAULTY_ROBOTS_COUNT_ATTRIBUTE])
    
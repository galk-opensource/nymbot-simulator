from .component import Component

USE_FAULT_HANDLING_ATTRIBUTE = "use_fault_handling"

class FaultHandlingComponent(Component):
    def apply(self, root, i=None, params=None):
        fault_handling = root.find(f"controllers/flock_controller/params/fault_handling")
        fault_handling.attrib[USE_FAULT_HANDLING_ATTRIBUTE] = str(params[USE_FAULT_HANDLING_ATTRIBUTE])

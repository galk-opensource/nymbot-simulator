from .component import Component
OCCLUSIONS = "check_occlusions"

class MediaComponent(Component):
    def apply(self, root, i=None, params=None):
        media = root.find("media/range_and_bearing")
        media.attrib[OCCLUSIONS] = str(params[OCCLUSIONS])

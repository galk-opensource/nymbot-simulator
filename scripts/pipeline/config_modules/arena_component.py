from .component import Component

NUMBER_OF_ROBOTS = "num_of_robots"

class ArenaComponent(Component):
    def apply(self, root, i=None, params=None):
        arena = root.find("arena/distribute/entity")
        arena.attrib[NUMBER_OF_ROBOTS] = str(params[NUMBER_OF_ROBOTS])



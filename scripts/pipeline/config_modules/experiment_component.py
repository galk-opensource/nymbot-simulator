from .component import Component

SEED = "num_of_trials"
EXPERIMENT_LENGTH_IN_SECS = "length"
RUNNING_TIME = "running_time"

class ExperimentComponent(Component):
    def apply(self, root, i=None, params=None):
        experiment = root.find('framework/experiment')
        if i is not None:
            experiment.attrib[SEED] = str(i)
            experiment.attrib[EXPERIMENT_LENGTH_IN_SECS] = str(params[RUNNING_TIME])

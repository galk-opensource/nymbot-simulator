import csv
from datetime import datetime
import json
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import os
import seaborn as sns
# import scienceplots

# plt.style.use('science')


##########################################
############## PLOTTING ##################
##########################################

colors = sns.color_palette()
# Define two different color palettes
palette1 = sns.color_palette("deep")
palette2 = sns.color_palette("pastel")
palette3 = sns.color_palette("muted")
palette4 = sns.color_palette("Paired")
palette5 = sns.color_palette("Set1")
palette6 = sns.color_palette("Set2")
palette7 = sns.color_palette("husl", 9)

REWARD_Y_MIN = -0.002
REWARD_Y_MAX = 0.002

# Concatenate the two palettes
COLORS = palette1 + palette2 + palette3 + \
    palette4 + palette5 + palette6 + palette7


LABELS = {
"target_distance": "Target Distance",
"gain" :"Gain",
"exponent" : "Exponent",
"r_sense" : "Sensing Radius",
"state_type" : "State Type",
"state_count" : "State Count",
"choose_action_method" :"Choose Action Method",
"learning_time" : "Learning Time",
"action_list" : "Action List",
"value" : "Action Value",
"action_duration" : "Action Duration",
"check_occlusions" : "Check Occlusions",
"min_pause_duration" : "Minimum Pause Duration",
"max_pause_duration" : "Maximum Pause Duration",
"min_go_duration" : "Minimum Go Duration",
"max_go_duration" : "Maximum Go Duration",
"early_pause_termination" : "Early Pause Termination",
"faulty_robots_count" : "Faulty Robots Count",
"output" : "Log Output",
"quantity" : "Quantity",
"num_of_trials" : "Number of Trials",
"length" : "Experiment Length",
"num_of_robots" : "Number of Robots",
"timestamp" : "Timestamp of the run",
"private_timestamp" :"Timestamp of the experiment",
"running_time" : "Running Time",
"locomotion_type" : "Locomotion Type",
"fault_handling" : "Fault Handling",
"use_fault_handling" : "Use Fault Handling",
}

METRICS = {
    "ORDER": "Order",
    "AVG_REWARD": "Global Average Reward"
}

##########################################
########## METRIC FUNCTIONS ##############
##########################################


def calculate_order_of_multiple_experiments(dir,metric=METRICS["ORDER"]):
    """
    Iterates all subdirectories of dir and calculates the order parameter for each experiment.

    returns a df each column is a different experiment.
    """

    experiments = {}
    avg_q_table = pd.DataFrame()
    cnt = 0    
    for subdir, _, files in os.walk(dir):
        for file in files:
            if file.endswith("metric.csv"):
                file_path = os.path.join(subdir, file)
                data = read_data(file_path)
                order = data[metric]
                experiment_name = os.path.basename(subdir)
                experiments[experiment_name] = order

            # if file.endswith("q_table.csv"):
            #     file_path = os.path.join(subdir, file)
            #     q_table = get_q_table(file_path)
            #     if avg_q_table.empty:
            #         avg_q_table = q_table
            #     else:
            #         avg_q_table = avg_q_table.add(q_table, fill_value=0)
            #     cnt += 1

    # Create q_table heatmap
    # create_q_table(avg_q_table / cnt, f"{dir}/q_table.png",multiplier=100)
    # Create a dataframe with one column that has the argmax of the q_table for each agent
    # q_table_argmax = avg_q_table.idxmax(axis=1).to_frame()
    # create_q_table(q_table_argmax, f"{dir}/q_table_argmax.png",is_int=True)
    return pd.DataFrame(experiments)



def get_q_table(data):
    """
    Gets the q-table from the data file.
    """
    q_table = pd.read_csv(data, header=None, skiprows=1)
    q_table.set_index(0, inplace=True)
    q_table.sort_index(inplace=True)
    return q_table 


def create_q_table(q_table, output_path='q_table.png',multiplier=1,is_int=False):
    """
    Creates a Q-table from the q-table file. Its color coded as a heatmap.
    The data is a csv file, first col is id of the agent, and then the actions are separated by space delimiter.
    """
    
    # format based on is_int
    fmt = "d" if is_int else ".4f"

    plt.figure(figsize=(10, 14))
    sns.heatmap(q_table*multiplier, cmap='viridis', cbar=True, linewidths=3.5, annot=True, fmt=fmt)
    plt.xlabel('Action')
    plt.ylabel('Agent ID')
    plt.title('Q-Table')
    plt.savefig(output_path)
    plt.close()


##########################################
########## OUTPUT FUNCTIONS ##############
##########################################

def plot_metric(order,label, output_path,error_type='se',show_individual=False,close_plot=True,color=COLORS[0],y_lim_min=0,y_lim_max=1,title="Order"):
    """
    order - 2D DataFrame where each column is a different experiment,
            and each row is a different time step.

    Creates a plot of the order parameter for each experiment.
    Shaded area is the std or se.
    """
    if type(order) == pd.Series:
        order = pd.DataFrame(order)
    
    # Calculate mean and standard error or standard deviation
    mean_order = order.mean(axis=1)
    # Save mean order to a csv file
    mean_order.name = "Order"
    mean_order.to_csv(output_path.replace(".png", "_mean.csv"), header=True)
    error_label = 'Standard Error' if error_type == 'se' else 'Standard Deviation'
    if error_type == 'std':
        error = order.std(axis=1)  # Standard deviation along the rows
    elif error_type == 'se':
        error = order.std(axis=1) / np.sqrt(order.count(axis=1))  # Standard error
    
    # Plot each experiment
    if show_individual:
        for column in order.columns:
            plt.plot(order.index, order[column], alpha=0.3)

    # Plot mean with standard error shading
    plt.plot(order.index, mean_order, color=color, label=label)
    plt.fill_between(order.index, mean_order - error, mean_order + error, color=color, alpha=0.3)
    plt.ylim(y_lim_min, y_lim_max)
    plt.xlabel('Time Step')
    plt.ylabel(f'{title} Parameter')
    # plt.title(f'{title} Parameter Over Time')
    plt.legend()
    plt.grid(True)

    if close_plot:
        plt.savefig(output_path)
        plt.close()


#Plot the order parameter for each experiment all in one plot using plot order parameter
def plot_order_parameter_all_in_one(order,labels, output_path ,error_type='se',y_lim_min=0,y_lim_max=1,title="Order"):
    """
    gets a list of 2d dataframes where each column is a different experiment, and each row is a different time step.
    Iterates over the list, creating a single plot with all the order parameters.
    """
    set_plot_params(True)    
    for i in range(len(order)):
        if i > len(COLORS)-1:
            #use heatmap colors
            color = COLORS[0]
        else:
            color = COLORS[i]
        if i == len(order)-1:
            plot_metric(order[i],labels[i], output_path,error_type=error_type,show_individual=False,close_plot=True,color=color,y_lim_min=y_lim_min,y_lim_max=y_lim_max,title=title)
        else:
            plot_metric(order[i],labels[i], output_path,error_type=error_type,show_individual=False,close_plot=False,color=color,y_lim_min=y_lim_min,y_lim_max=y_lim_max,title=title)

    



##########################################
########## MISC FUNCTIONS ################
##########################################

def read_data(file_path):
    """
    Read the data from the file path.
    """
    #check if file_path is dir or file, if dir add CSV
    if(os.path.isdir(file_path)):
        file_path = os.path.join(file_path, "raw.csv")
    data = pd.read_csv(file_path)

    return data




        
    


def set_plot_params(large=False):
    plt.rcParams['figure.figsize'] = (10, 6)  # figure size
    plt.rcParams['axes.grid'] = True  # Show grid by default
    sns.set_palette("colorblind")

    if large: # Good for pdfs and papers
        plt.rcParams['font.size'] = 18  # font size
        plt.rcParams['axes.titlesize'] = 24  # title font size
        plt.rcParams['axes.labelsize'] = 18  # label font size
        plt.rcParams['lines.linewidth'] = 3  # line width
        plt.rcParams['xtick.labelsize'] = 14  # tick label font size
        plt.rcParams['ytick.labelsize'] = 14  # tick label font size
        plt.rcParams['legend.fontsize'] = 16  # legend font size
    else: # Good for checking plots
        plt.rcParams['font.size'] = 12  # font size
        plt.rcParams['axes.titlesize'] = 16  # title font size
        plt.rcParams['axes.labelsize'] = 14  # label font size
        plt.rcParams['lines.linewidth'] = 2  # line width
        plt.rcParams['xtick.labelsize'] = 10  # tick label font size
        plt.rcParams['ytick.labelsize'] = 10  # tick label font size
        plt.rcParams['legend.fontsize'] = 12  # legend font size



def read_settings(file_path):
    settings = {}
    
    with open(file_path, 'r') as file:
        for line in file:
            # Split the line into key and value
            key, value = line.strip().split(':', 1)  # Split on the first colon
            settings[key.strip()] = value.strip()  # Remove any extra whitespace

    return settings

def get_label(file,label_by):
    settings = read_settings(file)
    # check if label_by is in settings keys
    if label_by in settings.keys():
        if(label_by == "min_go_duration" or label_by == "min_pause_duration" or 
           label_by == "max_go_duration" or label_by == "max_pause_duration"):
            return f"G: {settings['min_go_duration']}-{settings['max_go_duration']} | P: {settings['min_pause_duration']}-{settings['max_pause_duration']}"
        return LABELS[label_by] + " " + settings[label_by]
    else:
        return ""
    
import os
import json
from datetime import datetime

def is_all_digits(labels):
    """Check if all elements in the list are digits."""
    return all(str(label).isdigit() for label in labels)

def sort_labels_and_orders(labels, orders):
    """Sort labels and orders, avoiding direct sorting of DataFrames."""
    
    if len(labels) != len(orders):
        raise ValueError("Labels and orders must have the same length.")

    if is_all_digits(labels):
        # Convert labels to integers for sorting
        labels = [int(label) for label in labels]

    # Get sorted indices based on labels
    sorted_indices = sorted(range(len(labels)), key=lambda i: labels[i])

    # Apply sorting
    sorted_labels = [str(labels[i]) for i in sorted_indices]  # Convert back to strings
    sorted_orders = [orders[i] for i in sorted_indices]  # Reorder orders safely

    return sorted_labels, sorted_orders

def extract_num_of_robots(settings_file):
    """
    Extracts the number of robots from the experiment settings file.
    """
    try:
        with open(settings_file, 'r') as file:
            for line in file:
                key, value = line.strip().split(':', 1)  # Split on the first colon
                if key.strip() == "num_of_robots":
                    return value.strip()
    except FileNotFoundError:
        print(f"Error: Settings file {settings_file} not found.")
    except Exception as e:
        print(f"Error reading settings file {settings_file}: {e}")

    return None

import os
import json
from datetime import datetime

def create_output(input_path, type_of_plot="single", data="raw", get_label_by="num_of_robots"):
    """
    Run multiple experiments based on command line arguments.

    Args:
        -i, --input_path (str): Template for experiments.

    Returns:
        None
    """
    if type_of_plot == "all":
        # Dictionary to store results grouped by num_of_robots
        robot_groups = {}

        print(f"Processing directory: {input_path}")

        # Traverse directories to collect orders and labels
        for subdir, _, files in os.walk(input_path):
            if os.path.basename(subdir) == "logs":
                order = calculate_order_of_multiple_experiments(subdir, metric=METRICS["ORDER"])
                avg_reward = calculate_order_of_multiple_experiments(subdir, metric=METRICS["AVG_REWARD"])
                settings_dir = subdir.replace("logs", "settings")

                # Extract `num_of_robots` from settings
                #settings = extract_experiment_settings(f"{settings_dir}/experiment.settings")

                # Get the label based on the specified parameter (e.g., "early_pause_termination")
                label = get_label(f"{settings_dir}/experiment.settings", get_label_by)

                # Extract `num_of_robots` separately to determine grouping
                robot_count_label = get_label(f"{settings_dir}/experiment.settings", "num_of_robots")

                if not robot_count_label:
                    print(f"Warning: Missing num_of_robots in settings for {subdir}, skipping...")
                    continue

                robot_label = str(robot_count_label)


                # Initialize storage if not already present
                if robot_label not in robot_groups:
                    robot_groups[robot_label] = {
                        "orders": [],
                        "avg_rewards": [],
                        "labels": [],
                        "metadata": []
                    }

                # Append data for this experiment
                robot_groups[robot_label]["orders"].append(order)
                robot_groups[robot_label]["avg_rewards"].append(avg_reward)
                robot_groups[robot_label]["labels"].append(label)  # Use full parameter label
                robot_groups[robot_label]["metadata"].append(subdir)

        # Generate a combined plot per num_of_robots
        plot_creation_time = datetime.now().strftime("%H%M")

        for robot_label, data in robot_groups.items():
            output_dir = os.path.join(input_path, robot_label.split()[-1])
            os.makedirs(output_dir, exist_ok=True)

            # Sort data for consistency
            sorted_labels, sorted_orders = sort_labels_and_orders(data["labels"], data["orders"])
            _, sorted_avg_rewards = sort_labels_and_orders(data["labels"], data["avg_rewards"])

            # Generate one combined plot for all varying parameters
            plot_order_parameter_all_in_one(
                sorted_orders, sorted_labels,  
                f"{output_dir}/order_compare_{plot_creation_time}.png",
                error_type='se'
            )

            # Save metadata separately for each num_of_robots
            with open(f"{output_dir}/order_compare_{plot_creation_time}_metadata.json", 'w') as file:
                json.dump(data["metadata"], file)

    else:  # Single experiment case
        settings_dir = os.path.join(input_path, "settings")
        label = get_label(f"{settings_dir}/experiment.settings", get_label_by)  # Select correct label
        robot_count_label = get_label(f"{settings_dir}/experiment.settings", "num_of_robots")  # Extract num_of_robots

        if not robot_count_label:
            print(f"Warning: Missing num_of_robots in settings for {input_path}, skipping...")
            return

        robot_label = str(robot_count_label)

        output_dir = os.path.join(input_path, robot_label.split()[-1])
        os.makedirs(output_dir, exist_ok=True)

        # Calculate orders and rewards for the single experiment
        orders = calculate_order_of_multiple_experiments(input_path, metric=METRICS["ORDER"])
        avg_rewards = calculate_order_of_multiple_experiments(input_path, metric=METRICS["AVG_REWARD"])

        # Generate and save a single combined plot
        set_plot_params(True)
        plot_metric(orders, label, f"{input_path}/order_parameter.png", show_individual=True)


def extract_experiment_settings(settings_file):
    """
    Extracts all settings from the experiment settings file as a dictionary.
    """
    settings = {}
    try:
        with open(settings_file, 'r') as file:
            for line in file:
                key, value = line.strip().split(':', 1)
                settings[key.strip()] = value.strip()
    except Exception as e:
        print(f"Error reading settings file {settings_file}: {e}")
    
    return settings if settings else None






def plot_from_name_file(name_file_path, output_path, error_type='se', y_lim_min=0, y_lim_max=1, title="Order",do_sort=False):

    """
    Reads a list of files and labels from a name.txt file and creates a plot of them all.
    """
    orders = []
    labels = []

    try:
        with open(name_file_path, 'r') as file:
            for line in file:
                file_path, label = line.strip().split(',')
                # If line start with #, skip it
                if file_path.startswith("#"):
                    continue
                order = calculate_order_of_multiple_experiments(file_path, metric=METRICS["ORDER"])
                orders.append(order)
                labels.append(label)
                
    except FileNotFoundError:
        print(f"Error: The file {name_file_path} was not found.")
    except Exception as e:
        print(f"An error occurred: {e}")

    # Sort data by labels
    if do_sort:
        labels, orders = sort_labels_and_orders(labels, orders)

    # Generate and save plot
    plot_order_parameter_all_in_one(orders, labels, output_path, error_type=error_type, y_lim_min=y_lim_min, y_lim_max=y_lim_max, title=title)

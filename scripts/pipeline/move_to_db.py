import os
import shutil
from pathlib import Path
import json
from utils.json import fetch
PIPELINE_CONF = './scripts/parameters/pipeline.json'
SRC_DIR = "src_dir"
DEST_DIR = "dest_dir"
TMP_EXP_DIR = "tmp_exp_dir"
def move_data(pipeline_conf_file=PIPELINE_CONF):
    """
    Moves data from a source directory to a destination directory, preserving the directory structure.
    The function walks through the directory structure in the source root, and for each file found, it moves
    the file to the corresponding location in the destination root. If the destination file already exists, it skips the file.
    After moving all files, it removes the source root and a temporary directory.
    Raises:
        Exception: If there is an error moving a file.
        OSError: If there is an error removing the source or temporary directory.
    Note:
        The source and destination directories are defined by the global variables SRC_DIR and DEST_DIR, respectively.
    """

    json_data = fetch(pipeline_conf_file, [SRC_DIR, DEST_DIR, TMP_EXP_DIR])


    # Define source and destination root directories
    source_root = Path(json_data[SRC_DIR])
    dest_root = Path(json_data[DEST_DIR])

    # Walk through the directory structure in the source root
    for source_dirpath, dirnames, filenames in os.walk(source_root):
        #print((source_dirpath, dirnames, filenames))
        # Only proceed if in a "tttttt" directory (not intermediate yyyy/mm/dd levels)
        if not filenames:
            continue
        
        # Calculate relative path from source root
        relative_path = Path(source_dirpath).relative_to(source_root)
        
        # Define the destination directory
        dest_dirpath = dest_root / relative_path
        
        # Create the destination directory if it doesn’t exist
        dest_dirpath.mkdir(parents=True, exist_ok=True)

        # Move each file in the source directory to the destination directory
        for filename in filenames:
            source_file = Path(source_dirpath) / filename
            dest_file = dest_dirpath / filename
            # Check if the file already exists in the destination
            if dest_file.exists():
                print(f"Skipping {dest_file}, already exists.")
            else:
                try :
                    shutil.move(str(source_file), str(dest_file))
                
                except Exception as e:
                    print(f"Error moving {source_file} to {dest_file}: {e}")
                    exit(1)
    try:
        shutil.rmtree(source_root)
        shutil.rmtree(json_data[TMP_EXP_DIR])
    except OSError as e:
        print(f"Error removing {source_dirpath}: {e}")


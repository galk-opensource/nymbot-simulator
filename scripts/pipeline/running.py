import xml.etree.ElementTree as ET
import os
import subprocess
import traceback
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime
import argparse
import json
from utils.json import fetch_one

from .config_modules.arena_component import ArenaComponent
from .config_modules.experiment_component import ExperimentComponent
from .config_modules.logs_component import LogsComponent
from .config_modules.visualization_component import VisualizationComponent

TIMESTAMP = "timestamp"
PRIVATE_TIMESTAMP = "private_timestamp"
NUMBER_OF_ROBOTS = "num_of_robots"
EXPERIMENT_LENGTH_IN_SECS = "length"
SEED = "num_of_trials"
PIPELINE_CONF = './scripts/parameters/pipeline.json'

components = [
    ExperimentComponent(),
    ArenaComponent(),
    LogsComponent(),
    VisualizationComponent(),
]

def log_experiment_settings(settings, base_path):
    """
    Log experiment settings to a file.

    Args:
        settings (dict): A dictionary containing the experiment settings.
    """
    timestamp = settings.get(TIMESTAMP)
    robot_count = settings.get(NUMBER_OF_ROBOTS)
    private_time = settings.get(PRIVATE_TIMESTAMP)
    settings_dir = f'{base_path}/{timestamp}/{robot_count}/{private_time}/settings'
    os.makedirs(settings_dir, exist_ok=True)
    settings_path = os.path.join(settings_dir, 'experiment.settings')
    with open(settings_path, 'w') as f:
        for key, value in settings.items():
            if(key == EXPERIMENT_LENGTH_IN_SECS):
                f.write(f"{key}: {value*10}\n") # From seconds to ticks
            else:
                f.write(f"{key}: {value}\n")
    

def create_argos_config(i,params, template_file):
    # Load and modify the XML file
    tree = ET.parse(template_file)
    root = tree.getroot()

    for component in components:
            component.apply(root, i, params)
    
    # Create the folder for storing experiment files
    os.makedirs("experiments/experiments_argos_files", exist_ok=True)
    experiment_path = f'experiments/experiments_argos_files/experiment_{i}_RobotNum_{params[NUMBER_OF_ROBOTS]}.xml'
    
    # Save the modified XML configuration
    with open(experiment_path, 'wb') as f:
        f.write(ET.tostring(root, 'utf-8'))
    
    return experiment_path

def run_experiment(experiment_path, params,i, pipeline_conf_file=PIPELINE_CONF):
    prefix_data = fetch_one(pipeline_conf_file, "prefix_data")
    # Create a unique folder for each run based on current date and time
    data_path = f"{prefix_data}{params['experiment_name']}/{params[TIMESTAMP]}/{params[NUMBER_OF_ROBOTS]}/{params[PRIVATE_TIMESTAMP]}"
    experiment_dir = f"{data_path}/logs/{i}"
    #experiment_dir = f'{prefix_data}/{params[TIMESTAMP]}/{params[NUMBER_OF_ROBOTS]}/{params[PRIVATE_TIMESTAMP]}/logs/{i}'
    os.makedirs(experiment_dir, exist_ok=True)

    # File paths for stdout (chosen actions) and stderr (log)
    stderr_path = os.path.join(experiment_dir, 'err.txt')

    # Run the ARGoS command and capture stdout and stderr
    try:
        command = ['argos3', '-c', experiment_path]

        # Open os.devnull to nullify stdout
        with open(os.devnull, 'w') as devnull:
            subprocess.run(command, stdout=devnull, stderr=subprocess.PIPE, check=True, text=True)

        # print(f"Experiment {params} completed successfully. Output saved in {experiment_dir}")
    except subprocess.CalledProcessError as e:
        print(f"Experiment {params} failed with error: {e}")
        print(f"Command that failed: {' '.join(command)}")
        print(f"Exit code: {e.returncode}")
        
        # Capture and display the specific error line in your script if an error occurs
        print(f"Error traceback:\n{traceback.format_exc()}")
        
        # Display the stderr output for further diagnostic information
        if e.stderr:
            print(f"Standard error output:\n{e.stderr}")

        # Optionally re-raise the error if you want to propagate it
        raise

def run_experiments(params,template_file, pipeline_conf_file=PIPELINE_CONF):
    num_of_threads = fetch_one(pipeline_conf_file, "num_of_threads")
    
    settings = params
    # settings[RUNNING_TIME] *= 10 # From seconds to ticks
    data_path = f"experiments/data/{params['experiment_name']}"
    log_experiment_settings(settings, data_path)
    # Number of threads to use (parallelism)
    # Create experiment configuration files
    experiments_to_run = []
    # timestamp = datetime.now().strftime('%Y/%m/%d/%H%M')
    for i in range(1,params[SEED]+1):
        experiment_path = create_argos_config(i,params, template_file)
        experiments_to_run.append((experiment_path, params,i))

    # Run the experiments in parallel using ThreadPoolExecutor
    with ThreadPoolExecutor(max_workers=num_of_threads) as executor:
        futures = [executor.submit(run_experiment, experiment_path, p,it) 
                   for experiment_path, p,it in experiments_to_run]

        # Wait for all futures to complete
        for future in futures:
            try:
                future.result()  # This will raise exceptions if any occurred during execution
            except Exception as e:
                print(f"An error occurred: {e}")


#!/bin/bash -v


rm -rf build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo ../src  # Release, Debug, RelWithDebInfo 
make
sudo make install

echo "Build is done!"

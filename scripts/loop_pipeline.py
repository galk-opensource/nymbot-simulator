from sklearn.model_selection import ParameterGrid
from pipeline import create_output, running, move_to_db, split_data_to_files
from datetime import datetime
from tqdm import tqdm
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from tests.test_xml_tags_sync import check_tags_sync#, check_json_sync_with_consts
from tests.test_mandatory_components import test_component_exists, test_mandatory_attributes
from utils.json import fetch_one
from utils.printing import prRed
import json
import argparse
import os

TIMESTAMP = "timestamp"
PRIVATE_TIMESTAMP = "private_timestamp"
NUMBER_OF_ROBOTS = "num_of_robots"
MIN_PAUSE_DURATION_ATTRIBUTE = "min_pause_duration"
MAX_PAUSE_DURATION_ATTRIBUTE = "max_pause_duration"
MIN_GO_DURATION_ATTRIBUTE = "min_go_duration"
MAX_GO_DURATION_ATTRIBUTE = "max_go_duration"
RUNNING_TIME = "running_time"

mandatory_experiment_attributes = [
    NUMBER_OF_ROBOTS,
    MIN_PAUSE_DURATION_ATTRIBUTE,
    MAX_PAUSE_DURATION_ATTRIBUTE,
    MIN_GO_DURATION_ATTRIBUTE,
    MAX_GO_DURATION_ATTRIBUTE,
    RUNNING_TIME
]

mandatory_pipeline_attributes = [
    "prefix_data", "num_of_threads", "src_dir", "dest_dir", "tmp_exp_dir"
]

def parse_args():
    # Create the argument parser
    parser = argparse.ArgumentParser(description="Run an experiment with a config and template.")
    
    # Add mandatory arguments with short flags
    parser.add_argument("-m", "--experiment_name", type=str, required=True, help="The name of the experiment")
    parser.add_argument("-c", "--experiment_config", type=str, required=True, help="Path to the experiment JSON config file")
    parser.add_argument("-t", "--argos_template", type=str, required=True, help="Path to the .argos template file")

    # Parse the command-line arguments
    args = parser.parse_args()

    # Validate the experiment config file (check if it exists and is a valid JSON file)
    if not os.path.exists(args.experiment_config):
        print(f"Error: The experiment config file {args.experiment_config} does not exist.")
        exit(1)

    try:
        with open(args.experiment_config, 'r') as config_file:
            config = json.load(config_file)
    except json.JSONDecodeError:
        print(f"Error: The experiment config file {args.experiment_config} is not a valid JSON.")
        exit(1)

    # Validate the .argos template file (check if it exists)
    if not os.path.exists(args.argos_template):
        print(f"Error: The .argos template file {args.argos_template} does not exist.")
        exit(1)


    return args


def main():
    args = parse_args()
    params = {}
    pipeline_conf_file = './scripts/parameters/pipeline.json'
    check_tags_sync(args.argos_template, args.experiment_config)
    if not test_mandatory_attributes(args.experiment_config, mandatory_experiment_attributes):
        prRed("Aborting.")
        return
    if not test_mandatory_attributes(pipeline_conf_file, mandatory_pipeline_attributes):
        prRed("Aborting.")
        return

    
    with open(args.experiment_config, 'r') as file:
        json_data = json.load(file)
        for v in json_data.values():
            for param in v.values():
                params.update(param)
                
    prefix_data = fetch_one(pipeline_conf_file, "prefix_data")

    # Generate all combinations using ParameterGrid
    param_grid = ParameterGrid(params)

    # Apply filter for valid combinations
    combinations = [
        dict(combination) for combination in param_grid
        if combination[MAX_PAUSE_DURATION_ATTRIBUTE] > combination[MIN_PAUSE_DURATION_ATTRIBUTE]
        and combination[MAX_GO_DURATION_ATTRIBUTE] > combination[MIN_GO_DURATION_ATTRIBUTE]
    ]

    # Find parameter with the longest list of values for labeling purposes
    label_by = max(params, key=lambda x: len(params[x]))

    # Log start time
    start_time = datetime.now()
    results = []  # To collect results for heatmap creation

    # Run experiments with each valid combination
    for combination in tqdm(combinations, desc="Running experiments"):
        combination["experiment_name"] = args.experiment_name
        combination[TIMESTAMP] = start_time.strftime('%Y_%m_%d_%H%M%S')
        combination[PRIVATE_TIMESTAMP] = datetime.now().strftime(
            '%H%M%S')

        # Run the experiment and collect results
        running.run_experiments(combination, args.argos_template)
        # Assume 'result' is the outcome you want to visualize
        # results.append((combination['min_pause'],
        #                combination['max_pause'], result))

        # Split data and create output files
        data_path = f"{prefix_data}{combination['experiment_name']}/{combination[TIMESTAMP]}/{combination[NUMBER_OF_ROBOTS]}/{combination[PRIVATE_TIMESTAMP]}"
        #data_path = f"{prefix_data}/{combination[TIMESTAMP]}/{combination[NUMBER_OF_ROBOTS]}/{combination[PRIVATE_TIMESTAMP]}"
        split_data_to_files.split_data_to_files(data_path)
        create_output.create_output(data_path, "single", "order")

    # Generate final output for all experiments
    create_output.create_output(
         f"{prefix_data}{combination['experiment_name']}/{combination[TIMESTAMP]}/", "all", "order", label_by)
    move_to_db.move_data()

    # Log duration
    end_time = datetime.now()
    duration = end_time - start_time
    print(f"Duration: {duration.total_seconds()} seconds")

    # Prepare data for heatmap
    # results_df = pd.DataFrame(results, columns=['min_pause', 'max_pause', 'result'])
    # heatmap_data = results_df.pivot('min_pause', 'max_pause', 'result')

    # # Plot the heatmap
    # plt.figure(figsize=(10, 6))
    # sns.heatmap(heatmap_data, annot=True, cmap='YlGnBu', cbar_kws={'label': 'Result'})
    # plt.title('Experiment Results Heatmap')
    # plt.xlabel('max_pause')
    # plt.ylabel('min_pause')
    # plt.show()


if __name__ == "__main__":
    main()

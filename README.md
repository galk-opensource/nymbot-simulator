# The README in nymbot-workspace is the starting point for the nymbot project.

Its recommended to start reading from there, working top-to-bottom starting at the nymbot-workspace. 
 
# Documentation

## Folder structure
    nymbot_simulator
    ├── docs
    ├── experiments
    │    ├── templates
    │    └── examples
    ├── scripts
    │    ├── build_insectbot.sh
    │    └── compile_and_run.sh
    └── src
        ├── controllers
        │     └── Diff
        ├── loop_functions
        │    ├── api_loop_function
        │    └── your_loop_function
        ├── structs
        │    ├── obstacles
        │    └── nymbot_location_data
        ├── plugins
        └── utils
            ├── insectbot_circle_fov_sense
            └── draw

### ./docs

Contains documentation for the insectbot plugin.  The maintained user guide is actually in [a Goggle doc](https://docs.google.com/document/d/1igELhMPduEV5TrTc9NRCFStOol9jSnAsLdsWE9K45jo/edit?usp=sharing).  A local copy which not be up to date is in ./docs/user-guide.odt.

Documentation for developers of InsectBot is in docs/dev-guide.odt  

TODO:  Needs to be completed with respect to the diff_insectbot controller, which is important to working with the nymbot-workspace


### ./scripts

Useful scripts for installation and sample program compilation

### ./src

1. The *plugins* folder holds the critical part of the plugin: definition of simulated robot mechanics, differential steering interface, visualization, etc.

2. The *controllers* folder holds several alternative robot types, built on the same platform. These controllers differ in the API they offer to the programmer.  
    - *diff_nymbot* This is the controller to be used.
    - *dummy_nymbot* is an old controller, whose use is deprecated (TODO).
    - *insectbot_avoider*, *insectbot_random*, *messy_nymbot* are for use when using the simulator as a platform without using the physical robots. Their API **does not match** the API of the robots, so useful only if you are interested in running simulation-only robots.  Recommended only for playing around with the simulator, but not for research work.
    **Important note** - There is a proximity sensor installed in the code of all the controllers, don't use it sense the real robots don't have it.

2. The *loop_functions* folder holds example insectbot programs.  Each such loop_function can be called to control one or more robots. Also the folder holds a the api of all loop_functions. (Any new loop_function inherits from it)
insectbot_template_loop_function - is for use with the workspace but is not updated. Do not use it.
3. The *experiments* folder holds example argos experiment files.
4. The *structs* folder holds include files used by loop functions. It should be moved to the loop_functions folder (TODO).
5. The *utils* folder holds the sense and draw library. Documentation for the sense library is in [its README](./src/utils/README.md).

# Installation

The nymbot simulator uses the ARGoS simulator platform, on top of which this git repository adds a plugin simulating the nymbot robot. The plugin is called "insectbot" for historical reasons.

Installation consists of installing the ARGoS simulator (typically available as precompiled packages) and then
compiling the insectbot plugin in this repository.

## Step 1:  Install ARGoS simulator

Go to [The ARGoS Download Page](https://www.argos-sim.info/core.php) and follow the installation instructions for your ubuntu/linux mint version (or whatever operating system you use).  Make sue to  download the correct ".deb" package for your ubunut/mint version.

Which ubuntu version is your mint?  run "cat /etc/issue" to get your mint version and then it's a simple query on the internet. 

The instructions apply "apt install", which automatically takes care of all dependent packages, including qt6, development libraries, etc.

The bash script "scripts/old/install_argos.sh" attempts to do this automatically, but unfortunately requires modification depending on the version of ARGoS and the version of the operating system.  Until it is fixed, it cannot be relied on.


Verify that ARGoS is installed.  Run 
```shell
 argos3 --version 
 argos3 -q all
 ```
The output should be:
```shell
ARGOS_VERSION=3.0.0-beta59
ARGOS_INSTALL_PREFIX=/usr
ARGOS_USE_DOUBLE=ON
ARGOS_WITH_LUA=ON
ARGOS_BUILD_FLAGS= -Wall -Wno-unknown-pragmas -O3 -DNDEBUG -O3 -DNDEBUG
```
``` shell
[INFO] Loaded library "/usr/lib/argos3/libargos3plugin_simulator_drone.so"
[INFO] Loaded library "/usr/lib/argos3/libargos3plugin_simulator_eyebot.so"
[INFO] Loaded library "/usr/lib/argos3/libargos3plugin_simulator_media.so"
[INFO] Loaded library "/usr/lib/argos3/libargos3plugin_simulator_epuck.so"
[INFO] Loaded library "/usr/lib/argos3/libargos3plugin_simulator_genericrobot.so"
[INFO] Loaded library "/usr/lib/argos3/libargos3plugin_simulator_footbot.so"
[INFO] Loaded library "/usr/lib/argos3/libargos3plugin_simulator_spiri.so"
[INFO] Loaded library "/usr/lib/argos3/libargos3plugin_simulator_dynamics3d.so"
[INFO] Loaded library "/usr/lib/argos3/libargos3plugin_simulator_block.so"
[INFO] Loaded library "/usr/lib/argos3/libargos3plugin_simulator_pipuck.so"
...
```

## Step 2:  Build and install the insectbot plugin

From the root of the nymbot-simulator repository (i.e., where this README.md file is located),
you can directly execute
```shell
./scripts/build_insectbot.sh
```

What it does is create a temporary 'build' folder, and then builds the plugin in "Release" mode, which attempts to apply all relevant optimizations while allowing debugging of the plugin. If you need to debug the plugin, recompile in RelWi


## Step 3:  Test the installation
Run the following command:
```shell
argos3 -c experiments/templates/nymbot_simple.argos
```

# How the simulator works

Loop function controls the simulator as a whole while each robot is controled by the controller.
The loop_function has a couple of built in functions:

- Init - Called once at the start of the experiment.
- PreStep - Called before each step of the simulation.
- PostStep - Called after each step of the simulation.
- Reset - Called when the experiment is reset.
- Destroy - Called when the experiment is destroyed.

The controller is called between the PreStep and PostStep functions of the loop function.

In the loop_function there is an array of structs of type `nymbot_location_data`, which keeps the location of each nymbot in the arena.It is updated every prestep of the simulation by calling the `LastSeenPosition()`. The struct is defined as follows:

```cpp
struct nymbot_location_data {
    float x;
    float y;
    CDegrees orientation;
    Real wantedOrientation;
    int16_t angular_velocity = 0;
    vector<float> speed {0.0,0.0};
    bool is_managed = false;
    int16_t default_angular_velocity = DEFAULT_ANGULAR_VELOCITY;
    uint8_t default_speed = DEFAULT_SPEED;
    TStateNames state;
};
```

TODO: Explain the inheritance of the loop function, and how to create a new loop function.

# Running Examples


## Running a Simple Example
```shell
argos3 -c experiments/templates/nymbot_simple.argos
```
## Running a 2 behavior Example
```shell
argos3 -c experiments/templates/insectbot_two_behaviours_simple.argos
```
## Running a simple Torus Example
```shell
./scripts/run_insectbot_torus_example.sh
```
## Running a swarm Example
```shell
argos3 -c experiments/examples/sense_motion_diff.argos
```

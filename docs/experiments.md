# Experiment Files (.argos)

## Arena Configuration Guidelines
When adding new objects to the arena, follow these guidelines to assign appropriate IDs:

### Obstacles

The following types of immovable obstacles are recognized:

- **Walls**. Walls are immovable barriers. Each wall must be assigned an ID following the format *w[number]*,
Where '[number]' is a unique identifier for the wall. For instance, the first wall will be *w1*, the second wall will be *w2*, and so on.
- **Cylinders** are a type of object that can be included in the arena. Each cylinder must be assigned an ID following the format: *c[number]*, where '[number]' is a unique identifier for the cylinder. For instance, the first cylinder will be *c1*, the second cylinder will be *c2*, and so on.





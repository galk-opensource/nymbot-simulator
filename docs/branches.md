# Branches

__Updated for 27.7.23__

## List of last branches updates

``` shell
2023-07-27 origin/branches_docs
2023-07-24 origin/controller_velocities_test
2023-07-20 origin/feature/install-with-prepackaged-deb
2023-07-06 origin/acutator_skip_with_velocityController
2023-05-23 origin/nymbot_diff_controller
2022-12-02 origin/sense
2022-11-23 origin/data_runs
2022-11-15 origin/master
2022-09-25 origin/sense_motion
2022-08-01 origin/sense_circle_fov
2022-05-04 origin/obstacle_sense
2022-04-27 origin/draw_on_arena
2022-02-10 origin/single_robot_Infinity_shape_movement
2022-01-02 origin/circle_movement
```

To get a list like so,use the command:

``` shell
git for-each-ref --sort='-committerdate:iso' --format='%(committerdate:short) %(refname:short)' refs/remotes/origin/
```

## General branches

circle_movement - controller(dummyNymbot) circle movement by Noam.

single_robot_Infinity_shape_movement - infinity circle movement by Ron.

acutator_skip_with_velocityController - A try to skip the differntial drive actuator used by the insectbot model. The goal was to avoid the casting from linear speed+angular speed to two wheels and then vice versa again(we found argos uses the differntial drive functions anyway so we wondered why we need to use the actuator in the middle of the logic, which expected left and right wheel velocities. we asked why couldn't it take the linear and angular velocity directly)
It was found that the actuator is still needed because without the actuator,the animation is broken.
Keeping the branch for documntation.

install-with-prepackaged-deb - Gal trying to compile argos and our files separately.

## Research branches

I know the following branches were used for research,so i put them in this section with the authors in order to maybe give an overview of the research evolution:

HEAD of the tree : data_runs - by David
sense_motion - by Noam
sense_circle_fov - merged into sense motion by Tal
obstacle_sense - by Noam
draw_on_arena - by Noam

sense - Not part of the tree but looks connected. Used mainly for adding models. By Tal,Noam,Gal.

# TODO:

## General
- [V] Move Drawings into Util/draw.
- [V] Disconnect drawings from specific loopfunctions.
- [?] Write documentation (comments and readme) for the utils (sense_fov and drawings).

## Loop functions
- [V] Make loopfunction template that all inherit from it.
- [ ] Delete unwanted functions of loopfunctions template.
- [ ] Insectbot_on_frame_wave - Robots move in a wave. Delete.
- [ ] Insectbot_on_frame_sense - Old version of motion. Example of detecting walls.
- [ ] Insectbot_motion - changed from dummy to diff. Need to change the velocity and turn rate.
- [V] Remove error of comparison of integer expressions of different signedness: ‘int’ and ‘unsigned int’ by changing all id to type ‘unsigned int’.
- [V] Update all loop functions (except motion) to inherit from the template.


## Controllers
- [ ] Avoider - Decide if to leave or delete. Can be useful for nymbot 2.0.
- [ ] Dummy - delete.
- [V] Diff - Fix speed and turn rate.
    - [ ] Possible BUG: src/controllers/diff_nymbot/diff_nymbot.cpp line 115
- [ ] Map the difference between Dummy and Diff (sense_motion is a good example).


## Experiments
- [ ] Sort the old folder.
- [ ] Create a test experiment for obstacle detection.
- [ ] Change controllers to diff and check that the experiment works.

## Structs
- [V] Check why there is nymbot_area and nymbot_data (both have ANGLES_ARRAY_SIZE).

## Utils
- [ ] Do a draw lib.
- [ ] BUG: Color of circle doesn't work when there are rays.
- [ ] Rename the files accordingly.
- [ ] Why sense uses the 0.03/0.02 setup.
- [ ] Unless objects start to move the function findObstacles doesnt need to recive the obstcles array. Move it to the Init.
- [ ] Change all helper functions to work with ARGoS types. (CVectors, CRadians, etc.)
- [ ] New feature - make walls in any place/angle wanted
    - [V] Change in `initBoxesMap()` from hard coded ids to regex.
    - [V] Add orientation to detection_data and angle_detection_data.
    - [ ] Calculate centerAngle based on orientation and not ID.
    - [ ] Calculate distance based on angle of wall.
    - [ ] Create tests for FOV with generic walls.

## Plugins
- [ ] BUG: Check why height is used as length [here](./src/plugins/robots/insectbot/simulator/qtopengl_insectbot.cpp#61)
- [ ] Why the sim cooridnate of the robot is not the center of the entity?
